find_package(LibXml2 REQUIRED)
find_package(Xenomai QUIET)

if(NOT XENOMAI_FOUND)
	warn("Xenomai not found, you will not be able to link a real time program")
endif()

execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion
                OUTPUT_VARIABLE GCC_VERSION)

if (GCC_VERSION VERSION_GREATER 4.3 OR GCC_VERSION VERSION_EQUAL 4.3)
	if (GCC_VERSION VERSION_GREATER 4.7 OR GCC_VERSION VERSION_EQUAL 4.7)
		ADD_DEFINITIONS("-std=c++11")
	else()
		ADD_DEFINITIONS("-std=c++0x")
	endif()
else()
	message(STATUS "GCC version < 4.3, c+11 is not supported!")
endif()

SET(FRAMEWORK_INCLUDE_DIR	
	${LIBXML2_INCLUDE_DIR}	
	${CMAKE_SYSROOT}/usr/include/opencv
	${UAV_DEV}/include/Framework
)
SET(FRAMEWORK_LIBRARY_DIR
	${UAV_DEV}/lib/${ARCH_DIR}
)

SET(FRAMEWORK_LIBRARIES	
	${LIBXML2_LIBRARIES}
	udt pthread ${FRAMEWORK_LIBRARY_DIR}/libdspcv_gpp.a cv cxcore highgui FileLib rt z
)

#VRPN for Optitrack
IF (FRAMEWORK_USE_OPTITRACK)
	SET(FRAMEWORK_LIBRARIES vrpn ${FRAMEWORK_LIBRARIES})
ENDIF (FRAMEWORK_USE_OPTITRACK)

#nmea for GPS
IF (FRAMEWORK_USE_GPS)
	SET(FRAMEWORK_LIBRARIES nmea ${FRAMEWORK_LIBRARIES})
ENDIF (FRAMEWORK_USE_GPS)

#framework
SET(FRAMEWORK_LIBRARIES Framework ${FRAMEWORK_LIBRARIES})

#sensor and actuator lib
IF (FRAMEWORK_USE_SENSOR_ACTUATOR)
	SET(FRAMEWORK_INCLUDE_DIR ${FRAMEWORK_INCLUDE_DIR} ${UAV_DEV}/include/SensorActuator)
	SET(FRAMEWORK_LIBRARIES ${FRAMEWORK_LIBRARY_DIR}/libSensorActuator.a ${FRAMEWORK_LIBRARIES})
ENDIF (FRAMEWORK_USE_SENSOR_ACTUATOR)

#filter lib
IF (FRAMEWORK_USE_FILTER)
	SET(FRAMEWORK_INCLUDE_DIR ${FRAMEWORK_INCLUDE_DIR} ${UAV_DEV}/include/Filter)
	SET(FRAMEWORK_LIBRARIES ${FRAMEWORK_LIBRARY_DIR}/libFilter.a iir ${FRAMEWORK_LIBRARIES})
ENDIF (FRAMEWORK_USE_FILTER)

#meta lib
IF (FRAMEWORK_USE_META)
	SET(FRAMEWORK_INCLUDE_DIR ${FRAMEWORK_INCLUDE_DIR} ${UAV_DEV}/include/Meta)
	SET(FRAMEWORK_LIBRARIES ${FRAMEWORK_LIBRARY_DIR}/libMeta.a ${FRAMEWORK_LIBRARIES})
ENDIF (FRAMEWORK_USE_META)

#simulator lib
IF (FRAMEWORK_USE_SIMULATOR)
	if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm")
		SET(FRAMEWORK_USE_SIMULATOR_GL FALSE)
	endif()
	SET(FRAMEWORK_INCLUDE_DIR ${FRAMEWORK_INCLUDE_DIR}
		${UAV_DEV}/include/Simulator
		${CMAKE_SYSROOT}/usr/include/irrlicht
		${UAV_DEV}/include/SensorActuator
		${CMAKE_SYSROOT}/usr/include/vrpn
		)
	
	SET(SIMU_LIBRARY SensorActuator)
	IF (FRAMEWORK_USE_SIMULATOR_GL)
		ADD_DEFINITIONS("-DGL")
		SET(FRAMEWORK_LIBRARIES ${FRAMEWORK_LIBRARY_DIR}/libSimulator_gl.a
			 ${SIMU_LIBRARY} GL vrpn ${FRAMEWORK_LIBRARIES} Irrlicht Xxf86vm)
	else()
		SET(FRAMEWORK_LIBRARIES ${FRAMEWORK_LIBRARY_DIR}/libSimulator_nogl.a ${SIMU_LIBRARY} vrpn ${FRAMEWORK_LIBRARIES})
	endif()
ENDIF (FRAMEWORK_USE_SIMULATOR)

#set RT or NRT specific libraries
set(FRAMEWORK_LIBRARIES_RT ${FRAMEWORK_LIBRARIES} ${XENOMAI_LIBRARIES})

list(FIND FRAMEWORK_LIBRARIES_RT "Framework" POS)
if(POS GREATER -1)
list(REMOVE_AT FRAMEWORK_LIBRARIES_RT ${POS})
list(INSERT FRAMEWORK_LIBRARIES_RT ${POS} "${FRAMEWORK_LIBRARY_DIR}/libFramework_rt.a")
endif()

set(FRAMEWORK_LIBRARIES_NRT ${FRAMEWORK_LIBRARIES})

list(FIND FRAMEWORK_LIBRARIES_NRT "Framework" POS)
if(POS GREATER -1)
list(REMOVE_AT FRAMEWORK_LIBRARIES_NRT ${POS})
list(INSERT FRAMEWORK_LIBRARIES_NRT ${POS} "${FRAMEWORK_LIBRARY_DIR}/libFramework_nrt.a")
endif()

