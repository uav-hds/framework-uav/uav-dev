if(NOT DEFINED ENV{IGEP_ROOT})
	message(FATAL_ERROR  "variable IGEP_ROOT not defined")
endif()

include($ENV{IGEP_ROOT}/uav_dev/cmake_modules/colored_message.cmake)

if(NOT DEFINED ENV{IGEP_ROOT})
	err("variable IGEP_ROOT not defined")
endif()

#commente car sinon fonctionne pas si la toolchain n'est pas a l'endroit par defaut
#
#SET(CMAKE_CXX_FLAGS "-O2")

if(NOT UAV_DEV)
	IF(UAV_DEV_TAG)
		warn("Configuring uav_dev for tag ${UAV_DEV_TAG}")
		SET(UAV_DEV $ENV{IGEP_ROOT}/uav_dev_svn/tags/${UAV_DEV_TAG})
		if(EXISTS "${UAV_DEV}/cmake_modules/GlobalCmakeUAV.cmake")
			UNSET(UAV_DEV_TAG)
			include(${UAV_DEV}/cmake_modules/GlobalCmakeUAV.cmake)
			return()
		else()
		   	err("File not found ${UAV_DEV}/cmake_modules/GlobalCmakeUAV.cmake Please check that ${UAV_DEV} is up to date")
		endif()
	ELSE()
		SET(UAV_DEV $ENV{IGEP_ROOT}/uav_dev)
	ENDIF()
ENDIF()

include(${UAV_DEV}/cmake_modules/arch_dir.cmake)

list(APPEND CMAKE_MODULE_PATH ${UAV_DEV}/cmake_modules/)

#framework
SET(FRAMEWORK_USE_FILE ${UAV_DEV}/cmake_modules/FrameworkUseFile.cmake)

#default executable ouput paths
if(NOT DEFINED EXECUTABLE_OUTPUT_PATH)
	SET(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
endif()
if(NOT DEFINED TARGET_EXECUTABLE_OUTPUT_PATH)
	SET(TARGET_EXECUTABLE_OUTPUT_PATH bin/arm)
endif()

#reimplement add executable to add a custom target for delivery (only on ARM)
#delivery are read from ssh config file
function(ADD_EXECUTABLE)
	if("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "arm" AND EXISTS "$ENV{HOME}/.ssh/config")
		file(STRINGS $ENV{HOME}/.ssh/config TEST)
		foreach(f ${TEST})
			string(FIND ${f} "Host " POS)#cherche ligne host
			if(${POS} GREATER -1)
				string(REPLACE Host "" TARGET_NAME ${f})#enleve Host
				string(STRIP ${TARGET_NAME} TARGET_NAME)#enleve les espaces
			endif()
			string(FIND ${f} HostName POS)#cherche hostname
			if(${POS} GREATER -1)
				string(FIND ${f} "192.168." POS)#cherche addresse
				if(${POS} GREATER 0)#garde que les adresses en 192.168.6.x
					string(REPLACE HostName "" ADDRESS ${f})#enleve Hostname
					string(STRIP ${ADDRESS} ADDRESS)#enleve les espaces
					message("adding delivery target for " ${ARGV0} " (" ${ADDRESS} ")")
					string(REPLACE "/" "_" TARGET_PATH ${TARGET_EXECUTABLE_OUTPUT_PATH})#les / ne sont pas acceptés
					add_custom_target(
					    delivery_root@${ADDRESS}_${TARGET_PATH}_${ARGV0}
					    COMMAND make
					    COMMAND scp ${EXECUTABLE_OUTPUT_PATH}/${ARGV0} root@${ADDRESS}:${TARGET_EXECUTABLE_OUTPUT_PATH}
					)
				endif()
			endif()
		endforeach(f) 
	endif()
	#call original function
	_ADD_EXECUTABLE(${ARGV})
endfunction()
