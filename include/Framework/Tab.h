/*!
 * \file Tab.h
 * \brief Class displaying a QTab on the ground station
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2011/10/07
 * \version 4.0
 */

#ifndef TAB_H
#define TAB_H

#include <Layout.h>

namespace framework
{
namespace gui
{

    class TabWidget;

    /*! \class Tab
    *
    * \brief Class displaying a QTab on the ground station
    *
    * Tabs are displayed in a TabWidget.
    */
    class Tab: public Layout
    {
        public:
            /*!
            * \brief Constructor
            *
            * Construct a Tab in the TabWidget.
            *
            * \param parent parent
            * \param name name
            * \param position tab position, -1 to put at the last position
            */
            Tab(const TabWidget* parent,std::string name,int position=-1);

            /*!
            * \brief Destructor
            *
            */
            ~Tab();

        private:
    };

} // end namespace gui
} // end namespace framework

#endif // TAB_H
