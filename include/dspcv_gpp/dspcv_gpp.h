#include <cxtypes.h>

//codes conversions couleurs
#define DSP_BGR2GRAY 6
#define DSP_YUYV2GRAY 62
#define DSP_UYVY2GRAY 63

#if defined (__cplusplus)
extern "C" {
#endif /* defined (__cplusplus) */



void init_dsp(const char* dspExecutable,int warningTimeoutMs=-1) ;
void close_dsp(void) ;


char* dsptestjpeg(IplImage* src_img,unsigned int* compressed_size);


void dspGoodFeaturesToTrack(IplImage* img_src,CvPoint* features,unsigned int* count,float quality_level, float min_distance);


void dspPyrDown(IplImage* img_src,IplImage* img_dst,unsigned char level);


void dspCalcOpticalFlowPyrLK(IplImage* img_A,IplImage* img_B,IplImage* pyr_A,IplImage* pyr_B,
        CvPoint* features_A,CvPoint* features_B,int count,CvSize winSize,
        int level,char *feat_status,unsigned int *error,CvTermCriteria criteria,int flags);

void dspCvtColor(IplImage* img_src,IplImage* img_dst,int code);

void dspThreshold(IplImage* img_src,IplImage* img_dst, float threshold,float max_value,int threshold_type );

void dspCloneImage(IplImage* img_src,IplImage* img_dst);

void dspSobel(IplImage* src_img,IplImage* dst_img,int dx,int dy);

int dspHoughLines2(IplImage* img_src, CvMat* line_storage, int method,
                              float rho, float theta, int threshold,
                              float param1=0, float param2=0);

int32_t dspHoughLines2_test(IplImage* img_src, CvMat* line_storage, int32_t method,
                              float rho, float theta_min,float theta_max,float theta_step, int32_t threshold);

int32_t dspHoughLines2_cv(IplImage* img, CvMat* line_storage, int32_t method,
            float rho, float theta, int32_t threshold,
            float param1, float param2);

#if defined (__cplusplus)
}
#endif /* defined (__cplusplus) */


