//  created:    2011/09/13
//  filename:   rt_thread.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe implementant une RT_TASK
//
//
/*********************************************************************/

#ifndef RT_THREAD_H
#define RT_THREAD_H

#include <native/task.h>
#include <string>

class rt_thread
{

    public:
        rt_thread(std::string name);
        virtual ~rt_thread();
        void start(void);

    private:
        std::string th_name;
        bool isRunning;

        static void main_task(void * arg);

    protected:
        virtual void run(void)=0;
        void SleepMS(uint32_t time);
        RT_TASK task;
};

#endif // RT_THREAD_H
