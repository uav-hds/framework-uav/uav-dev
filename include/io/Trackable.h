//  created:    2013/03/21
//  filename:   Trackable.h
//
//  author:     César Richard
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet trackable
//
//
/*********************************************************************/
#ifndef TRACKABLE_H
#define TRACKABLE_H

#include <vrpn_Tracker.h>

#define _USE_MATH_DEFINES
#include <string>
#include <native/pipe.h>

class vrpn_Connection;

class Trackable
{
    public:
        Trackable(std::string name, vrpn_Connection *connection, vrpn_TRACKERCHANGEHANDLER changedCallback);
        Trackable(std::string name, vrpn_Connection *connection);
        ~Trackable(void);

    private:
        vrpn_Tracker_Remote* tracker;
        vrpn_TRACKERCHANGEHANDLER changedCallback;
        std::string name;
        struct timeval lastPacketTime;
        vrpn_float64 matrix_rot[3][3];
        vrpn_float64 quaternions_tab[4];
        vrpn_float64 euler_tab[3];
        vrpn_float64 position_tab[3];
        vrpn_float64 vitesse_tab[3];
        int nrt_pipe;
        RT_PIPE rt_pipe;
        RTIME last_rt_time;
        vrpn_float64 old_position[3];
        struct timeval last_time;
        bool init;

        void initialize(std::string name, vrpn_Connection *connection);
        void setQuaternions(vrpn_float64 quaternions[4]);
        void setMatrix(vrpn_float64 matrix[3][3]);
        void setEuler(vrpn_float64 euler[3]);
        void setPosition(vrpn_float64 position[3]);
        void setVitesse(vrpn_float64 position[3],struct timeval time);

    protected:
        void QtoM();
        void QtoE();

    public:
        RTIME Update();
        void mainloop();
        bool getShutup();
        void setShutup(bool flag);
        std::string getName();
        struct timeval getLastPacketTime();
        void setLastPacketTime(struct timeval time);

        vrpn_float64* getQuaternions();
        vrpn_float64*** getMatrix();
        void getMatrix(vrpn_float64 matrix[3][3]);

        //Euler angles getters
        vrpn_float64* getEuler();
        void getEuler(vrpn_float64* yaw,vrpn_float64* pitch,vrpn_float64* roll);
        void getEuler(float* yaw,float* pitch,float* roll);
        void getEuler(vrpn_float64* yaw,vrpn_float64* pitch,vrpn_float64* roll,struct timeval* time);

        //Position getters
        vrpn_float64* getPosition();
        void getPosition(float* x,float* y,float* z);
        void getPosition(vrpn_float64* x,vrpn_float64* y,vrpn_float64* z);
        void getPosition(vrpn_float64* x,vrpn_float64* y,vrpn_float64* z,struct timeval* time);
        void getVitesse(vrpn_float64* x,vrpn_float64* y,vrpn_float64* z);
        void getVitesse(float* x,float* y,float* z);

    public:
        static void	VRPN_CALLBACK handle_pos (void *userdata, const vrpn_TRACKERCB t);
};

#endif // TRACKABLE_H
