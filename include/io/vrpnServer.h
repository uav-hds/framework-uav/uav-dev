//  created:    2012/11/08
//  filename:   vrpnServer.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    objet se connectant au serveur vrpn
//
//
/*********************************************************************/


#ifndef VRPNSERVER_H
#define VRPNSERVER_H

#include <string>
#include <vector>
#include <pthread.h>
#include <vrpn_Connection.h>
#include "Trackable.h"
//#include <vrpn_Tracker.h>

class vrpnServer
{

    public:
        vrpnServer(std::string address,int us_period);
        ~vrpnServer();
        Trackable* AddTrackable(std::string name);
        void Start(void);


    private:
        pthread_t thread;
        bool continuer;
        int timeout;
        std::vector<Trackable*> trackables;
        vrpn_Connection *connection;
        static void* loop(void * arg);

};

#endif // VRPNSERVER_H
