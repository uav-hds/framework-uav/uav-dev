#ifndef IO_H
#define IO_H

#define BLCTRV1_BROKEN 1
#define BLCTRV2 2
#define BLCTRV2_TEMPO 3

#include <typedef.h>
#include <string>

//fonctions I2C
void init_i2c(char* filename);
void close_i2c(void);
int nb_motors();
void set_motors(uint16* moteurs,int nb_moteurs);
void stop_pwms(void);
//range*43+43=dist max en mm
void init_us(uint8 range,uint8 max_gain);
int start_capture_us(void);
float get_value_us();

#endif // IO_H
