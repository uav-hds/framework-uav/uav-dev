//  created:    2011/08/19
//  filename:   3dmgx3.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   objet integrant la centrale 3dmgx3
//
//
/*********************************************************************/

#ifndef GX3_H
#define GX3_H

#include "typedef.h"
#include "rt_thread.h"
#include <rtdm/rtserial.h>
#include <native/timer.h>
#include <native/mutex.h>

#define EULER_ANGULAR 0xcf
#define ACC_ANGULAR_MATRIX 0xc8

class gx3 :  public rt_thread
{

    public:
        gx3(std::string sensor_name,std::string fd_name,unsigned char command_type=EULER_ANGULAR);
        ~gx3();
        float roll();
        float pitch();
        float yaw();
        float droll();
        float dpitch();
        float dyaw();
        float ax();
        float ay();
        float az();
        RTIME time(void);
        bool IsStarted();

    private:
        float roll_f,pitch_f,yaw_f,droll_f,dpitch_f,dyaw_f,ax_f,ay_f,az_f;
        int data_rate,gyro_acc_size,mag_size,up_comp,north_comp;
        bool coning,disable_magn,disable_north_comp,disable_grav_comp;
        int uart_fd;
        bool is_started;
        RT_MUTEX gx3_mutex;
        void update_imu(void);
        void gyros_bias(void);
        void sampling_settings(void);
        void SetBaudrate(int baudrate);
        void DeviceBaudrate(int value);
        void DeviceReset(void);
        bool calc_checksum(uint8 *buf,int size);
        void SetRxTimeout(RTIME timeout_ns);
        void FlushInput(void);
        RTIME imu_time;

        int get_firmware_number(void);

        void run(void);

        unsigned char command;
};

#endif // GX3_H
