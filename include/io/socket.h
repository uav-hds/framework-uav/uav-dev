//  created:    2013/03/20
//  filename:   socket.h
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:    classe implementant une socket udt
//
//
/*********************************************************************/
#ifndef SOCKET_H
#define SOCKET_H

#include <string>
#include <typedef.h>

class udt_socket_impl;

class udt_socket
{

    public:
        udt_socket(std::string name,int port);
        ~udt_socket();
        void send_sock(uint8* buf,int32 size);
        int32 read_sock(uint8* buf,int32 buf_size);//buf must be of buf_size size
        void debug(const char * format, ...);

    private:
        class udt_socket_impl* pimpl_;

};

#endif // SOCKET_H
