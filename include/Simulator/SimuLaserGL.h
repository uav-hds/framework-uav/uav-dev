/*!
 * \file SimuUsGL.h
 * \brief Class for a simulation us
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/02/07
 * \version 3.4
 */

#ifndef SIMULASERGL_H
#define SIMULASERGL_H

#include <SimuLaser.h>
#include <SensorGL.h>

namespace framework
{
    namespace gui
    {
        class DoubleSpinBox;
        class Vector3DSpinBox;
    }
}

namespace framework
{
    namespace simulator
    {
        class Model;
    }
}

namespace framework
{
namespace sensor
{
    /*! \class SimuUsGL
    * \brief Class for a simulation us
    *
    */
    class SimuLaserGL : public SimuLaser, public SensorGL
    {
        public:
            SimuLaserGL(const simulator::Model* parent,std::string name,int dev_id);
            ~SimuLaserGL();

        private:
            void UpdateFrom(const core::io_data *data);
            gui::DoubleSpinBox *range;
            gui::Vector3DSpinBox *position,*direction;
    };
} // end namespace sensor
} // end namespace framework
#endif // SIMULASERGL_H
