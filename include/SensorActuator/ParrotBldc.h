/*!
 * \file ParrotBldc.h
 * \brief Class for Ardrone 2 Bldc
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/12/19
 * \version 4.0
 */

#ifndef PARROTBLDC_H
#define PARROTBLDC_H

#include "Bldc.h"

namespace framework
{
    namespace core
    {
        class FrameworkManager;
        class cvmatrix;
        class SerialPort;
    }
    namespace gui
    {
        class Layout;
    }
}

class ParrotBldc_impl;

namespace framework
{
namespace actuator
{
    /*! \class ParrotBldc
    *
    * \brief Class for Ardrone 2 Bldc
    */
    class ParrotBldc : public Bldc
    {
        public:
            /*!
            * \brief Constructor
            *
            * Construct a ParrotBldc.
            *
            * \param parent parent
            * \param layout layout
            * \param name name
            * \param serialport serialport
            */
            ParrotBldc(const core::IODevice* parent,gui::Layout* layout,std::string name,core::SerialPort* serialport);

            /*!
            * \brief Destructor
            *
            */
            ~ParrotBldc();

            /*!
            * \brief Has speed measurement
            *
            * Reimplemented from Bldc. \n
            *
            * \return true if it has speed measurement
            */
            bool HasSpeedMeasurement(void) const{return false;};

            /*!
            * \brief Has current measurement
            *
            * Reimplemented from Bldc. \n
            *
            * \return true if it has current measurement
            */
            bool HasCurrentMeasurement(void) const{return false;};

        private:
            /*!
            * \brief Set motors values
            *
            * Reimplemented from Bldc. \n
            * Values size must be the same as MotorsCount()
            *
            * \param values motor values
            */
            void SetMotors(float* value);

            class ParrotBldc_impl* pimpl_;
    };
} // end namespace actuator
} // end namespace framewor
#endif // PARROTBLDC_H
