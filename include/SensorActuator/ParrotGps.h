/*!
 * \file ParrotGps.h
 * \brief Class for Ardrone 2 gps receiver
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/06/04
 * \version 4.0
 */

#ifndef PARROTGPS_H
#define PARROTGPS_H

#include <Thread.h>
#include <Gps.h>

namespace framework
{
    namespace core
    {
        class FrameworkManager;
        class SerialPort;
    }
}

namespace framework
{
namespace sensor
{
    /*! \class ParrotGps
    *
    * \brief Class for Ardrone 2 gps receiver
    */
    class ParrotGps : public core::Thread, public Gps
    {
        public:
            /*!
            * \brief Constructor
            *
            * Construct a Novatel.
            *
            * \param parent parent
            * \param name name
            * \param serialport serialport
            * \param NMEAFlags NMEA sentances to enable
            * \param priority priority of the Thread
            */
            ParrotGps(const core::FrameworkManager* parent,std::string name,core::SerialPort *serialport,Gps::NMEAFlags_t NMEAFlags,uint8_t priority);

            /*!
            * \brief Destructor
            *
            */
            ~ParrotGps();

        private:
            /*!
            * \brief Update using provided datas
            *
            * Reimplemented from IODevice.
            *
            * \param data data from the parent to process
            */
            void UpdateFrom(const core::io_data *data){};

            /*!
            * \brief Run function
            *
            * Reimplemented from Thread.
            *
            */
            void Run(void);

            core::SerialPort *serialport;
            void Sync(void);
    };
} // end namespace sensor
} // end namespace framewor
#endif // PARROTGPS_H
