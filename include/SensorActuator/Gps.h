/*!
 * \file Gps.h
 * \brief Base class for GPS
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2013/08/23
 * \version 4.0
 */

#ifndef GPS_H
#define GPS_H

#include <IODevice.h>
#include <nmea/nmea.h>

namespace framework
{
    namespace core
    {
        class cvmatrix;
        class FrameworkManager;
        class GeoCoordinate;
        class Vector3D;
    }
    namespace gui
    {
        class Layout;
        class DataPlot1D;
        class Tab;
        class TabWidget;
        class PushButton;
        class Map;
        class Label;
    }
}

namespace framework
{
namespace sensor
{
    /*! \class Gps
    *
    * \brief Base class for GPS
    */
    class Gps : public core::IODevice
    {
        public:
            /*!
            \enum FixQuality_t
            \brief Fix qualty indicators
            */
            enum class FixQuality_t {
                Invalid=0,/*!< invalid */
                Gps=1,/*!< Gps */
                DGps=2,/*!< Differential Gps */
                Pps=3,/*!< Pps */
                Rtk=4,/*!< RTK */
                RtkFloat=5,/*!< RTK float */
                Estimated=6,/*!< Estimated */
                Manual=7,/*!< Manual */
                Simulation=8,/*!< Simulation */
                };

            /*!
            \enum NMEAFlags_t
            \brief NMEA flags
            */
            enum NMEAFlags_t {
                GGA=0x01,/*!< GGA */
                VTG=0x02,/*!< VTG */
                GST=0x04,/*!< GST */
                };

            /*!
            * \brief Constructor
            *
            * Construct a Gps.
            *
            * \param parent parent
            * \param name name
            * \param NMEAFlags NMEA sentances to enable
            */
            Gps(const core::FrameworkManager* parent,std::string name,NMEAFlags_t NMEAFlags);

            /*!
            * \brief Destructor
            *
            */
            ~Gps();

            /*!
            * \brief Use default plot
            *
            */
            void UseDefaultPlot(void);

            /*!
            * \brief East plot
            *
            * \return east plot
            */
            gui::DataPlot1D* EPlot(void) const;

            /*!
            * \brief North plot
            *
            * \return north plot
            */
            gui::DataPlot1D* NPlot(void) const;

            /*!
            * \brief Up plot
            *
            * \return up plot
            */
            gui::DataPlot1D* UPlot(void) const;

            /*!
            * \brief East velocity plot
            *
            * \return east velocity plot
            */
            gui::DataPlot1D* VEPlot(void) const;

            /*!
            * \brief North velocity plot
            *
            * \return north velocity plot
            */
            gui::DataPlot1D* VNPlot(void) const;

            /*!
            * \brief Main tab
            *
            * \return main tab
            */
            gui::TabWidget* GetTab(void) const;

            /*!
            * \brief Setup Layout
            *
            * \return setup Layout
            */
            gui::Layout* GetLayout(void) const;

            /*!
            * \brief Plot tab
            *
            * \return plot Tab
            */
            gui::Tab* GetPlotTab(void) const;

            /*!
            * \brief Number of used satellites
            *
            * \return number of used satellites
            */
            uint16_t NbSat(void) const;

            /*!
            * \brief Fix Quality
            *
            * \return fix quality
            */
            FixQuality_t FixQuality(void) const;

            /*!
            * \brief Set reference for ENU coordinates
            *
            * The actual position is used as reference to calculate
            * ENU coordinates.
            *
            * \return fix quality
            */
            void SetRef(void);

            /*!
            * \brief Get ENU position
            *
            * \param point to store position
            */
            void GetENUPosition(core::Vector3D *point);

        protected:
            /*!
            * \brief Parse a NMEA frame
            *
            * This function must be called by the reimplemented class. \n
            * When a frame is parsed, GPS datas are filled.
            *
            * \param frame NMEA frame
            * \param frame_size frame size
            *
            */
            void parseFrame(const char *frame, int frame_size);

            NMEAFlags_t NMEAFlags;

        protected:
            core::GeoCoordinate *position;

        private:
            /*!
            * \brief Update using provided datas
            *
            * Reimplemented from IODevice.
            *
            * \param data data from the parent to process
            */
            void UpdateFrom(const core::io_data *data){};

            gui::Tab *main_tab,*sensor_tab;
            gui::TabWidget* tab;
            gui::PushButton *button_ref;
            gui::DataPlot1D* e_plot;
            gui::DataPlot1D* n_plot;
            gui::DataPlot1D* u_plot;
            gui::DataPlot1D* ve_plot;
            gui::DataPlot1D* vn_plot;
            gui::Tab* plot_tab;
            gui::Map *map;
            gui::Label *nb_sat_label,*fix_label;
            uint16_t nb_sat;
            FixQuality_t fix;
            bool take_ref;
            nmeaINFO info;
            nmeaPARSER parser;
            nmeaGPGGA pack;
            nmeaPOS pos;
            double lat_ref,long_ref,alt_ref;

            //matrix
            core::cvmatrix *output;
    };
} // end namespace sensor
} // end namespace framewor
#endif // GPS_H
