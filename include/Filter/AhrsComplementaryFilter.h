/*!
 * \file AhrsComplementaryFilter.h
 * \brief Class defining an Ahrs Kalman filter
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/01/15
 * \version 4.0
 */

#ifndef AHRSCOMPLEMENTARYFILTER_H
#define AHRSCOMPLEMENTARYFILTER_H

#include <Ahrs.h>
#include <Vector3D.h>
#include <Quaternion.h>

namespace framework {
    namespace sensor {
        class Imu;
    }
    namespace gui {
        class DoubleSpinBox;
    }
}

namespace framework { namespace filter {
    /*! \class AhrsComplementaryFilter
    *
    * \brief Class defining an Ahrs complementary filter
    *
    * Implementation from Augustin Manecy
    */
    class AhrsComplementaryFilter : public Ahrs {
        public:
            /*!
            * \brief Constructor
            *
            * Construct an AhrsComplementaryFilter.
            *
            * \param parent parent
            * \param name name
            */
            AhrsComplementaryFilter(const sensor::Imu* parent,std::string name);

            /*!
            * \brief Destructor
            *
            */
            ~AhrsComplementaryFilter();

        private:
            /*!
            * \brief Update using provided datas
            *
            * Reimplemented from IODevice.
            *
            * \param data data from the parent to process
            */
            void UpdateFrom(const core::io_data *data);

            core::Time previous_time;

            bool isInit;
            core::Quaternion QHat;
            core::Vector3D BHat;
            gui::DoubleSpinBox *ka[3];
            gui::DoubleSpinBox *kb[3];
    };
} // end namespace filter
} // end namespace framework
#endif // AHRSCOMPLEMENTARYFILTER_H
