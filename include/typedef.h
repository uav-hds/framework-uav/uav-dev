#ifndef TYPEDEF_H
#define TYPEDEF_H

//----------------------------------------------------------------------//
//  MAths definitions                                                   //
//----------------------------------------------------------------------//
#define RAD_TO_DEG(x)		((x)*180.0f/PI)	/*!< Convert a radian value into a degree value */
#define DEG_TO_RAD(x)		((x)/180.0f*PI)	/*!< Convert a degree value into a radian value */
#define PI ((float)3.14159265358979323846)
#define PI_D ((double)3.14159265358979323846)


typedef unsigned char uint8;
typedef signed char int8;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned int uint32;
typedef int int32;


#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
/*
#ifndef __XENO__
#include <pthread.h>
typedef unsigned long long RTIME;
typedef pthread_mutex_t RT_MUTEX;
#endif
*/
#endif // TYPEDEF_H
