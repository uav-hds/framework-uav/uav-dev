//  created:    2016/02/05
//  filename:   UavFactory.cpp
//
//  author:     Guillaume Sanahuja
//              Copyright Heudiasyc UMR UTC/CNRS 7253
//
//  version:    $Id: $
//
//  purpose:   construct a Uav based on the type name
//
//
/*********************************************************************/

#ifndef UAVFACTORY
#define UAVFACTORY

#include <Uav.h>

framework::meta::Uav* CreateUav(framework::core::FrameworkManager* parent,std::string uav_name,std::string uav_type,framework::filter::UavMultiplex *multiplex=NULL);

#endif // UAVFACTORY
