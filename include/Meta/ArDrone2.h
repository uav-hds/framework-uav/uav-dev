/*!
 * \file ArDrone2.h
 * \brief Class defining an ardrone2 uav
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2014/06/10
 * \version 4.0
 */

#ifndef ARDRONE2_H
#define ARDRONE2_H

#include "Uav.h"

namespace framework {
    namespace core {
        class FrameworkManager;
    }
    namespace sensor {
        class ParrotNavBoard;
    }
    namespace filter {
        class UavMultiplex;
    }
}


namespace framework
{
namespace meta
{
    /*! \class ArDrone2
    *
    * \brief Class defining an ardrone2 uav
    */
    class ArDrone2 : public Uav {
        public:
            ArDrone2(core::FrameworkManager* parent,std::string uav_name,filter::UavMultiplex *multiplex=NULL);
            ~ArDrone2();
            void StartSensors(void);

        private:
            sensor::ParrotNavBoard *navboard;
    };
} // end namespace meta
} // end namespace framework
#endif // ARDRONE2_H
