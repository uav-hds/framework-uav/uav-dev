/*!
 * \file HdsX8.h
 * \brief Class defining an ardrone2 uav
 * \author Guillaume Sanahuja, Copyright Heudiasyc UMR UTC/CNRS 7253
 * \date 2016/02/08
 * \version 4.0
 */

#ifndef HDSX8_H
#define HDSX8_H

#include "Uav.h"

namespace framework
{
namespace meta
{
    /*! \class HdsX8
    *
    * \brief Class defining an ardrone2 uav
    */
    class HdsX8 : public Uav {
        public:
            HdsX8(core::FrameworkManager* parent,std::string uav_name,filter::UavMultiplex *multiplex=NULL);
            ~HdsX8();
            void StartSensors(void);

        private:

    };
} // end namespace meta
} // end namespace framework
#endif // HDSX8_H
