#! /bin/bash

# $1 is the drone ip
if [ "$#" -ne 1 ] ; then
  echo "Usage: $0 drone_ip_address"
  exit 1
fi

. $IGEP_ROOT/uav_dev/bin/noarch/ubuntu_cgroup_hack.sh

#we must run as root
if [ $EUID -ne 0 ]; then
  exec sudo -E $0 $*
fi 

./dualshock3 -a ${1}:20000 -c usb -t 100
