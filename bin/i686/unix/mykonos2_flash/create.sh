#!/bin/sh

KERNEL_ZIMAGE=$ARDRONE2_ROOT/ardrone2_src/linux-ardrone2-2.6.32-9/arch/arm/boot/zImage
KERNEL_CONFIG=$ARDRONE2_ROOT/ardrone2_src/linux-ardrone2-2.6.32-9/.config
ROOT_DIRECTORY=$ARDRONE2_ROOT/rootfs
BOARD_NAME=mykonos2

export PATH=$PATH:$PWD/bin

PWD=$(pwd -P)
IMAGE_FILE_PLF=$PWD/image_file.plf
PLFTOOL=$PWD/bin/plftool
MK_KERNEL_PLF=$PWD/bin/mk_kernel_plf
FIXSTAT_DO=$PWD/bin/fixstat.py
PINST_BUILD=$PWD/bin/pinst_build
PINST_USB=$PWD/bin/pinst_usb
PFLASHER=$PWD/bin/pflasher
BOOT_CFG=$PWD/boot.cfg
U_CONF=$PWD/bin/static_uconf
INSTALLER=$PWD/bin/mykonos2_lucie_installer_installer.plf

#CMDLINE="mtdparts=omap2-nand.0:512K(Pbootloader),8M(Pmain_boot),8M(Pfactory),32M(Psystem),81408K(Pupdate) console=ttyO3,115200 loglevel=8 ubi.mtd=Pfactory,2048 ubi.mtd=Psystem,2048 ubi.mtd=Pupdate,2048 root=ubi1:system rootfstype=ubifs parrot5.low_latency=1"
CMDLINE="mtdparts=omap2-nand.0:512K(Pbootloader),8M(Pmain_boot),8M(Pfactory),32M(Psystem),81408K(Pupdate) console=ttyO3,115200 loglevel=8 earlyprintk=ttyO3,115200 ubi.mtd=Pfactory,2048 ubi.mtd=Psystem,2048 ubi.mtd=Pupdate,2048 root=/dev/sda1 parrot5.low_latency=1 rootwait mem=80M"

# Clean
rm $IMAGE_FILE_PLF
rm kernel.plf

echo "Create kernel.plf"
echo "P_APP=PA_MYKONOS2" > $BOOT_CFG
echo "CMDLINE='$CMDLINE'" >> $BOOT_CFG

if [ -f "$KERNEL_ZIMAGE" ]; then
    $MK_KERNEL_PLF  $BOOT_CFG  $KERNEL_ZIMAGE \
				$KERNEL_CONFIG kernel.plf
		PLF_ARGS="$PLF_ARGS -F p_type=PTY_UPDATE -F p_targ=PTA_OMAP3 -F p_app=PA_MYKONOS2"
		echo "add kernel"
		PLF_ARGS="$PLF_ARGS -j u_all -b u_data=kernel.plf"
		echo "Add bootloader"
		PLF_ARGS="$PLF_ARGS -j u_all -b u_bootloader=$PWD/bin/sys_bootldr.bin"
		echo "Add updater"
		PLF_ARGS="$PLF_ARGS -j u_all -b u_pinst=$INSTALLER"
		echo "Add kernel"
		$PLFTOOL $PLF_ARGS  $IMAGE_FILE_PLF  -j u_all -b u_conf=$U_CONF 
else
    echo "Image plf: no kernel image found";
fi
rm $BOOT_CFG
echo "kernel.plf done"

FIXSTAT="$FIXSTAT_DO \
    --user-file=$ROOT_DIRECTORY/etc/passwd \
    --group-file=$ROOT_DIRECTORY/etc/group \
"
		#--use-default"

cd $ROOT_DIRECTORY
echo "Create image plf"
find . ! -name '.' -printf '%P\n' | $FIXSTAT | \
			plfbatch '-a u_unixfile="&"' $IMAGE_FILE_PLF ;
cd -

echo "Image plf done -> $IMAGE_FILE_PLF"

echo "Flash"
sudo $PFLASHER $PWD/bin/inst_usb_bootldr.bin $INSTALLER $IMAGE_FILE_PLF
rm *.plf
