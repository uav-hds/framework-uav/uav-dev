#! /bin/bash
if [ $EUID -eq 0 ]; then
{
	cd "$( dirname "$0" )"
	cp -a wlan_uav /etc/NetworkManager/system-connections
	_uuid="$(uuidgen)"
	sed -i "s|<UUID>|${_uuid}|" /etc/NetworkManager/system-connections/wlan_uav
	chown root:root /etc/NetworkManager/system-connections/wlan_uav
	chmod 600 /etc/NetworkManager/system-connections/wlan_uav
	service network-manager reload
}
fi
