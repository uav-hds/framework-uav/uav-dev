#! /bin/bash

echo -n "Please wait until drone detection... "
# We start the adhoc wifi connection and then wait until we ping the drone
nmcli con up id WLAN_UAV > /dev/null
if [ $? -ne 0 ]; then
  echo "Adhoc local Wifi connection problem. Aborting."
  exit 1
fi
DRONE_DETECTED=1
until [ $DRONE_DETECTED -eq 0 ]; do
  ping -c1 -q $IP_DRONE > /dev/null
  DRONE_DETECTED=$?
done
echo "drone detected"
