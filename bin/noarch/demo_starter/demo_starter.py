#! /usr/bin/python
#GUI classes designed for PySide 1.1.1

from ui.wzd_demos import *
from ui.dlg_show_pix import *
import time
import subprocess
import re
import paramiko
import pxssh

class DroneDetection(QtCore.QThread):
	STATUS_OK = 0
	STATUS_CANT_ACTIVATE_WIFI = 1

	def __init__(self, _uav_demos):
		QtCore.QThread.__init__(self)
		self.uav_demos = _uav_demos
		self.stopRequired = False
		
	def run(self):
		self.state = DroneDetection.STATUS_OK
		#Raise wifi interface WLAN_UAV
		if subprocess.call("nmcli con up id wlan_uav --timeout 5 > /dev/null 2>&1", shell=True) != 0:
			self.state = DroneDetection.STATUS_CANT_ACTIVATE_WIFI
			return

		#Loop until we ping the drone
		while subprocess.call("ping -c1 -q " + Uav_demos.IP_DRONE + "> /dev/null", shell=True) != 0:
			if self.stopRequired == True:
				return
			else:
				time.sleep(1)
		return
		
	#Using this method is mandatory since no GUI handling should happen while thread runs
	def onFinished(self):
		if self.stopRequired:
			#We've been required to stop. Just do it!
			return
		if self.state == DroneDetection.STATUS_CANT_ACTIVATE_WIFI:
			msgBox = QtGui.QMessageBox()
			msgBox.setStandardButtons(QtGui.QMessageBox.Retry | QtGui.QMessageBox.Abort)
			msgBox.setDefaultButton(QtGui.QMessageBox.Retry)
			msgBox.setText("Could not set up wifi.")
			msgBox.setInformativeText("Please make sure no hardware button is blocking wifi on your laptop.")
			msgBox.setIcon(QtGui.QMessageBox.Warning)
			ret = msgBox.exec_()
			if ret == QtGui.QMessageBox.Retry:
				self.start()
			else: #cancel
				self.uav_demos.cleanupExit(Uav_demos.EXIT_AT_USER_REQUEST)
		else:
			#Update internal state and signal page completion
			self.uav_demos.droneDetected = True
			self.uav_demos.ui_wzd_demos.wzdpg_powerUp.completeChanged.emit()
			#Fill the progress bar
			self.uav_demos.ui_wzd_demos.progressBar_drone.setMaximum(1)
			self.uav_demos.ui_wzd_demos.progressBar_drone.setValue(1)

class PadDetection(QtCore.QThread):
	STATUS_NOT_DETECTED = 0
	STATUS_DETECTED = 1

	def __init__(self, _uav_demos):
		QtCore.QThread.__init__(self)
		self.uav_demos = _uav_demos
		self.stopRequired = True
		
	def run(self):
		self.state = PadDetection.STATUS_NOT_DETECTED

		#Loop until we see the pad
		while True:
			for BDADDR in subprocess.check_output("hcitool con | grep -e \"^[[:space:]]> ACL\" | cut -d\  -f3", shell=True).splitlines():
				for DEVNAME in subprocess.check_output("hcitool name " + BDADDR, shell=True).splitlines():
					if DEVNAME == "PLAYSTATION(R)3 Controller":
						self.state = PadDetection.STATUS_DETECTED
						return
			if self.stopRequired == True:
				return
			else:
				time.sleep(1)
		return
		
	#Using this method is mandatory since no GUI handling should happen while thread runs
	def onFinished(self):
		if self.stopRequired:
			#We've been required to stop. Just do it!
			return
		else:
			#Update internal state and signal page completion
			self.uav_demos.padDetected = True
			self.uav_demos.ui_wzd_demos.wzdpg_chooseDemo.completeChanged.emit()
			#Fill the progress bar
			self.uav_demos.ui_wzd_demos.progressBar_pad.setMaximum(1)
			self.uav_demos.ui_wzd_demos.progressBar_pad.setValue(1)
			
class BatteryVerif(QtCore.QThread):
	def __init__(self, _uav_demos):
		QtCore.QThread.__init__(self)
		self.uav_demos = _uav_demos
		self.batteryVoltage = 0.0
		self.stopRequired = False
		self.uav_demos.ui_wzd_demos.btn_retryBattery.setEnabled(False)
		
	def run(self):
		#Loop until we get the battery's voltage
		try:
			ssh = paramiko.SSHClient()
			ssh.load_system_host_keys()
			ssh.set_missing_host_key_policy( paramiko.AutoAddPolicy() )
			ssh.connect(self.uav_demos.IP_DRONE, username='root', password='')
			while self.batteryVoltage == float(0.0):
				if self.stopRequired:
					ssh.close()
					return
				stdin, stdout, stderr = ssh.exec_command("voltage")
				self.batteryVoltage = float(re.sub('[\nV]', '', stdout.read()))
		finally:
			ssh.close()
		return
		
	#Using this method is mandatory since no GUI handling should happen while thread runs
	def onFinished(self):
		if self.stopRequired:
			#We've been required to stop. Just do it!
			return
		else:
			self.uav_demos.batteryVoltage = self.batteryVoltage
			#Display the value
			self.uav_demos.ui_wzd_demos.btn_retryBattery.setEnabled(True)
			self.uav_demos.ui_wzd_demos.txtBatteryVoltage.setText(str(self.batteryVoltage)+'V')
			if self.batteryVoltage < 11.0:
				self.uav_demos.ui_wzd_demos.lbl_batteryIcon.setPixmap("./pix/icons/indicatorRed.png")
				msgBox = QtGui.QMessageBox()
				msgBox.setText("Battery too low")
				msgBox.setIcon(QtGui.QMessageBox.Warning)
				msgBox.setInformativeText("The drone battery voltage is too low, please replace it with a fully charged one.")
				msgBox.exec_()
			else:
				if self.batteryVoltage < 11.5:
					self.uav_demos.ui_wzd_demos.lbl_batteryIcon.setPixmap("./pix/icons/indicatorYellow.png")
				else:
					self.uav_demos.ui_wzd_demos.lbl_batteryIcon.setPixmap("./pix/icons/indicatorGreen.png")
					#if self.uav_demos.thrd_groundStation.isRunning() == False:
						#self.uav_demos.thrd_groundStation.start()
					if self.uav_demos.thrd_droneController.isRunning() == False:
						self.uav_demos.thrd_droneController.start()
					
				#Update internal state and signal page completion
				self.uav_demos.isBatteryChecked = True
				self.uav_demos.ui_wzd_demos.wzdpg_startDemo.completeChanged.emit()
			self.uav_demos.isBatteryChecked = True

class DroneController(QtCore.QThread):
	def __init__(self, _uav_demos):
		QtCore.QThread.__init__(self)
		self.uav_demos = _uav_demos
		self.stopRequired = False
		return
	   
	def run(self):
		client = paramiko.SSHClient()
		client.load_system_host_keys()
		client.connect('192.168.6.1',username='root',password='')
		channel = client.invoke_shell()
		channel.send('cd demo\n./demo_fo.sh\n')
		while True:
			if self.stopRequired:
				return
			while channel.recv_ready():
				if self.stopRequired:
					return
				self.uav_demos.ui_wzd_demos.boxConsole.insertPlainText(channel.recv(2048))
				self.uav_demos.ui_wzd_demos.boxConsole.ensureCursorVisible ()
		
	#Using this method is mandatory since no GUI handling should happen while thread runs
	def onFinished(self):
		client.close()
		return
			
class GroundStation(QtCore.QThread):
	def __init__(self, _uav_demos):
		QtCore.QThread.__init__(self)
		self.uav_demos = _uav_demos
		return
	   
	def run(self):
		subprocess.call("gksudo /home/cesar/igep/uav_dev/bin/x86_64/unix/station_sol_fo",shell=True)
			
		
	#Using this method is mandatory since no GUI handling should happen while thread runs
	def onFinished(self):
		return
			

class Uav_demos(QtGui.QApplication):
	IP_DRONE = "192.168.6.1" #as of 13/04/10
	#IP_DRONE = "127.0.0.1"

	EXIT_NO_WIFI = 1
	EXIT_AT_USER_REQUEST = 2
	EXIT_BATTERY_TOO_LOW = 3

	def __init__(self, argv):
		QtGui.QApplication.__init__(self, argv)
		self.droneDetected = False
		self.padDetected = False
		self.isBatteryChecked = False
		self.batteryVoltage = 0.0

		#Check prerequisite
		status = self.checkPrerequisites()
		if (status != 0):
			sys.exit(status)

		#Wizard
		self.wzd_demos = QtGui.QWizard()
		self.ui_wzd_demos = Ui_wzd_demos()
		self.ui_wzd_demos.setupUi(self.wzd_demos)

		#Dialog showFacilitiesMap
		self.dlg_show_pix = QtGui.QDialog()
		self.ui_dlg_show_pix = Ui_dlg_show_pix()
		self.ui_dlg_show_pix.setupUi(self.dlg_show_pix)
		self.dlg_show_pix.setModal(True)

		#Rendering logic
		self.setupRl()

		#Drone detection thread
		self.thrd_droneDetection = DroneDetection(self)
		self.thrd_droneDetection.finished.connect(self.thrd_droneDetection.onFinished)

		#Pad detection thread
		self.thrd_padDetection = PadDetection(self)
		self.thrd_padDetection.finished.connect(self.thrd_padDetection.onFinished)
		
		#Battery voltage getter thread
		self.thrd_batteryVerif = BatteryVerif(self)
		self.thrd_batteryVerif.finished.connect(self.thrd_batteryVerif.onFinished)
		
		#Ground station thread
		self.thrd_groundStation = GroundStation(self)
		self.thrd_groundStation.finished.connect(self.thrd_groundStation.onFinished)
		
		#Drone controller thread
		self.thrd_droneController = DroneController(self)
		self.thrd_droneController.finished.connect(self.thrd_droneController.onFinished)

		self.wzd_demos.show()

	#Exit app after clean up
	def cleanupExit(self, ret):
		# exit drone detection thread if still running
		if self.thrd_droneDetection.isRunning():
			self.thrd_droneDetection.stopRequired=True
			self.thrd_droneDetection.wait()
		# exit battery ground station thread if still running	
		if self.thrd_groundStation.isRunning():
			self.thrd_groundStation.terminate()
			self.thrd_groundStation.wait()
		# exit pad detection thread if still running
		if self.thrd_padDetection.isRunning():
			self.thrd_padDetection.stopRequired=True
			self.thrd_padDetection.wait()
		# exit battery voltage getter thread if still running
		if self.thrd_batteryVerif.isRunning():
			self.thrd_batteryVerif.stopRequired=True
			self.thrd_batteryVerif.wait()
		# exit drone controller thread if still running
		if self.thrd_droneController.isRunning():
			self.thrd_droneController.stopRequired=True
			self.thrd_droneController.wait()
			
		#switch off drone wifi connection (network manager deamon will reactivate default wifi connection)
		subprocess.call("LC_ALL=C nmcli con down id WLAN_UAV > /dev/null 2>&1", shell=True)
		
		#Restart bluetooth deamon to ensure pad connection is closed
		#subprocess.call("gksudo service bluetooth restart", shell=True)
		
		
		sys.exit(ret)

	#Check prerequisistes
	def checkPrerequisites(self):
		msgBox = QtGui.QMessageBox()

		#Is NetworkManager installed?
		if subprocess.call("dpkg -s network-manager | grep Status | grep installed > /dev/null 2>&1", shell=True) != 0:
			msgBox.setText("The \"network-manager\" package is missing and will be installed.")
			msgBox.exec_()
			subprocess.call("gksudo apt-get install network-manager", shell=True)

		#Is there a Wifi interface?
		if subprocess.call("LC_ALL=C nmcli -t -f GENERAL -m tabular dev list | grep '802-11-wireless' > /dev/null 2>&1", shell=True) != 0:
			msgBox.setText("No wifi interface found. Will now exit.")
			msgBox.exec_()
			return self.EXIT_NO_WIFI

		#Is a wifi connection WLAN_UAV set?
		if subprocess.call("LC_ALL=C nmcli -f all con list id WLAN_UAV > /dev/null 2>&1", shell=True) != 0:
			msgBox.setText("A wifi connexion must be configured in order to communicate with the drone. "
			"This is needed only once. Your password may be required.")
			msgBox.exec_()
			subprocess.call("gksudo ./bash/add_WLAN_UAV.sh", shell=True)

		return 0


	#Rendering logic
	def setupRl(self):
		self.ui_wzd_demos.btn_showFacilitiesMap.clicked.connect(self.on_btn_showFacilitiesMap_clicked)
		self.ui_wzd_demos.btn_showRoomMap.clicked.connect(self.on_btn_showRoomMap_clicked)
		self.ui_wzd_demos.btn_showFlyingRoom.clicked.connect(self.on_btn_showFlyingRoom_clicked)
		self.ui_wzd_demos.btn_retryBattery.clicked.connect(self.on_btn_retryBattery_clicked)
		self.ui_wzd_demos.wzdpg_powerUp.isComplete = self.wzdpg_powerUp_isComplete
		self.ui_wzd_demos.wzdpg_powerUp.showEvent = self.wzdpg_powerUp_showEvent
		self.ui_wzd_demos.wzdpg_chooseDemo.isComplete = self.wzdpg_chooseDemo_isComplete
		self.ui_wzd_demos.wzdpg_chooseDemo.showEvent = self.wzdpg_chooseDemo_showEvent
		self.ui_wzd_demos.wzdpg_startDemo.isComplete = self.wzdpg_startDemo_isComplete
		self.ui_wzd_demos.wzdpg_startDemo.showEvent = self.wzdpg_startDemo_showEvent
		self.wzd_demos.rejected.connect(self.on_wzd_cancelled)
		
	def wzdpg_powerUp_isComplete(self):
		return self.droneDetected

	def wzdpg_powerUp_showEvent(self, event):
		#Surprisingly show events keep on being sent to the wizard page when the thread is started
		if (self.droneDetected == False) and (self.thrd_droneDetection.isRunning() == False):
			self.thrd_droneDetection.start()

	def wzdpg_chooseDemo_isComplete(self):
		return self.padDetected

	def wzdpg_chooseDemo_showEvent(self, event):
		#Surprisingly show events keep on being sent to the wizard page when the thread is started
		if (self.padDetected == False) and (self.thrd_padDetection.isRunning() == False):
			self.thrd_padDetection.start()
			
	def wzdpg_startDemo_isComplete(self):
		return self.isBatteryChecked

	def wzdpg_startDemo_showEvent(self, event):
		#Surprisingly show events keep on being sent to the wizard page when the thread is started
		if (self.isBatteryChecked== False) and (self.thrd_batteryVerif.isRunning() == False):
			self.thrd_batteryVerif.start()	 

	def on_btn_showFlyingRoom_clicked(self):
		pass

	def on_wzd_cancelled(self):
		self.cleanupExit(self.EXIT_AT_USER_REQUEST)
	
	def on_btn_retryBattery_clicked(self):
		if (self.thrd_batteryVerif.isRunning() == False):
			self.thrd_batteryVerif.start() 
		
	def on_btn_showFacilitiesMap_clicked(self):
		self.ui_dlg_show_pix.label.setPixmap("./pix/full/plan_royallieu.gif")
		self.dialogUpdate()
		self.dlg_show_pix.show()

	def on_btn_showRoomMap_clicked(self):
		self.ui_dlg_show_pix.label.setPixmap("./pix/320x240/room_B122.jpg")
		self.dialogUpdate()
		self.dlg_show_pix.show()
		
	def dialogUpdate(self):
		self.ui_dlg_show_pix.label.adjustSize()
		#These 2 lines are necessary to force scrollArea.sizeHint recalculation
		self.ui_dlg_show_pix.scrollArea.takeWidget()
		self.ui_dlg_show_pix.scrollArea.setWidget(self.ui_dlg_show_pix.label)
		self.ui_dlg_show_pix.scrollArea.adjustSize() #stick to new sizeHint
		#These 2 lines are necessary for the dialog to accept to change its size
		self.ui_dlg_show_pix.verticalLayout.removeWidget(self.ui_dlg_show_pix.scrollArea)
		self.ui_dlg_show_pix.verticalLayout.addWidget(self.ui_dlg_show_pix.scrollArea)
		self.dlg_show_pix.adjustSize()
		
if __name__ == "__main__":
	import sys
	app = Uav_demos(sys.argv)
	#Wait until main events loop is done
	sys.exit(app.exec_())

