- uav-demo à la dymo
- sudo "uav-demo"

=== Hardware ===
A) prendre le matériel dans la salle B122
  - (prévoir un plan)
  - manette
  - batteries ("Pro-Tronik") (en tout 4 batteries)
  - drone
  - multimètre

B) Manette
	-> détection manette et warning 'appariement non réussit'
	-> détection niveau batterie manette et warning 'batterie manette faible'

C) batteries
  tester si > 11V avec multimetre

D) drone
	allumer avec photo du bouton

-> ifconfig up réseau wifi ad-hoc WLAN_UAV (network_manager)
  /!\ Apparemment le wifi s'active automatiquement quand le drone est allumé
-> pinger 192.168.6.1 (/etc/hosts) X4a

lancer sur le drone:
/!\ pour ssh sans mdp -> mail Guillaume
- soit /home/root/bin/arm/demo_of.sh (optical flow)
- soit /home/root/bin/arm/demo_ligne.sh (suivi de ligne)
/!\ pas bouger le drone pdt 10s (calibration gyro)
	-> détection "calib gyros ok" dans sortie programme?

lancer sur l'ordi:
/!\ en sudo pour que la manette fonctionne
- soit uav_dev/bin/x86_64/unix/station_sol_fo
- soit uav_dev/bin/x86_64/unix/station_sol_ligne

pendant démo
-> monitoring batterie manette
	-> signal sonore sur PC
	-> atterissage forcé si niveau trop bas

-> Interdire le trim qd flux optique
-> 
