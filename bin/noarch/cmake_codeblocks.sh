DIR=build_$(uname -m)

function info ()
{
	echo -e "\033[32m$1\033[0m"
}

function warn ()
{
	echo -e "\033[33m$1\033[0m"
}

function err ()
{
	echo -e "\033[31m$1\033[0m"
	exit 1
}

if ! [ -f  ./CMakeLists.txt ]; then
	err "Current directory does not contain CMakeLists.txt"
fi

#recupere cmake
#if [ -f  ${OECORE_NATIVE_SYSROOT}/usr/bin/cmake ]; then
#	CMAKE_NATIVE=${OECORE_NATIVE_SYSROOT}/usr/bin/cmake
#else
#	err "cross toolchain is not installed, please read https://devel.hds.utc.fr/projects/igep/wiki/toolchain/install"
	CMAKE_NATIVE=cmake
#fi

#verifie l'existence du lien symbolique build
if [ -d build ];then
	if ! readlink build > /dev/null ; then
		#c'est un répertoire, on quitte pour ne rien effacer
		err "Erreur: build existe et est un répertoire; c'est sensé être un lien symbolique."
	fi
	if ! readlink build | grep -w $DIR >/dev/null; then
		warn "Attention, build pointait vers un autre répertoire."
	fi
	rm build
fi

#creation du repertoire
mkdir -p $DIR
#creation du lien symbolique
ln -s $DIR build

#creation project x86
info "*************  Creating project for x86 *************"
cd build
rm -f CMakeCache.txt
rm -rf CMakeFiles
CMAKE=$(eval "echo \"\$OECORE_CORE2_64_NATIVE_SYSROOT\"")/usr/bin/cmake
$CMAKE ../ -G "CodeBlocks - Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${OECORE_CMAKE_CORE2_64_TOOLCHAIN}
# -DCMAKE_BUILD_TYPE=Release 

#creation projet ARM
info "************* Creating project for ARM *************"
cd ..
mkdir -p build_arm
cd build_arm
rm -f CMakeCache.txt
rm -rf CMakeFiles
CMAKE=$(eval "echo \"\$OECORE_ARMV7A_NEON_NATIVE_SYSROOT\"")/usr/bin/cmake
$CMAKE ../ -G "CodeBlocks - Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=${OECORE_CMAKE_ARMV7A_NEON_TOOLCHAIN}
# -DCMAKE_BUILD_TYPE=Release
