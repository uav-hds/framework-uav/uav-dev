#!/bin/bash
ARCH_DIR=build_$(uname -m)
NB_THREADS=$(nproc)
from_scratch=no

function green_echo ()
{
	echo -e "\033[32m$1\033[0m"
}

function red_echo ()
{
	echo -e "\033[31m$1\033[0m"
}

function check_error ()
{
	if [ "$?" != "0" ]; then
		red_echo "Erreur, arret"
		exit 1
	fi
}

function sanity_check () {
	if [ -z $IGEP_ROOT ]; then
		red_echo "You must set the IGEP_ROOT environement variable"
		exit 1
	fi
}

function configure ()
{
	if [ "$from_scratch" = "yes" ]; then
		green_echo "Configuring $1/$2"
		cd $1/$2

		if [ -f cmake_codeblocks.sh ]; then
			green_echo "only configuring it for $(uname -m)"
			./cmake_codeblocks.sh > /dev/null
		else
			rm -f build_arm/CMakeCache.txt
			$IGEP_ROOT/uav_dev/bin/noarch/cmake_codeblocks.sh > /dev/null
		fi
	
		check_error
	else
		green_echo "Not configuring $1/$2"
	fi
}

function cross_compile ()
{
	if ! [ -f  $1/$2/cmake_codeblocks.sh ]; then
		green_echo "Cross compiling and installing $2"
		cd $1/$2/build_arm
		if [ "$from_scratch" = "yes" ]; then make clean > /dev/null; fi
		make -j$NB_THREADS > /dev/null
		check_error
		make install
	fi
}

function compile ()
{
	green_echo "Compiling and installing $2"
	cd $1/$2/$ARCH_DIR
	if [ "$from_scratch" = "yes" ]; then make clean > /dev/null; fi
	make -j$NB_THREADS > /dev/null
	check_error	
	make install
}

function compile_framework()
{
	for projects in station_sol Dbt2csv Framework SensorActuator Filter Meta Simulator Controller/DualShock3 NetworkToXbee; do
		configure $IGEP_ROOT/uav_lib/framework $projects
	   	compile $IGEP_ROOT/uav_lib/framework $projects
		cross_compile $IGEP_ROOT/uav_lib/framework $projects
	done
}

function compile_examples()
{
	for projects in ex_framework ex_kalman_framework; do
		configure $IGEP_ROOT/uav_ex $projects
	   	compile $IGEP_ROOT/uav_ex $projects
		cross_compile $IGEP_ROOT/uav_ex $projects
	done
	
	for projects in ex_opencv; do
		configure $IGEP_ROOT/uav_ex $projects
	   	compile $IGEP_ROOT/uav_ex $projects
		cross_compile $IGEP_ROOT/uav_ex $projects
	done 

	for projects in control simulator; do
		configure $IGEP_ROOT/uav_ex/ex_simulator_framework $projects
	   	compile $IGEP_ROOT/uav_ex/ex_simulator_framework $projects
		cross_compile $IGEP_ROOT/uav_ex/ex_simulator_framework $projects
	done

	for projects in simulator uav; do
		configure $IGEP_ROOT/uav_ex/demo_optitrack_framework $projects
	   	compile $IGEP_ROOT/uav_ex/demo_optitrack_framework $projects
		cross_compile $IGEP_ROOT/uav_ex/demo_optitrack_framework $projects
	done

	for projects in simulator uav; do
		configure $IGEP_ROOT/uav_ex/demo_fo_framework $projects
	   	compile $IGEP_ROOT/uav_ex/demo_fo_framework $projects
		cross_compile $IGEP_ROOT/uav_ex/demo_fo_framework $projects
	done

	for projects in simulator uav; do
		configure $IGEP_ROOT/uav_ex/demo_flotte_framework $projects
	   	compile $IGEP_ROOT/uav_ex/demo_flotte_framework $projects
		cross_compile $IGEP_ROOT/uav_ex/demo_flotte_framework $projects
	done

	for projects in uav; do
		configure $IGEP_ROOT/uav_ex/demo_ligne_framework $projects
	   	compile $IGEP_ROOT/uav_ex/demo_ligne_framework $projects
		cross_compile $IGEP_ROOT/uav_ex/demo_ligne_framework $projects
	done

	for projects in CustomReferenceAngles CustomTorques; do
		configure $IGEP_ROOT/uav_ex/skeletons_framework $projects
	   	compile $IGEP_ROOT/uav_ex/skeletons_framework $projects
		cross_compile $IGEP_ROOT/uav_ex/skeletons_framework $projects
	done
}

sanity_check

printf "Compile all from scratch [O/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "o" ] || [ "$answer" = "O" ]; then
	from_scratch=yes
fi

printf "Compile Framework [O/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "o" ] || [ "$answer" = "O" ]; then
	compile_framework
fi

printf "Compile Framework documentation [O/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "o" ] || [ "$answer" = "O" ]; then
	cd $IGEP_ROOT/uav_lib/framework
	doxygen Doxyfile.in
fi

printf "Compile examples [O/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "o" ] || [ "$answer" = "O" ]; then
	compile_examples
fi

exit 0
