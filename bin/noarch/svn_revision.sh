#usage svn_revision.sh dir file
#dir: directory to get svn revision
#file: output file to put revision

SVN_REV=$(svnversion $1)
TXT="#define SVN_REV \"svnversion of $1 is ${SVN_REV}\\n\""


if [ -e $2 ] && [ "$TXT" = "$(cat $2)" ] ; then 
	/bin/echo "$2 is up to date"
else
	/bin/echo $TXT > $2
fi
