#!/bin/bash
POKY_ROOT=${POKY_ROOT:-/home/poky/poky-12.0.1/}
POKY_SCRIPT=${POKY_SCRIPT:-${IGEP_ROOT}/igep_src/poky-dizzy-12.0.1/oe-init-build-env}

function green_echo ()
{
	echo -e "\033[32m$1\033[0m"
}

function red_echo ()
{
	echo -e "\033[31m$1\033[0m"
}

function check_error ()
{
	if [ "$?" != "0" ]; then
		red_echo "Erreur, arret"
		exit 1
	fi
}

function sanity_check () {
	if [ ! -d $POKY_ROOT ]; then
		red_echo "You must set the POKY_ROOT environement variable"
		exit 1
	fi

	if [ ! -f $POKY_SCRIPT ]; then
		red_echo "You must set the POKY_SCRIPT environement variable"
		exit 1
	fi
}


function compile_image()
{
	green_echo "Compilation image $1"
	export MACHINE=$1
	bitbake $2
	check_error
}

function compile_images()
{
	for images in uav overo pacpus airbox ardrone2; do
		compile_image $images core-image-base
	done

	compile_image ardrone2-installer core-image-minimal-mtdutils
}

function compile_sdk()
{
	green_echo "Compilation $1 sdk for $2"
	export MACHINE=$1
	export SDKMACHINE=$2
	bitbake meta-toolchain-framework-uav
	check_error
}

function compile_sdks()
{
	for host in x86_64 i686; do
		compile_sdk uav $host
	done

	compile_sdk genericx86-64 x86_64
	compile_sdk genericx86 i686
}

sanity_check

cd $POKY_ROOT
source $POKY_SCRIPT > /dev/null

printf "Compiler les images [O/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "o" ]; then
	compile_images
fi

printf "Compiler les sdks [O/n]?"
read answer

if [ "$answer" = "" ] || [ "$answer" = "o" ]; then
	compile_sdks
fi

exit 0
