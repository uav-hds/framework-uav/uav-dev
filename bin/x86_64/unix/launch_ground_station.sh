#! /bin/sh
#defaults options values
PORT=9000
NAME="ground_station"

usage() {
  USAGE="usage: $0 [-n name] [-p port] [-?]|[-h]"
  echo $USAGE;
  exit 1
}

while getopts h?n:p: OPT; do
  case $OPT in
  n)      NAME=$OPTARG;;
  p)      PORT=$OPTARG;;
  h|\?)     usage;;
  esac
done
shift `expr $OPTIND - 1`

#bail out in case unknown options remain
[ "$1" = "--" ] && usage 

export LD_LIBRARY_PATH="${OECORE_HOST_SYSROOT}/usr/lib:${OECORE_HOST_SYSROOT}/lib"
#export QT_QPA_PLATFORM_PLUGIN_PATH="${OECORE_HOST_SYSROOT}/usr/lib/qt5/plugins/platforms/"
#export QT_QPA_FONTDIR="${OECORE_HOST_SYSROOT}/usr/lib/fonts/"
./station_sol -n $NAME -p $PORT
