#!/bin/bash
echo Installation des paquets
#Installation des paquets requis
sudo apt-get update && sudo apt-get install dkms qt-sdk libxml2-dev libxxf86vm-dev kernel-package -y

echo Installation du noyau
#Installation du noyau
rm linux-headers-xenomai-*.deb linux-image-xenomai-*.deb
wget https://www.hds.utc.fr/uav/xenomai/linux-headers-xenomai-x86_64.deb
wget https://www.hds.utc.fr/uav/xenomai/linux-image-xenomai-x86_64.deb
sudo dpkg -i linux-headers-xenomai-x86_64.deb
sudo dpkg -i linux-image-xenomai-x86_64.deb

echo Configuration du noyau
#Configuration Xenomai
resultat=$( getent group xenomai )
if [[ -z "$resultat" ]] ; then
     groupadd xenomai
fi

if [[ $(groups | grep xenomai -c) > 0 ]]  ; then
     sudo usermod -G xenomai -a $(whoami)
fi
if [[ $(cat /etc/default/grub | grep xeno_nucleus.xenomai_gid= -c) > 0 ]]  ; then
cd /etc/default/
sudo echo "
--- grub.orig   2013-07-19 12:16:16.932269399 +0200
+++ grub        2013-07-19 12:17:42.472271170 +0200
@@ -8,7 +8,7 @@
 GRUB_HIDDEN_TIMEOUT_QUIET=true
 GRUB_TIMEOUT=10
 GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
-GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash\"
+GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash xeno_nucleus.xenomai_gid=$(getent group test | cut -d: -f3)\"
 GRUB_CMDLINE_LINUX=""
" > tmpPatchGrub.patch
     sudo patch -p0 < tmpPatchGrub.patch
     sudo rm tmpPatchGrub.patch
fi

echo Mise a jour de grub
#Mise a jour de grub
sudo update-grub2

echo Ajout de regles udev
#Ajout des regles udev
sudo echo -e "# real-time heap device (Xenomai:rtheap)\nKERNEL==\"rtheap\", MODE=\"0660\", GROUP==\"xenomai\"" > /etc/udev/rules.d/rtheap.rules
sudo echo -e "# real-time pipe devices (Xenomai:rtpipe)\nKERNEL==\"rtp[0-9]*\", MODE=\"0660\", GROUP=\"xenomai\"" > /etc/udev/rules.d/rtpipes.rules

echo Done !
