var classframework_1_1sensoractuator_1_1_gx3__25__imu =
[
    [ "Command", "classframework_1_1sensoractuator_1_1_gx3__25__imu.html#afd699144ce8bee45a1c87b8869ceb1f8", [
      [ "EulerAnglesandAngularRates", "classframework_1_1sensoractuator_1_1_gx3__25__imu.html#afd699144ce8bee45a1c87b8869ceb1f8aba20a080f7a8d6875e7977d818560cc6", null ],
      [ "AccelerationAngularRateandOrientationMatrix", "classframework_1_1sensoractuator_1_1_gx3__25__imu.html#afd699144ce8bee45a1c87b8869ceb1f8aee983c05e3c60118be96e93c465c8002", null ]
    ] ],
    [ "Gx3_25_imu", "classframework_1_1sensoractuator_1_1_gx3__25__imu.html#a36c6dfcbb7ad03aca847b63751219abc", null ],
    [ "~Gx3_25_imu", "classframework_1_1sensoractuator_1_1_gx3__25__imu.html#a3dd851f3ecc59ea502ca2a79a09f5da2", null ],
    [ "::Gx3_25_imu_impl", "classframework_1_1sensoractuator_1_1_gx3__25__imu.html#aced28a91da3d8a16423107ad05ba2c10", null ]
];