var classframework_1_1gui_1_1_data_plot =
[
    [ "Color_t", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1c", [
      [ "Red", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1ca0d829f4fcfb4f1bc4c40d0b627f72aa3", null ],
      [ "Blue", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1ca8fab133d2d6300bcc7aa43519ba80d31", null ],
      [ "Green", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1ca1531d101862a5ea9626c0b4d2f329d6d", null ],
      [ "Yellow", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1cad1b5c35eb68314a63fe481713e19306b", null ],
      [ "Black", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1caa6d29b92e3e478fa0225edd23d91fcde", null ],
      [ "White", "classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1cab521d0c7c10ef458143b70cf0df83028", null ]
    ] ],
    [ "DataPlot", "classframework_1_1gui_1_1_data_plot.html#a0293190926424ca9b2ce78bbfe5e32c5", null ],
    [ "~DataPlot", "classframework_1_1gui_1_1_data_plot.html#a4f66d787b74b780d1c11460f0c7a888f", null ],
    [ "AddDataToSend", "classframework_1_1gui_1_1_data_plot.html#a13333bf3cc8514fb1edc1991b6a8f923", null ]
];