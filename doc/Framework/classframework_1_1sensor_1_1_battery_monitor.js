var classframework_1_1sensor_1_1_battery_monitor =
[
    [ "BatteryMonitor", "classframework_1_1sensor_1_1_battery_monitor.html#a67104e712b1b02838e934b314bbd92c6", null ],
    [ "~BatteryMonitor", "classframework_1_1sensor_1_1_battery_monitor.html#a09e6ceffaa1f36a5c5440b0d48c9766d", null ],
    [ "IsBatteryLow", "classframework_1_1sensor_1_1_battery_monitor.html#a7dc52be9f5270a9c7e879a908cf938ce", null ],
    [ "SetBatteryValue", "classframework_1_1sensor_1_1_battery_monitor.html#abc1f4bb2bb538563f8b690aa76b199f5", null ],
    [ "GetVoltage", "classframework_1_1sensor_1_1_battery_monitor.html#a94958b7fa638911e0ef1d69562e50fc8", null ]
];