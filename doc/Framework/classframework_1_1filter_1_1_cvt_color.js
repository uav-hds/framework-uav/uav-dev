var classframework_1_1filter_1_1_cvt_color =
[
    [ "Conversion_t", "classframework_1_1filter_1_1_cvt_color.html#a380b8c490377265e5cd342c9307f6441", [
      [ "BGR", "classframework_1_1filter_1_1_cvt_color.html#a380b8c490377265e5cd342c9307f6441a2ad5640ebdec72fc79531d1778c6c2dc", null ],
      [ "GRAY", "classframework_1_1filter_1_1_cvt_color.html#a380b8c490377265e5cd342c9307f6441a48bf014c704c9eaae100a98006a37bf7", null ]
    ] ],
    [ "CvtColor", "classframework_1_1filter_1_1_cvt_color.html#a822b1378629697f79281ec5ccc36092a", null ],
    [ "~CvtColor", "classframework_1_1filter_1_1_cvt_color.html#abd63fec1b84d36c580db7b0a682daecd", null ],
    [ "Output", "classframework_1_1filter_1_1_cvt_color.html#af4de218ce7b4f8e55fff35b41583fd50", null ],
    [ "GetOutputDataType", "classframework_1_1filter_1_1_cvt_color.html#a9337580b3811be244ae34d345370d015", null ]
];