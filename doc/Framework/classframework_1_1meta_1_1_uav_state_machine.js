var classframework_1_1meta_1_1_uav_state_machine =
[
    [ "AltitudeMode_t", "classframework_1_1meta_1_1_uav_state_machine.html#a6e09026d2f2bd36f8305ab48ddf469ff", [
      [ "Manual", "classframework_1_1meta_1_1_uav_state_machine.html#a6e09026d2f2bd36f8305ab48ddf469ffae1ba155a9f2e8c3be94020eef32a0301", null ],
      [ "Custom", "classframework_1_1meta_1_1_uav_state_machine.html#a6e09026d2f2bd36f8305ab48ddf469ffa90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "OrientationMode_t", "classframework_1_1meta_1_1_uav_state_machine.html#a3f4723c18787f24ccda1fb13980bbb13", [
      [ "Manual", "classframework_1_1meta_1_1_uav_state_machine.html#a3f4723c18787f24ccda1fb13980bbb13ae1ba155a9f2e8c3be94020eef32a0301", null ],
      [ "Custom", "classframework_1_1meta_1_1_uav_state_machine.html#a3f4723c18787f24ccda1fb13980bbb13a90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "ThrustMode_t", "classframework_1_1meta_1_1_uav_state_machine.html#a5155ff97d1168cd2ae70479abf4d5b26", [
      [ "Default", "classframework_1_1meta_1_1_uav_state_machine.html#a5155ff97d1168cd2ae70479abf4d5b26a7a1920d61156abc05a60135aefe8bc67", null ],
      [ "Custom", "classframework_1_1meta_1_1_uav_state_machine.html#a5155ff97d1168cd2ae70479abf4d5b26a90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "TorqueMode_t", "classframework_1_1meta_1_1_uav_state_machine.html#ae7b46bff9e6e5724671f7c87b835deb1", [
      [ "Default", "classframework_1_1meta_1_1_uav_state_machine.html#ae7b46bff9e6e5724671f7c87b835deb1a7a1920d61156abc05a60135aefe8bc67", null ],
      [ "Custom", "classframework_1_1meta_1_1_uav_state_machine.html#ae7b46bff9e6e5724671f7c87b835deb1a90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "Event_t", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebc", [
      [ "EnteringFailSafeMode", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebcacbe7d94f9011b8b4837172c66eda611c", null ],
      [ "EnteringControlLoop", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebcaffd8e436a03fc288a339dc5799afca23", null ],
      [ "StartLanding", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebca0a0480d215decaf46260e3db52e4dc4c", null ],
      [ "FinishLanding", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebca0912358d703c9a28d93a2707b86161b2", null ],
      [ "Stopped", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebcac23e2b09ebe6bf4cb5e2a9abe85c0be2", null ],
      [ "TakingOff", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebca85916c5f3400cfa25d988f05b6736a94", null ],
      [ "EmergencyStop", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebcac2e719ff1493c7c49d2e5a780450b501", null ],
      [ "Stabilized", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebcafda22026db89cdc5e88b262ad9424b41", null ],
      [ "ZTrajectoryFinished", "classframework_1_1meta_1_1_uav_state_machine.html#a80b31b7d7abd7a66e859ae2c65564ebcac5bb3b0bbec39429565b3abce47e5436", null ]
    ] ],
    [ "UavStateMachine", "classframework_1_1meta_1_1_uav_state_machine.html#a5547e447de6472edbeddfe8f36ee83c9", null ],
    [ "~UavStateMachine", "classframework_1_1meta_1_1_uav_state_machine.html#a8fe4982599fbe98e2d8138f958e97144", null ],
    [ "GetAltitudeMode", "classframework_1_1meta_1_1_uav_state_machine.html#a58ba24c07f49c07518db07a270bba5fa", null ],
    [ "SetAltitudeMode", "classframework_1_1meta_1_1_uav_state_machine.html#a12733faeeb5105a3ca00aac21c9e12bc", null ],
    [ "GotoAltitude", "classframework_1_1meta_1_1_uav_state_machine.html#a78657285041d223a76852e8fc4dfc5d4", null ],
    [ "GetOrientationMode", "classframework_1_1meta_1_1_uav_state_machine.html#a506dcdfb30ccf9b4afb122d6b33ff235", null ],
    [ "SetOrientationMode", "classframework_1_1meta_1_1_uav_state_machine.html#af69a36a0a90194441fedd92fe447f56c", null ],
    [ "GetThrustMode", "classframework_1_1meta_1_1_uav_state_machine.html#a1351159d7db6bd6021612e51535a7a6b", null ],
    [ "SetThrustMode", "classframework_1_1meta_1_1_uav_state_machine.html#a05b32f8fdcae476b66d67713e496c94a", null ],
    [ "GetTorqueMode", "classframework_1_1meta_1_1_uav_state_machine.html#a06ff8de499c777937f49849b12a83b7a", null ],
    [ "SetTorqueMode", "classframework_1_1meta_1_1_uav_state_machine.html#a1c6961c63a886ff3fd18b9fb8e6a3fd3", null ],
    [ "GetCurrentQuaternion", "classframework_1_1meta_1_1_uav_state_machine.html#ad2e1831c28699d4f3e82cb2141da8e42", null ],
    [ "GetCurrentAngularSpeed", "classframework_1_1meta_1_1_uav_state_machine.html#add7bf2503f1a25d38d7f419863a7ff80", null ],
    [ "GetUav", "classframework_1_1meta_1_1_uav_state_machine.html#af6dce914ee556acf9bd8163bcb004625", null ],
    [ "Land", "classframework_1_1meta_1_1_uav_state_machine.html#af326e387ae6fbab284443cdb7057d9fd", null ],
    [ "TakeOff", "classframework_1_1meta_1_1_uav_state_machine.html#a914e57ea7f60abf2c731e3a31488a5c7", null ],
    [ "EmergencyStop", "classframework_1_1meta_1_1_uav_state_machine.html#af09d0d651ea9fc86328d654408243f5e", null ],
    [ "SignalEvent", "classframework_1_1meta_1_1_uav_state_machine.html#a8960dd56ece6550bd9b8a92be07adcfc", null ],
    [ "GetOrientation", "classframework_1_1meta_1_1_uav_state_machine.html#aa53793215d8e4dc676af214c866f5058", null ],
    [ "GetDefaultOrientation", "classframework_1_1meta_1_1_uav_state_machine.html#a1ab38c9be11307077e30e8351f5ffa8f", null ],
    [ "AltitudeValues", "classframework_1_1meta_1_1_uav_state_machine.html#afabdb1bf37d7bedd628bb7e3d3a949d4", null ],
    [ "EnterFailSafeMode", "classframework_1_1meta_1_1_uav_state_machine.html#a2ae5666e3fd8d6264b2d2e30c7afd393", null ],
    [ "ExitFailSafeMode", "classframework_1_1meta_1_1_uav_state_machine.html#aae2ff893e13b081190074d2cfe3d9a54", null ],
    [ "FailSafeAltitudeValues", "classframework_1_1meta_1_1_uav_state_machine.html#a91de677c99140af51dd32ba09d7be5e1", null ],
    [ "GetButtonsLayout", "classframework_1_1meta_1_1_uav_state_machine.html#a130804e0c530cdc728a37914b28f7252", null ],
    [ "ExtraSecurityCheck", "classframework_1_1meta_1_1_uav_state_machine.html#ac8662149101d68f7bff6dd274b6358c5", null ],
    [ "ExtraCheckJoystick", "classframework_1_1meta_1_1_uav_state_machine.html#aac35ed539221c62be70c0ddc13f87aa1", null ],
    [ "ExtraCheckPushButton", "classframework_1_1meta_1_1_uav_state_machine.html#ab187859ca392816243261cd80ef7dad9", null ],
    [ "GetDefaultReferenceAltitude", "classframework_1_1meta_1_1_uav_state_machine.html#a8c51b8efe6ebfd2e0b80af97f15de843", null ],
    [ "GetReferenceAltitude", "classframework_1_1meta_1_1_uav_state_machine.html#a162703e90ac3679878279e643d1cb52b", null ],
    [ "GetDefaultReferenceOrientation", "classframework_1_1meta_1_1_uav_state_machine.html#a45bdf9a14a13c8bd218628a0287684a8", null ],
    [ "GetReferenceOrientation", "classframework_1_1meta_1_1_uav_state_machine.html#aef6849bd301ece0a52fc896c47762a11", null ],
    [ "ComputeCustomTorques", "classframework_1_1meta_1_1_uav_state_machine.html#a3418f1f46c9729b2bc07b9719854e805", null ],
    [ "ComputeDefaultTorques", "classframework_1_1meta_1_1_uav_state_machine.html#a977cd10d9c53167f920be254331fbfd2", null ],
    [ "ComputeCustomThrust", "classframework_1_1meta_1_1_uav_state_machine.html#a01fa7edfd5d5a89a11226a46d4593fb3", null ],
    [ "ComputeDefaultThrust", "classframework_1_1meta_1_1_uav_state_machine.html#ae0b4d4a8dbc36ad38290ecfb9470beb3", null ],
    [ "AddDeviceToControlLawLog", "classframework_1_1meta_1_1_uav_state_machine.html#abb723909f8b40229adae864144006ddd", null ],
    [ "AddDataToControlLawLog", "classframework_1_1meta_1_1_uav_state_machine.html#a0da7f45b432b7da6fd27004bfdded413", null ],
    [ "GetJoystick", "classframework_1_1meta_1_1_uav_state_machine.html#af1b105a933e68ffc7ca4ef7e5d8bb2d4", null ],
    [ "setupLawTab", "classframework_1_1meta_1_1_uav_state_machine.html#add302f62323d280ad2f5513b1ce0cbb0", null ],
    [ "graphLawTab", "classframework_1_1meta_1_1_uav_state_machine.html#a4c580148337e716ec886da0eb921d956", null ]
];