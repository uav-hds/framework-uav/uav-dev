var classframework_1_1core_1_1_send_data =
[
    [ "SendData", "classframework_1_1core_1_1_send_data.html#ac850a4cfcbe761023d0dd9f4f68b2e88", null ],
    [ "~SendData", "classframework_1_1core_1_1_send_data.html#a4cc9949b1ac1d0c36569a8bc9b67127f", null ],
    [ "SendSize", "classframework_1_1core_1_1_send_data.html#adf23f3d8f74748d04b3d889e2d95f75b", null ],
    [ "SendPeriod", "classframework_1_1core_1_1_send_data.html#a116f08024a513aad422921914fdac19c", null ],
    [ "IsEnabled", "classframework_1_1core_1_1_send_data.html#a6f360d50b849504f853969fcddd7459e", null ],
    [ "SetEnabled", "classframework_1_1core_1_1_send_data.html#a6fa0afd5bc9399b979dd45423b3e0186", null ],
    [ "SetSendSize", "classframework_1_1core_1_1_send_data.html#a87d34d5e6a02e1a6c956ece0e9d958e1", null ],
    [ "SetSendPeriod", "classframework_1_1core_1_1_send_data.html#a5e9102e031fad21f51f2db629cdb7829", null ],
    [ "::ui_com", "classframework_1_1core_1_1_send_data.html#a55412ffb5b5723ec3af4e163141276ce", null ]
];