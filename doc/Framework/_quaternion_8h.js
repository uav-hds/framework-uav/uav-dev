var _quaternion_8h =
[
    [ "Quaternion", "classframework_1_1core_1_1_quaternion.html", "classframework_1_1core_1_1_quaternion" ],
    [ "operator+", "_quaternion_8h.html#a7aa669feda0f57bd6630043d78d0315e", null ],
    [ "operator-", "_quaternion_8h.html#a12567b99dac29055d6c7c349dc4928ae", null ],
    [ "operator-", "_quaternion_8h.html#a7314a848649ffe49e0e906375471e572", null ],
    [ "operator*", "_quaternion_8h.html#a81b746c1faf2408d80a041cca4b158bf", null ],
    [ "operator*", "_quaternion_8h.html#a93b3a6d1b1d3e5a6e718f43157b6dacc", null ],
    [ "operator*", "_quaternion_8h.html#ad34ab4a43d28bf874e01550fe590a77e", null ]
];