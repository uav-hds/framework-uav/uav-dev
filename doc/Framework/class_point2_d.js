var class_point2_d =
[
    [ "Point2D", "class_point2_d.html#a5ff02aed785284f425f15dad55d8269b", null ],
    [ "Point2D", "class_point2_d.html#a89c02213326a45abc0272cc521d0b846", null ],
    [ "~Point2D", "class_point2_d.html#a1960ff3b89d1854c7f0242240c5e7fc8", null ],
    [ "CopyFrom", "class_point2_d.html#ac0bef9a5a3eb99f9cc13c6e8a469f276", null ],
    [ "Rotate", "class_point2_d.html#ab694a7624b8a5eb52627a46ba8f4537c", null ],
    [ "RotateDeg", "class_point2_d.html#a9e88247ffec1a7685ed02ed15391de2a", null ],
    [ "Norm", "class_point2_d.html#a246302d2d2400a45e0e7166b91cb7977", null ],
    [ "Normalize", "class_point2_d.html#a7484dfd847e758e6f249faafe9d7882d", null ],
    [ "x", "class_point2_d.html#a2a5ef0ad00bc9e912a9aefcedf004cc4", null ],
    [ "y", "class_point2_d.html#a989485fd2d8026ceec5d57e8c7e629ab", null ]
];