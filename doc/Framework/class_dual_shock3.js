var class_dual_shock3 =
[
    [ "Led", "class_dual_shock3.html#a39f3460b4bc201d00f55fefd983a1fc4", [
      [ "led1", "class_dual_shock3.html#a39f3460b4bc201d00f55fefd983a1fc4a1b302b956cae699df571ac14bacc57cc", null ],
      [ "led2", "class_dual_shock3.html#a39f3460b4bc201d00f55fefd983a1fc4a45cf36ee1d94da5ac6fb60f73b497e07", null ],
      [ "led3", "class_dual_shock3.html#a39f3460b4bc201d00f55fefd983a1fc4a681320f8efd0dad958715bdf5d5c4c41", null ],
      [ "led4", "class_dual_shock3.html#a39f3460b4bc201d00f55fefd983a1fc4a3e6f57697f37271f24a8a263b704fe5e", null ]
    ] ],
    [ "ButtonsMask", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4", [
      [ "start", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a5ad1e18885afc61c351e4afd89473120", null ],
      [ "select", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a7505268b337c969bed2ef5c2d52c1a41", null ],
      [ "square", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a2ded786a1c17ec61bfd139eeae9322bc", null ],
      [ "triangle", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4aae94b964699c5ba1c60d7ac106e7660e", null ],
      [ "circle", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4ac3cdc054545c5dda2115f890bc584bf6", null ],
      [ "cross", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4aa2e6e9ebd587c1da8a0e1b7475d871da", null ],
      [ "L1", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a448ed5263ba43e39ffc72df618c34f28", null ],
      [ "L2", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a5e4f06577013dc46c46faed7ab8acf93", null ],
      [ "L3", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a47ae660bf7dd6d2cb4f4fdd65c77e1c3", null ],
      [ "R1", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a3b823c052badbbab92710c6c8f73f390", null ],
      [ "R2", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a41c000dca770b2282c81e38cf3638817", null ],
      [ "R3", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4ac406a56a025c468ecc7570f8d1dc6059", null ],
      [ "up", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4ab2a8761f55cb8acc9c45aea914b9bb07", null ],
      [ "down", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4a113f2e1cb43669be032258c723b24328", null ],
      [ "left", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4ab0116c02b869be6aaa06d84c02f67941", null ],
      [ "right", "class_dual_shock3.html#aa74b6390ee7c80ae4fedfe37780432f4aa48d3b9b4385b016f06dbf69ae35332f", null ]
    ] ],
    [ "DualShock3", "class_dual_shock3.html#a86351b3d9b6255c458de2a09fe036c40", null ],
    [ "~DualShock3", "class_dual_shock3.html#ab0d10e23248288a4b4db0e2856fe8f60", null ],
    [ "Roll", "class_dual_shock3.html#a5b480c1d6c0e3ea28907ce44c491c2d7", null ],
    [ "Pitch", "class_dual_shock3.html#afd1705642a19b28b8ec0e5cfc207c1e2", null ],
    [ "Yaw", "class_dual_shock3.html#a7e2e28d386a758fb0cb6698430f779a9", null ],
    [ "Thrust", "class_dual_shock3.html#ae2fc29b94770456fbca7486e1ff95fa7", null ],
    [ "Rumble", "class_dual_shock3.html#ad41a78d0ab0ccb6d9c665de59ee006ce", null ],
    [ "SetLedON", "class_dual_shock3.html#ad43434cf76dd455d55dbe18e0c50d3c3", null ],
    [ "SetLedOFF", "class_dual_shock3.html#a55f62985e0e908a30f76378fd8f5e5ce", null ],
    [ "SetLed", "class_dual_shock3.html#a980618b26a1979c637118b8ce016c0ff", null ],
    [ "IsConnected", "class_dual_shock3.html#ac0539815039d864579ea05741f64fe93", null ],
    [ "SetupTab", "class_dual_shock3.html#a4d9671c7736fa5690bbd4d37032510d2", null ],
    [ "ButtonsPressed", "class_dual_shock3.html#a5bdcbcceba0bb3509b08f828faa771b4", null ],
    [ "DualShock3_impl", "class_dual_shock3.html#a9ef06706d54145f6c43953bc72da6355", null ]
];