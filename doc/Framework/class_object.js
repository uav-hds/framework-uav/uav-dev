var class_object =
[
    [ "Object", "class_object.html#a588cf9896a6c1f62733bc7faf013941d", null ],
    [ "~Object", "class_object.html#aa3e791419d84c4c346ef9499513b8e00", null ],
    [ "ObjectName", "class_object.html#afaa39b1021f3a731cda644164b2be3ba", null ],
    [ "ObjectType", "class_object.html#a3e34b6351fad490250b5c0619ea22e17", null ],
    [ "Parent", "class_object.html#a21664d9c70f4a3dc1cd3a9ed0cf1eb5e", null ],
    [ "RootParent", "class_object.html#aceddcf29a2c656eaaa77fbb99f344425", null ],
    [ "RootFrameworkManager", "class_object.html#a4d3f28e95db7f6d7d9818ba1316024c9", null ],
    [ "TypeChilds", "class_object.html#a0784589b4cd7109d5a6ab88bbc9fdd20", null ],
    [ "Childs", "class_object.html#a47326acf6f5b86e667e90f7c36b5222d", null ],
    [ "GetTime", "class_object.html#ad46e831ac641c6aa827a3876c9f47086", null ],
    [ "Printf", "class_object.html#a8554cf8ecd619d2f5b1a3eed36413cb6", null ],
    [ "Warning", "class_object.html#a119852e43f936ff51605c4e994aa3883", null ],
    [ "Error", "class_object.html#a28958c66842aafb754b3f851c6bbc867", null ],
    [ "ErrorOccured", "class_object.html#a6a9e4e23a9d13a07552bc37f8cc004aa", null ],
    [ "Widget_impl", "class_object.html#aea54e136a2f5a2d023439ac4f3ef1593", null ]
];