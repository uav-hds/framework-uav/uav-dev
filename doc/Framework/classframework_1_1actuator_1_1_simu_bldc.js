var classframework_1_1actuator_1_1_simu_bldc =
[
    [ "SimuBldc", "classframework_1_1actuator_1_1_simu_bldc.html#a308b8789ff46cd30a952da65b8abb3e3", null ],
    [ "SimuBldc", "classframework_1_1actuator_1_1_simu_bldc.html#a0616fd81bc36716ef0f2b9f662a2b091", null ],
    [ "~SimuBldc", "classframework_1_1actuator_1_1_simu_bldc.html#aaae9d47b4e1f90e7b6e3e0cfba2b4747", null ],
    [ "GetSpeeds", "classframework_1_1actuator_1_1_simu_bldc.html#a62004437b9432ecd64d5aea8b0cf4d26", null ],
    [ "HasSpeedMeasurement", "classframework_1_1actuator_1_1_simu_bldc.html#ae19b66e549a18c09ea41abbeddcfad0a", null ],
    [ "HasCurrentMeasurement", "classframework_1_1actuator_1_1_simu_bldc.html#aa1aea0b36e35049abdcb9416dd018242", null ]
];