var classframework_1_1gui_1_1_widget =
[
    [ "Widget", "classframework_1_1gui_1_1_widget.html#aa22cbbef51bba4a41a387f2157de74be", null ],
    [ "~Widget", "classframework_1_1gui_1_1_widget.html#a5828f9d29972aa861dc4a4fe21997499", null ],
    [ "setEnabled", "classframework_1_1gui_1_1_widget.html#a7fe633c500864d494e67ba9e5ab4e3cd", null ],
    [ "isEnabled", "classframework_1_1gui_1_1_widget.html#a472d499749ee9acadc1d446387cbbbe7", null ],
    [ "SetPersistentXmlProp", "classframework_1_1gui_1_1_widget.html#af78e50b7be98bdd572028b0e47c4509b", null ],
    [ "GetPersistentXmlProp", "classframework_1_1gui_1_1_widget.html#a7f3edf9b759566ed766e96e3b802f3c7", null ],
    [ "SetVolatileXmlProp", "classframework_1_1gui_1_1_widget.html#ad132417af77a0b84089cc87f7b0a54c9", null ],
    [ "SendXml", "classframework_1_1gui_1_1_widget.html#a3ac7f6fe666efa23167636cc081f7e43", null ],
    [ "XmlEvent", "classframework_1_1gui_1_1_widget.html#ab314c94187e93f5507f6dbfdea58a778", null ],
    [ "core::FrameworkManager", "classframework_1_1gui_1_1_widget.html#ae9d4248776523b22e3242651bcee4557", null ],
    [ "::Widget_impl", "classframework_1_1gui_1_1_widget.html#a2a6f0753cfcbf314ad7fa952bfe9bc67", null ],
    [ "::FrameworkManager_impl", "classframework_1_1gui_1_1_widget.html#a85f53b6422522c0e6d8c02a3fda425e8", null ]
];