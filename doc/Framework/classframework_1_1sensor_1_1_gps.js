var classframework_1_1sensor_1_1_gps =
[
    [ "FixQuality_t", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070", [
      [ "Invalid", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a4bbb8f967da6d1a610596d7257179c2b", null ],
      [ "Gps", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a06960407a35b206422d40932f13c8d91", null ],
      [ "DGps", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070aee015802cac7ac6094dc62db8b8a2f51", null ],
      [ "Pps", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070abd9768d943ecb6ddb75aeffea3bfd829", null ],
      [ "Rtk", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a8ad9533f39ec3fe43d6ee0104464a116", null ],
      [ "RtkFloat", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a1eacb557d24c49af2ec6832c5fc32413", null ],
      [ "Estimated", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a3c311fbd0f9e51ce27b984f55164cf83", null ],
      [ "Manual", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070ae1ba155a9f2e8c3be94020eef32a0301", null ],
      [ "Simulation", "classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a4f502b57d2835715eaa382c7d4c32e94", null ]
    ] ],
    [ "NMEAFlags_t", "classframework_1_1sensor_1_1_gps.html#a6835e9e2cdd6189bbdfb6d4483478234", [
      [ "GGA", "classframework_1_1sensor_1_1_gps.html#a6835e9e2cdd6189bbdfb6d4483478234addb2829db61b82906101f81621b72a15", null ],
      [ "VTG", "classframework_1_1sensor_1_1_gps.html#a6835e9e2cdd6189bbdfb6d4483478234a668a0c7e54c498682f662b13938a2bfb", null ],
      [ "GST", "classframework_1_1sensor_1_1_gps.html#a6835e9e2cdd6189bbdfb6d4483478234a1a1ffbc77c26d00e01df0a98f7c711df", null ]
    ] ],
    [ "Gps", "classframework_1_1sensor_1_1_gps.html#ab30e2e915b24fdda381ba16a435eb050", null ],
    [ "~Gps", "classframework_1_1sensor_1_1_gps.html#a99868101e26a88375c576254d12bbdda", null ],
    [ "UseDefaultPlot", "classframework_1_1sensor_1_1_gps.html#a5ad4c1deab4b628fb1f6d9553499d15e", null ],
    [ "EPlot", "classframework_1_1sensor_1_1_gps.html#a16f2841eb386fe1bd864130441605d12", null ],
    [ "NPlot", "classframework_1_1sensor_1_1_gps.html#a72aae77c6f5f67b089843cf684112459", null ],
    [ "UPlot", "classframework_1_1sensor_1_1_gps.html#a7588126b45d2a322145dc94bb0e0168c", null ],
    [ "VEPlot", "classframework_1_1sensor_1_1_gps.html#ae9b44dcc5e3d9804dfb0a44c695c3ebd", null ],
    [ "VNPlot", "classframework_1_1sensor_1_1_gps.html#a22f882b31b9cb59186e13dac3aeae56c", null ],
    [ "GetTab", "classframework_1_1sensor_1_1_gps.html#a4bbc2c14b7123f06b5ae9c80b2e18f7b", null ],
    [ "GetLayout", "classframework_1_1sensor_1_1_gps.html#aa3d9c3cace27dafe48673bde9a7b26ad", null ],
    [ "GetPlotTab", "classframework_1_1sensor_1_1_gps.html#a4a27b55c12e2089fed49cdf9e9714ee1", null ],
    [ "NbSat", "classframework_1_1sensor_1_1_gps.html#aca54673898eddbc1d1078fdc7f1d88dd", null ],
    [ "FixQuality", "classframework_1_1sensor_1_1_gps.html#a8c480b2dcb3be744b0f0aeffbc4a3cbe", null ],
    [ "SetRef", "classframework_1_1sensor_1_1_gps.html#a2ed3abd4f1cf24d5be873dc995d11cb9", null ],
    [ "GetENUPosition", "classframework_1_1sensor_1_1_gps.html#a2aa80e9268af7c1b7f13c71a057ec4b5", null ],
    [ "parseFrame", "classframework_1_1sensor_1_1_gps.html#a25fe40b70f56d99bc216498a553ca757", null ],
    [ "NMEAFlags", "classframework_1_1sensor_1_1_gps.html#af3dfc19cfd0257e2067b9a284fdc9074", null ],
    [ "position", "classframework_1_1sensor_1_1_gps.html#a19efa6085bcca447f0d94b7a29a4a8e3", null ]
];