var _object_8h =
[
    [ "Message", "classframework_1_1core_1_1_message.html", "classframework_1_1core_1_1_message" ],
    [ "Object", "classframework_1_1core_1_1_object.html", "classframework_1_1core_1_1_object" ],
    [ "Warn", "_object_8h.html#a0e8095ce1ed00bd4150a20c72d4e4330", null ],
    [ "Err", "_object_8h.html#a855a0b471afb53d73f572c402f1737e0", null ],
    [ "Info", "_object_8h.html#ad436e1e1d6bf1c744f5eca58bfa1fcc8", null ],
    [ "TIME_INFINITE", "_object_8h.html#ae1568cf3090c973c3bc47f86ae2f5afb", null ],
    [ "TIME_NONBLOCK", "_object_8h.html#a9b03c04ef7c1cc240afc43e9ef8ba8b7", null ],
    [ "Time", "_object_8h.html#a556a09521d9b94e1c9f0bfe6744d611d", null ],
    [ "GetTime", "_object_8h.html#a54a48f7a2a292bf1f6da7c1e65d73868", null ],
    [ "Printf", "_object_8h.html#abdc90e4bd0996ea501a4c48e2336203c", null ]
];