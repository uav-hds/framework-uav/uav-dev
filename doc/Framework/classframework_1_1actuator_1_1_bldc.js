var classframework_1_1actuator_1_1_bldc =
[
    [ "Bldc", "classframework_1_1actuator_1_1_bldc.html#a51bac3502175d806c0720a9c087068bf", null ],
    [ "Bldc", "classframework_1_1actuator_1_1_bldc.html#a5ed0806b58da9237351ba8a19d070a1a", null ],
    [ "~Bldc", "classframework_1_1actuator_1_1_bldc.html#ae69b362d1840052e17830a67ec44afa1", null ],
    [ "LockUserInterface", "classframework_1_1actuator_1_1_bldc.html#af9ec4a81b41c679aeadbb0973a693c44", null ],
    [ "UnlockUserInterface", "classframework_1_1actuator_1_1_bldc.html#a087b039432817a763474e897b3f5cdf4", null ],
    [ "UseDefaultPlot", "classframework_1_1actuator_1_1_bldc.html#a4e9a43815c14e0e584f76c516a58a70c", null ],
    [ "Output", "classframework_1_1actuator_1_1_bldc.html#a25d34de702c4a799e80e839fe4c2929f", null ],
    [ "MotorsCount", "classframework_1_1actuator_1_1_bldc.html#a5a237bd484c06e990eefa237e74fa6d0", null ],
    [ "SetEnabled", "classframework_1_1actuator_1_1_bldc.html#a964de7e5226e6ee2317ec3d4d13308a0", null ],
    [ "AreEnabled", "classframework_1_1actuator_1_1_bldc.html#a260a95f6c4f20fe661b4793a92b45528", null ],
    [ "SetPower", "classframework_1_1actuator_1_1_bldc.html#aa074ef310ada48a7205295c393bc97d1", null ],
    [ "GetLayout", "classframework_1_1actuator_1_1_bldc.html#aeda456e3039a119b2e242f86f33176de", null ],
    [ "HasSpeedMeasurement", "classframework_1_1actuator_1_1_bldc.html#ada01355096b9e0baf3f7da43e5c2e7f4", null ],
    [ "HasCurrentMeasurement", "classframework_1_1actuator_1_1_bldc.html#aa55e5e301932e6f52f0fa8c51f73a254", null ],
    [ "::Bldc_impl", "classframework_1_1actuator_1_1_bldc.html#ae6cf9929b8571df321e07aa561b64a9a", null ],
    [ "output", "classframework_1_1actuator_1_1_bldc.html#ace6b63ba0d31c61cb25235e28fbc16b3", null ]
];