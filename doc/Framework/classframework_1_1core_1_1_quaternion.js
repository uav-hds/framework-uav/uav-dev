var classframework_1_1core_1_1_quaternion =
[
    [ "Quaternion", "classframework_1_1core_1_1_quaternion.html#a3a1c3830ca00a49c41d2246e26ac39c6", null ],
    [ "~Quaternion", "classframework_1_1core_1_1_quaternion.html#ad8d1965a6222c840daae5536f2503a67", null ],
    [ "GetNorm", "classframework_1_1core_1_1_quaternion.html#acbb77de24a84b6a031f33fbb25b729a8", null ],
    [ "Normalize", "classframework_1_1core_1_1_quaternion.html#af71f96f0edf8491d46e080b3a3cd4f29", null ],
    [ "GetLogarithm", "classframework_1_1core_1_1_quaternion.html#a24f424cf9147df43632a5c2ed4ee3db6", null ],
    [ "GetLogarithm", "classframework_1_1core_1_1_quaternion.html#a051f1d1f9f921991a0ab1f414878cfd0", null ],
    [ "Conjugate", "classframework_1_1core_1_1_quaternion.html#a9e201925afd5d8777040e162beb28454", null ],
    [ "GetConjugate", "classframework_1_1core_1_1_quaternion.html#a43e13e22b373ffa61c754cbf97e759e7", null ],
    [ "GetDerivative", "classframework_1_1core_1_1_quaternion.html#a7ed5fb6d063f6e72b845d23898b18b6d", null ],
    [ "Derivate", "classframework_1_1core_1_1_quaternion.html#a0a01fc4d483e3e3418e1e3d1f9b3a718", null ],
    [ "ToEuler", "classframework_1_1core_1_1_quaternion.html#afebfad4202fe91b844d605b25874e9aa", null ],
    [ "ToEuler", "classframework_1_1core_1_1_quaternion.html#a6e092fe84484f3f881613a63730e344a", null ],
    [ "ToRotationMatrix", "classframework_1_1core_1_1_quaternion.html#aed265e0c2884ea6031aba6225b0ee232", null ],
    [ "operator+=", "classframework_1_1core_1_1_quaternion.html#abfa1ca989b7109427c84cac38293d8b3", null ],
    [ "operator-=", "classframework_1_1core_1_1_quaternion.html#a00ff3b8544cc556a447f53c0f998ab7f", null ],
    [ "operator=", "classframework_1_1core_1_1_quaternion.html#a15cd5ed68a28e48459c0d315244f5e93", null ],
    [ "q0", "classframework_1_1core_1_1_quaternion.html#a47aef1d550786760389d409078bf14cb", null ],
    [ "q1", "classframework_1_1core_1_1_quaternion.html#adace442ce2538585fe5dbdd9da79275e", null ],
    [ "q2", "classframework_1_1core_1_1_quaternion.html#ad5db0b992ffe05a7ee9514e162e7cd8c", null ],
    [ "q3", "classframework_1_1core_1_1_quaternion.html#a80176bc6571ad07f9a61daa49d303410", null ]
];