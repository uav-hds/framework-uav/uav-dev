var group___framework =
[
    [ "Box", "classframework_1_1core_1_1_box.html", [
      [ "Box", "classframework_1_1core_1_1_box.html#ae6dc6dec3960567cc69a3841c3716044", null ],
      [ "~Box", "classframework_1_1core_1_1_box.html#a957f7623616a9fe61717310fd57baf60", null ],
      [ "ValueChanged", "classframework_1_1core_1_1_box.html#a84a23ce9afcdbdc0008fb810aa448bcc", null ],
      [ "SetValueChanged", "classframework_1_1core_1_1_box.html#aab9f7e5af1f5baa2dfc18a8418ae921b", null ],
      [ "GetMutex", "classframework_1_1core_1_1_box.html#a9dbfeb5bf3796bbb4032cf00f116e9cf", null ],
      [ "ReleaseMutex", "classframework_1_1core_1_1_box.html#aaa28d8e9e3df9db0f03ee38589a63fa7", null ]
    ] ],
    [ "CheckBox", "classframework_1_1core_1_1_check_box.html", [
      [ "CheckBox", "classframework_1_1core_1_1_check_box.html#aed793f38879045485809aaa43acf112b", null ],
      [ "~CheckBox", "classframework_1_1core_1_1_check_box.html#a43663f6c82790ca262170d0800e0ff63", null ],
      [ "IsChecked", "classframework_1_1core_1_1_check_box.html#afa4e55c85321f78518fea361d8b40c34", null ],
      [ "Value", "classframework_1_1core_1_1_check_box.html#a7ee6e744a4edc74c52a526614ac45d76", null ]
    ] ],
    [ "ComboBox", "classframework_1_1core_1_1_combo_box.html", [
      [ "ComboBox", "classframework_1_1core_1_1_combo_box.html#abbb54bbb9af6ea85706fe5b0bd4711c7", null ],
      [ "~ComboBox", "classframework_1_1core_1_1_combo_box.html#af5ae954bfef82c15e48f41100c7969c2", null ],
      [ "AddItem", "classframework_1_1core_1_1_combo_box.html#af5000b376a0e148e5bcf5d537e333319", null ],
      [ "CurrentIndex", "classframework_1_1core_1_1_combo_box.html#aaecdc0e0841f594b51b4ff720820e2b5", null ]
    ] ],
    [ "ConditionVariable", "classframework_1_1core_1_1_condition_variable.html", [
      [ "ConditionVariable", "classframework_1_1core_1_1_condition_variable.html#ab9ae6871a1f289bcd2f890a75c0514d5", null ],
      [ "~ConditionVariable", "classframework_1_1core_1_1_condition_variable.html#afd2fe1bb838a4b7c3256bfcab5e0edcd", null ],
      [ "CondWait", "classframework_1_1core_1_1_condition_variable.html#ac4ece7fa807a2e959011acb5037fd0b8", null ],
      [ "CondSignal", "classframework_1_1core_1_1_condition_variable.html#a5a949eb6aae721403fc6a26ece04b6bd", null ]
    ] ],
    [ "cvimage", "classframework_1_1core_1_1cvimage.html", [
      [ "cvimage", "classframework_1_1core_1_1cvimage.html#a7f12d6cf9e0853f3f857f75bc8d885e1", null ],
      [ "~cvimage", "classframework_1_1core_1_1cvimage.html#ad702529a508b1f67bbff58e8e99e2f2c", null ],
      [ "Width", "classframework_1_1core_1_1cvimage.html#a2440a06ef7213dc07d228f75c230488a", null ],
      [ "Height", "classframework_1_1core_1_1cvimage.html#ad9f4d7428d64e160d3e2db99c85ec67f", null ],
      [ "img", "classframework_1_1core_1_1cvimage.html#a5cb3c8860d70215f05d111b4815956df", null ]
    ] ],
    [ "cvmatrix", "classframework_1_1core_1_1cvmatrix.html", [
      [ "cvmatrix", "classframework_1_1core_1_1cvmatrix.html#a6ae4b7d5024f2d20a7fae8a13f293667", null ],
      [ "cvmatrix", "classframework_1_1core_1_1cvmatrix.html#a173005704f87ffeab3423ca552edabd1", null ],
      [ "~cvmatrix", "classframework_1_1core_1_1cvmatrix.html#a15496182b07f60d8dee6ebbf273454fa", null ],
      [ "Value", "classframework_1_1core_1_1cvmatrix.html#a257c2994fa71ffd2caba4f40e37d32c5", null ],
      [ "ValueNoMutex", "classframework_1_1core_1_1cvmatrix.html#aac8537135e1b02389be552a9ce221d7a", null ],
      [ "SetValue", "classframework_1_1core_1_1cvmatrix.html#ac5af3ad1d3a1695ec8fe2152b38cd8b5", null ],
      [ "SetValueNoMutex", "classframework_1_1core_1_1cvmatrix.html#a5a381a31496c977a175f68dce33f9446", null ],
      [ "Name", "classframework_1_1core_1_1cvmatrix.html#a16a953e092515758d6b9b355ca8f6f71", null ],
      [ "Rows", "classframework_1_1core_1_1cvmatrix.html#a367a9f5a114a6efbaef1aea25963408f", null ],
      [ "Cols", "classframework_1_1core_1_1cvmatrix.html#af8937539f5f9db4ed6f7adfc373eed12", null ],
      [ "DataType", "classframework_1_1core_1_1cvmatrix.html#ab64d0b07fdcac4ea9f7f1b13be133419", null ]
    ] ],
    [ "cvmatrix_descriptor", "classframework_1_1core_1_1cvmatrix__descriptor.html", [
      [ "cvmatrix_descriptor", "classframework_1_1core_1_1cvmatrix__descriptor.html#a735a4889a2f9a9793d1753aec2d58ba2", null ],
      [ "~cvmatrix_descriptor", "classframework_1_1core_1_1cvmatrix__descriptor.html#ac27929158efc59cbf1d674ae87ab0167", null ],
      [ "SetElementName", "classframework_1_1core_1_1cvmatrix__descriptor.html#a9cba19c8dbaa571df112eeda0bef6059", null ],
      [ "ElementName", "classframework_1_1core_1_1cvmatrix__descriptor.html#aa719d40bc0051d8226943145967aad4f", null ],
      [ "Rows", "classframework_1_1core_1_1cvmatrix__descriptor.html#adccd3f9e02774dd2fc3cd3e6b18e3dcc", null ],
      [ "Cols", "classframework_1_1core_1_1cvmatrix__descriptor.html#a9d749f3dccb00b1e86fd78ad17d9d186", null ]
    ] ],
    [ "DataPlot", "classframework_1_1core_1_1_data_plot.html", [
      [ "DataPlot", "classframework_1_1core_1_1_data_plot.html#a162d6799d9cb8b04fa78dc191270c1b7", null ],
      [ "~DataPlot", "classframework_1_1core_1_1_data_plot.html#a26d8945fee6927c4d1fbc62ad1a029b9", null ],
      [ "XmlEvent", "classframework_1_1core_1_1_data_plot.html#a4230c27f62f4c945e5d00202ce928593", null ],
      [ "AddDataToSend", "classframework_1_1core_1_1_data_plot.html#a9a32aa3fbbf77baadad7f05351e03d22", null ],
      [ "AddDataToSend", "classframework_1_1core_1_1_data_plot.html#a628f90119c512c4d29c7903aed4d8f36", null ]
    ] ],
    [ "DataPlot1D", "classframework_1_1core_1_1_data_plot1_d.html", [
      [ "DataPlot1D", "classframework_1_1core_1_1_data_plot1_d.html#a3edd1bce74f250954c8c17473d33bcfb", null ],
      [ "~DataPlot1D", "classframework_1_1core_1_1_data_plot1_d.html#a36a4952eb6dcb4d982301716d6d7afba", null ],
      [ "AddCurve", "classframework_1_1core_1_1_data_plot1_d.html#a4fa8bfd6f16d5ae2e7dd2570286b5faf", null ],
      [ "AddCurve", "classframework_1_1core_1_1_data_plot1_d.html#acce2ca5a68ab078930b728ea07fe96c7", null ]
    ] ],
    [ "DataPlot2D", "classframework_1_1core_1_1_data_plot2_d.html", [
      [ "DataPlot2D", "classframework_1_1core_1_1_data_plot2_d.html#a9a23bf6b1ec886f7d591f7f5952e9195", null ],
      [ "~DataPlot2D", "classframework_1_1core_1_1_data_plot2_d.html#a8be1a8c23f817ee43bb5d403fa1a2241", null ],
      [ "AddCurve", "classframework_1_1core_1_1_data_plot2_d.html#a51151d6bf224d962690d8f829e8144a0", null ]
    ] ],
    [ "DoubleSpinBox", "classframework_1_1core_1_1_double_spin_box.html", [
      [ "DoubleSpinBox", "classframework_1_1core_1_1_double_spin_box.html#ab53c8c08f1dd506283ccbcfc0bbcf76e", null ],
      [ "~DoubleSpinBox", "classframework_1_1core_1_1_double_spin_box.html#add77b083acc15e8468cee0a0740cd8a0", null ],
      [ "Value", "classframework_1_1core_1_1_double_spin_box.html#a84c74c164bee9647684abd7d6491a0f9", null ]
    ] ],
    [ "Euler", "classframework_1_1core_1_1_euler.html", [
      [ "Euler", "classframework_1_1core_1_1_euler.html#a496cc17cc6fb0a951cf28b1fa4996424", null ],
      [ "Euler", "classframework_1_1core_1_1_euler.html#a7f34e3a3694894abc3e64adce5f9ab61", null ],
      [ "~Euler", "classframework_1_1core_1_1_euler.html#af989d2e25ef2561ecdee62a6f3094815", null ],
      [ "CopyFrom", "classframework_1_1core_1_1_euler.html#a693e1d306cbb4e3d28c2dee44a208477", null ],
      [ "RotateX", "classframework_1_1core_1_1_euler.html#a4218e2453532035c1da081ce05cd7625", null ],
      [ "RotateXDeg", "classframework_1_1core_1_1_euler.html#a39ca5f6a50cd25782b674511ead7bedb", null ],
      [ "RotateY", "classframework_1_1core_1_1_euler.html#a528d6fe7bebabfd8f783ec6dbf510e86", null ],
      [ "RotateYDeg", "classframework_1_1core_1_1_euler.html#a497ff976a3b4b0b5e77f5ac17bc0648a", null ],
      [ "RotateZ", "classframework_1_1core_1_1_euler.html#a955d5c391dddc6959a21cadc776f4aae", null ],
      [ "RotateZDeg", "classframework_1_1core_1_1_euler.html#a73510ac5853edf97b9d54c0304383ea5", null ],
      [ "roll", "classframework_1_1core_1_1_euler.html#a2a74eff3712df6dcca757c3d105c2076", null ],
      [ "pitch", "classframework_1_1core_1_1_euler.html#af7d5e26735084cdc6160d573b4f23ac9", null ],
      [ "yaw", "classframework_1_1core_1_1_euler.html#ad55508b2ebca2cf4f868b7480dc51dce", null ]
    ] ],
    [ "FrameworkManager", "classframework_1_1core_1_1_framework_manager.html", [
      [ "FrameworkManager", "classframework_1_1core_1_1_framework_manager.html#a057295924f84b2c9a1fa2e21a3cd9c48", null ],
      [ "~FrameworkManager", "classframework_1_1core_1_1_framework_manager.html#aa450d1de1dfd800a375793f516778dc0", null ],
      [ "SetupLogger", "classframework_1_1core_1_1_framework_manager.html#a910cf41d8cfcad3f01a2d4820b4ecdc2", null ],
      [ "AddDeviceToLog", "classframework_1_1core_1_1_framework_manager.html#a6056cf9ac936155c49613c672ade55ed", null ],
      [ "StartLog", "classframework_1_1core_1_1_framework_manager.html#ac9ace8e2d4239e880d40a0dbf0730cc1", null ],
      [ "StopLog", "classframework_1_1core_1_1_framework_manager.html#a20b5c34e89f2254822c630f231ac8547", null ],
      [ "IsLogging", "classframework_1_1core_1_1_framework_manager.html#a576a5122d5093e86915abe680b1d0537", null ],
      [ "SetupDSP", "classframework_1_1core_1_1_framework_manager.html#a4e8036c67cfa1c87252df763856a574e", null ],
      [ "AddSendData", "classframework_1_1core_1_1_framework_manager.html#a56bd1b3e075c2455c0cd3e7472209907", null ],
      [ "RemoveSendData", "classframework_1_1core_1_1_framework_manager.html#a3c68f92e8df522e4848bc9e431e33b1f", null ],
      [ "UpdateSendData", "classframework_1_1core_1_1_framework_manager.html#aeef6db11d38a760b77e94f651eb509cb", null ],
      [ "UpdateDataToSendSize", "classframework_1_1core_1_1_framework_manager.html#ac8a2126862207a8234b7df7e437d8f24", null ],
      [ "BlockCom", "classframework_1_1core_1_1_framework_manager.html#a2848deb4f447f069240886a053335de8", null ],
      [ "UnBlockCom", "classframework_1_1core_1_1_framework_manager.html#aca78bacec4bcddb9201df47abe1e14e9", null ],
      [ "ConnectionLost", "classframework_1_1core_1_1_framework_manager.html#a1e60c097631b78dec279308c7439344a", null ],
      [ "::Widget_impl", "classframework_1_1core_1_1_framework_manager.html#a2a6f0753cfcbf314ad7fa952bfe9bc67", null ],
      [ "::IODevice_impl", "classframework_1_1core_1_1_framework_manager.html#a98e657a0de938e3819054ef6b02299c8", null ]
    ] ],
    [ "GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html", [
      [ "GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html#a7bf8719f5500933c09366b1d4f8d6f5a", null ],
      [ "GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html#a76c1a1cbee5345ce33a9806455c5b46c", null ],
      [ "~GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html#ad5c8bedb43f17d2b05be9bd4d8b3367a", null ],
      [ "CopyFrom", "classframework_1_1core_1_1_geo_coordinate.html#a7598c5592173156f64bdc5fc6356e42d", null ],
      [ "SetCoordinates", "classframework_1_1core_1_1_geo_coordinate.html#a7b72cda95437b7e8e23fc26d710b13ec", null ],
      [ "GetCoordinates", "classframework_1_1core_1_1_geo_coordinate.html#accb94165575f6921af8cb43241e3446a", null ]
    ] ],
    [ "GridLayout", "classframework_1_1core_1_1_grid_layout.html", [
      [ "GridLayout", "classframework_1_1core_1_1_grid_layout.html#a73171a7d79e3146a6629d4ffdbc3ea5d", null ],
      [ "~GridLayout", "classframework_1_1core_1_1_grid_layout.html#afccf38ec826f83f943d7de28701c2f03", null ]
    ] ],
    [ "GroupBox", "classframework_1_1core_1_1_group_box.html", [
      [ "GroupBox", "classframework_1_1core_1_1_group_box.html#ab9b496c77ad0bd3aaba7afd410217323", null ],
      [ "~GroupBox", "classframework_1_1core_1_1_group_box.html#a5ee1542b57e0f9b88c53a0e9277bb717", null ]
    ] ],
    [ "ImuData", "classframework_1_1core_1_1_imu_data.html", [
      [ "AvailableData_t", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5", [
        [ "HasEulerAngles", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5adee0c22f26ba8eb9aafd9a10cca223ec", null ],
        [ "HasOrientationMatrix", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5ac47ab253179568ba53a5e6c7f917727d", null ],
        [ "HasQuaternions", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5aaa7c0eb66806bbf963f4cfed12cffc77", null ],
        [ "HasFilteredAngRates", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5af261bdff1456145e958b2b751474098f", null ],
        [ "HasRawAcc", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5a0a4c1998f1ccb69ceca94a353a60f23d", null ],
        [ "HasRawGyr", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5a1e8136ec65fe458786f594e121922854", null ],
        [ "HasRawMag", "classframework_1_1core_1_1_imu_data.html#a1401700f12eb4eeeaeff79183aa2fab5addee7cecfcab7c062d0b903e125b94cf", null ]
      ] ],
      [ "PlotableData_t", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8", [
        [ "Roll", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a08723a04eee0790a4da90f797939cd75", null ],
        [ "Pitch", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a4613f62f7ca657d182ba35d5a81b36bd", null ],
        [ "Yaw", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a300d25046416c3ccffea878d0b533efe", null ],
        [ "Wx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a5ccf8febd337ab51f707d9315250148e", null ],
        [ "Wy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8aabff74ec2fe4086c9be3514ef08c9261", null ],
        [ "Wz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a807728c6d8cbea3282f14eaa6643b19b", null ],
        [ "RawAx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a4d5057af07548b6b34e160ef48004cf0", null ],
        [ "RawAy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8adf083b1df8e937957c759ad93ad3028e", null ],
        [ "RawAz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a4f359d1e7d7b978325f85166e529286c", null ],
        [ "RawGx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a16c71ada258e96a3f0880fc2e431b9c5", null ],
        [ "RawGy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a596ddf0a2379801d40a5251e341c92d3", null ],
        [ "RawGz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8ab0dfaab3b5d46ed0b4ead0735a1fa602", null ],
        [ "RawMx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8af223557b356e0d40a37592f143f80a37", null ],
        [ "RawMy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a61646a6c036574ae54037126ee5b0805", null ],
        [ "RawMz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a7576067451da5dfc71410871b9feca6c", null ]
      ] ],
      [ "Conversion_t", "classframework_1_1core_1_1_imu_data.html#affb815ea722ccb1a9b2134691218c697", [
        [ "None", "classframework_1_1core_1_1_imu_data.html#affb815ea722ccb1a9b2134691218c697a334826b8820d26a2efa66d811b16b88e", null ],
        [ "Rad2Deg", "classframework_1_1core_1_1_imu_data.html#affb815ea722ccb1a9b2134691218c697afb75a94c925ac972e60fd4d0a172fd1e", null ]
      ] ],
      [ "ImuData", "classframework_1_1core_1_1_imu_data.html#a705a5045e050915c466865344fddc212", null ],
      [ "~ImuData", "classframework_1_1core_1_1_imu_data.html#ae8ec0cd1606d05b577c36f2203bf9421", null ],
      [ "AvailableData", "classframework_1_1core_1_1_imu_data.html#af6b45956821c9c275e7ffdda35924e77", null ],
      [ "Convert", "classframework_1_1core_1_1_imu_data.html#a3677d509d05d382471bb46ffa7725e29", null ],
      [ "EulerAngles", "classframework_1_1core_1_1_imu_data.html#ad16a3fe18fabcd11a5604f43191ca7e9", null ],
      [ "FilteredAngRates", "classframework_1_1core_1_1_imu_data.html#a1567213ae15831c851bd9a48fb046692", null ],
      [ "RawAcc", "classframework_1_1core_1_1_imu_data.html#a133e64645e78f3fb40900233f3a82139", null ],
      [ "RawGyr", "classframework_1_1core_1_1_imu_data.html#a371662d8bdd9ab0bc4a4f270307335b2", null ],
      [ "RawMag", "classframework_1_1core_1_1_imu_data.html#a5c3a8cdb1d6bb7a35874307df37e4605", null ]
    ] ],
    [ "io_data", "classframework_1_1core_1_1io__data.html", [
      [ "DataType_t", "classframework_1_1core_1_1io__data.html#a9733182e38c4616f82c08145621d8739", [
        [ "Float", "classframework_1_1core_1_1io__data.html#a9733182e38c4616f82c08145621d8739aeed6f8250f23a1c5d4827d95ddd962e5", null ],
        [ "Int8_t", "classframework_1_1core_1_1io__data.html#a9733182e38c4616f82c08145621d8739ad919b22c075e341b076d9ba2851ec1dc", null ]
      ] ],
      [ "io_data", "classframework_1_1core_1_1io__data.html#a050bb28ad52b84486375cae079b8bfec", null ],
      [ "~io_data", "classframework_1_1core_1_1io__data.html#aa08b00b8f76a7bb14d7498e8e34649a2", null ],
      [ "Size", "classframework_1_1core_1_1io__data.html#a264cf96e6a6da56b26828384b00c178f", null ],
      [ "SetDataTime", "classframework_1_1core_1_1io__data.html#af4eca364240e1b754fa989bda9717d5a", null ],
      [ "DataTime", "classframework_1_1core_1_1io__data.html#a20dcbf89d575bb506748eaa2abab9d7b", null ],
      [ "Prev", "classframework_1_1core_1_1io__data.html#a0ffbd15f1082d98948d9060e8200c59e", null ],
      [ "SetSize", "classframework_1_1core_1_1io__data.html#a7e0b15e6591c47bfc0e49565ddf460c2", null ],
      [ "AppendLogDescription", "classframework_1_1core_1_1io__data.html#a64efcd879c5ea3a1e570cd7f4ed8e4ce", null ],
      [ "SetPtrToCircle", "classframework_1_1core_1_1io__data.html#a65e6d218e3699bfbb7c8c958d85156bf", null ],
      [ "IODevice", "classframework_1_1core_1_1io__data.html#a281fe62c2ab73385c87f74e53683dd5c", null ],
      [ "::IODevice_impl", "classframework_1_1core_1_1io__data.html#a98e657a0de938e3819054ef6b02299c8", null ],
      [ "::io_data_impl", "classframework_1_1core_1_1io__data.html#a6bb0f9fd0e860684a091117e8949d7f7", null ],
      [ "prev", "classframework_1_1core_1_1io__data.html#a12e0da00d9b0af3e7009f7c840d91272", null ]
    ] ],
    [ "IODevice", "classframework_1_1core_1_1_i_o_device.html", [
      [ "IODevice", "classframework_1_1core_1_1_i_o_device.html#a95b56a32dc9a0af90da89ed351385b57", null ],
      [ "~IODevice", "classframework_1_1core_1_1_i_o_device.html#aca7bca58d80d05ade95107d40cc54edd", null ],
      [ "AddDeviceToLog", "classframework_1_1core_1_1_i_o_device.html#ab908771540c2c4ab3f061bde6729ec28", null ],
      [ "ProcessUpdate", "classframework_1_1core_1_1_i_o_device.html#a50eb13bc82e98bc0e170d77bba026de4", null ],
      [ "SetDataToLog", "classframework_1_1core_1_1_i_o_device.html#ad622dcf9bb33bf0a4230ce76c2152904", null ],
      [ "::IODevice_impl", "classframework_1_1core_1_1_i_o_device.html#a98e657a0de938e3819054ef6b02299c8", null ],
      [ "::Thread_impl", "classframework_1_1core_1_1_i_o_device.html#a8d4dacaaa8b2d282a4c38c48281387c7", null ],
      [ "::FrameworkManager_impl", "classframework_1_1core_1_1_i_o_device.html#a85f53b6422522c0e6d8c02a3fda425e8", null ]
    ] ],
    [ "Label", "classframework_1_1core_1_1_label.html", [
      [ "Label", "classframework_1_1core_1_1_label.html#aee0b92ed67b97226f5e9777f5ae9ce21", null ],
      [ "~Label", "classframework_1_1core_1_1_label.html#a9d7344339861e4ee2a3ba62100b5a6d1", null ],
      [ "SetText", "classframework_1_1core_1_1_label.html#a63270f81f50fbcdcbbe922638045bcfe", null ]
    ] ],
    [ "LayoutItem", "classframework_1_1core_1_1_layout_item.html", [
      [ "LayoutItem", "classframework_1_1core_1_1_layout_item.html#ada8b56a70a8230de20e11af00b24aeee", null ],
      [ "~LayoutItem", "classframework_1_1core_1_1_layout_item.html#a1a6f3586fed91cb488fa57eb20ede981", null ],
      [ "Box", "classframework_1_1core_1_1_layout_item.html#a3f229d41a6747c1fd12cbe102d06b13c", null ]
    ] ],
    [ "Map", "classframework_1_1core_1_1_map.html", [
      [ "Map", "classframework_1_1core_1_1_map.html#a94ab2499f498b923b4017b489170f9c8", null ],
      [ "~Map", "classframework_1_1core_1_1_map.html#ac906cf166444bfac769e96fade91631d", null ],
      [ "AddPoint", "classframework_1_1core_1_1_map.html#ac15820b3d8d0bd3379d2e61d397b6043", null ],
      [ "CopyDatas", "classframework_1_1core_1_1_map.html#a499fd5d1c4b4d8d3c3461aef5b5fc252", null ]
    ] ],
    [ "Mutex", "classframework_1_1core_1_1_mutex.html", [
      [ "Mutex", "classframework_1_1core_1_1_mutex.html#a5cff68e32692551ada6ccd1b912a72d4", null ],
      [ "~Mutex", "classframework_1_1core_1_1_mutex.html#a3a67ec45af9ba25a8a885d8473c9ae65", null ],
      [ "GetMutex", "classframework_1_1core_1_1_mutex.html#ac348d4d204f0e438d7d7475524306573", null ],
      [ "ReleaseMutex", "classframework_1_1core_1_1_mutex.html#a7405b92a0fd95130967e5d510222a7c6", null ],
      [ "::ConditionVariable_impl", "classframework_1_1core_1_1_mutex.html#aad79ca01711327d0cf4a99bbadeeb11d", null ]
    ] ],
    [ "Object", "classframework_1_1core_1_1_object.html", [
      [ "Object", "classframework_1_1core_1_1_object.html#a2c0d3a6932a32e14fe0da2cb5d396c8f", null ],
      [ "~Object", "classframework_1_1core_1_1_object.html#ac934815b59eb1cf1a47bdb114fa86da0", null ],
      [ "ObjectName", "classframework_1_1core_1_1_object.html#aa7f53a758a9fceb50c44751854929960", null ],
      [ "ObjectType", "classframework_1_1core_1_1_object.html#a2374065f2e4c5fbf8878e6a29fbf16b7", null ],
      [ "Parent", "classframework_1_1core_1_1_object.html#a5c95035e30ff6514f1b419fb85448a8c", null ],
      [ "TypeChilds", "classframework_1_1core_1_1_object.html#ac0b037da8cdd7c40b211888442dacd3d", null ],
      [ "Childs", "classframework_1_1core_1_1_object.html#ad7d6e338d564ee997523131d3e53efb6", null ],
      [ "Warning", "classframework_1_1core_1_1_object.html#a8a83ff865df29be24a15e50d7382876e", null ],
      [ "Error", "classframework_1_1core_1_1_object.html#a777476dd40eaf5ca7733c2e1aaeb90dd", null ],
      [ "ErrorOccured", "classframework_1_1core_1_1_object.html#aa57e9e38dcc7f0388071eb54c2b30be4", null ],
      [ "::Widget_impl", "classframework_1_1core_1_1_object.html#a2a6f0753cfcbf314ad7fa952bfe9bc67", null ]
    ] ],
    [ "OneAxeRotation", "classframework_1_1core_1_1_one_axe_rotation.html", [
      [ "OneAxeRotation", "classframework_1_1core_1_1_one_axe_rotation.html#ab438f2f325654f5b4ce8915071129fb5", null ],
      [ "~OneAxeRotation", "classframework_1_1core_1_1_one_axe_rotation.html#a1a0f14b3178978850f1abf75301f596b", null ],
      [ "ComputeRotation", "classframework_1_1core_1_1_one_axe_rotation.html#ad84b2d212f3244042810379803360608", null ],
      [ "ComputeRotation", "classframework_1_1core_1_1_one_axe_rotation.html#a89b707bd300b820e01196509c55af258", null ]
    ] ],
    [ "Picture", "classframework_1_1core_1_1_picture.html", [
      [ "Picture", "classframework_1_1core_1_1_picture.html#a1262677bfdba7c794e27a73136d38923", null ],
      [ "~Picture", "classframework_1_1core_1_1_picture.html#a62ff73ece223d130ca9d9d532a7c19a4", null ]
    ] ],
    [ "Point2D", "classframework_1_1core_1_1_point2_d.html", [
      [ "Point2D", "classframework_1_1core_1_1_point2_d.html#a4350767d5a88df2f35c4ef44cef00dcd", null ],
      [ "Point2D", "classframework_1_1core_1_1_point2_d.html#a0f47da9853e43444af492d9cf946f61f", null ],
      [ "~Point2D", "classframework_1_1core_1_1_point2_d.html#abcb117e4375a3eef413cbc76d4a6a169", null ],
      [ "CopyFrom", "classframework_1_1core_1_1_point2_d.html#a173752b80dae7af59b183a40a872ea33", null ],
      [ "Rotate", "classframework_1_1core_1_1_point2_d.html#aaf8ae73f6077a9507ab6c8d9ae8cc2fd", null ],
      [ "RotateDeg", "classframework_1_1core_1_1_point2_d.html#a1a78720641bad669c6625c606a445779", null ],
      [ "Norm", "classframework_1_1core_1_1_point2_d.html#a76f3d08aa61755a5dc194541d2578f3c", null ],
      [ "Normalize", "classframework_1_1core_1_1_point2_d.html#a9c3b954b47d3ecaa6b0e87d1b5ec2680", null ],
      [ "x", "classframework_1_1core_1_1_point2_d.html#a2001a99f4cec3a8688169925e99b4913", null ],
      [ "y", "classframework_1_1core_1_1_point2_d.html#a7612176298951461df14dc9ae00cb49f", null ]
    ] ],
    [ "Point3D", "classframework_1_1core_1_1_point3_d.html", [
      [ "Point3D", "classframework_1_1core_1_1_point3_d.html#a5c7531cca7b5c3a81a34b4cc46208d26", null ],
      [ "Point3D", "classframework_1_1core_1_1_point3_d.html#ad76e44929f1acd3c609107641bedac85", null ],
      [ "~Point3D", "classframework_1_1core_1_1_point3_d.html#af1ee45d703ac5a36f35b721811c6aed9", null ],
      [ "CopyFrom", "classframework_1_1core_1_1_point3_d.html#a5f83d387f92bb005be1ca5c5231884a5", null ],
      [ "RotateX", "classframework_1_1core_1_1_point3_d.html#a7fa558a2d1319abd99d2497324b2253e", null ],
      [ "RotateXDeg", "classframework_1_1core_1_1_point3_d.html#ac6b0f5db7980c641ada678f963837d5f", null ],
      [ "RotateY", "classframework_1_1core_1_1_point3_d.html#a28e7a11fc017e4c660d4ab45a7c9236f", null ],
      [ "RotateYDeg", "classframework_1_1core_1_1_point3_d.html#a28d83a4869b847e617933c905df9e0e5", null ],
      [ "RotateZ", "classframework_1_1core_1_1_point3_d.html#a5c39f3563aad84ed29d443647e901b15", null ],
      [ "RotateZDeg", "classframework_1_1core_1_1_point3_d.html#a1fc238d5f6845e00743b958648b0907d", null ],
      [ "To2Dxy", "classframework_1_1core_1_1_point3_d.html#aa08eba1310db059ad09d15ed5bd5f7be", null ],
      [ "Norm", "classframework_1_1core_1_1_point3_d.html#a80a4803e4f2b04f56efaff7ecb1850ac", null ],
      [ "Normalize", "classframework_1_1core_1_1_point3_d.html#a29cd11912dfc2cee517421fe7a4a8274", null ],
      [ "x", "classframework_1_1core_1_1_point3_d.html#acd87f77c6f9b7910503b428c039062dc", null ],
      [ "y", "classframework_1_1core_1_1_point3_d.html#a559222769c545076728647cc164759e6", null ],
      [ "z", "classframework_1_1core_1_1_point3_d.html#a26d3078830242ae0514109d02f072734", null ]
    ] ],
    [ "PushButton", "classframework_1_1core_1_1_push_button.html", [
      [ "PushButton", "classframework_1_1core_1_1_push_button.html#a2d6f7d66273882dc37e7cd5665fc4ec8", null ],
      [ "~PushButton", "classframework_1_1core_1_1_push_button.html#a029c811aac0a5568e65cb724aeb09d84", null ],
      [ "Clicked", "classframework_1_1core_1_1_push_button.html#a6782ae70637cae092943d31f373fc388", null ]
    ] ],
    [ "SendData", "classframework_1_1core_1_1_send_data.html", [
      [ "SendData", "classframework_1_1core_1_1_send_data.html#ac850a4cfcbe761023d0dd9f4f68b2e88", null ],
      [ "~SendData", "classframework_1_1core_1_1_send_data.html#a4cc9949b1ac1d0c36569a8bc9b67127f", null ],
      [ "SendSize", "classframework_1_1core_1_1_send_data.html#a191a64ed3b4729803139309217dc2b72", null ],
      [ "SendPeriod", "classframework_1_1core_1_1_send_data.html#a42a9877870a32c317469367652cd7c69", null ],
      [ "IsEnabled", "classframework_1_1core_1_1_send_data.html#aeda326390ccd71ed0130e58cfb423ddf", null ],
      [ "SetEnabled", "classframework_1_1core_1_1_send_data.html#a6fa0afd5bc9399b979dd45423b3e0186", null ],
      [ "SetSendSize", "classframework_1_1core_1_1_send_data.html#a87d34d5e6a02e1a6c956ece0e9d958e1", null ],
      [ "SetSendPeriod", "classframework_1_1core_1_1_send_data.html#ad49e9f0815834da44c2452089285fd7d", null ],
      [ "::ui_com", "classframework_1_1core_1_1_send_data.html#a55412ffb5b5723ec3af4e163141276ce", null ]
    ] ],
    [ "SharedMem", "classframework_1_1core_1_1_shared_mem.html", [
      [ "SharedMem", "classframework_1_1core_1_1_shared_mem.html#a6134fbfd60c492613a991c80500a315f", null ],
      [ "~SharedMem", "classframework_1_1core_1_1_shared_mem.html#a973d796cd5fa6f61cc5dc55445b8d021", null ],
      [ "Write", "classframework_1_1core_1_1_shared_mem.html#ad78337f7be1b77d9e166b7b3d50a03ea", null ],
      [ "Read", "classframework_1_1core_1_1_shared_mem.html#a7dabbed4810b77ce137da18a86e9e2f1", null ]
    ] ],
    [ "Socket", "classframework_1_1core_1_1_socket.html", [
      [ "Socket", "classframework_1_1core_1_1_socket.html#a140496070bc697ae080dcfbb2c43635e", null ],
      [ "Socket", "classframework_1_1core_1_1_socket.html#acae17939935c2dfc710313a4032dac5c", null ],
      [ "~Socket", "classframework_1_1core_1_1_socket.html#a70089bda092e48d729cc4afe0e5c1668", null ],
      [ "SendMessage", "classframework_1_1core_1_1_socket.html#a9f8c016a5eee95d7d3b3749f6d43314d", null ],
      [ "SendMessage", "classframework_1_1core_1_1_socket.html#a06ca7376cbc9eaf476f2651005569889", null ],
      [ "RecvMessage", "classframework_1_1core_1_1_socket.html#aa07455775017781fdf8981e7ad6bd44b", null ]
    ] ],
    [ "SpinBox", "classframework_1_1core_1_1_spin_box.html", [
      [ "SpinBox", "classframework_1_1core_1_1_spin_box.html#aed7d941a3459dc4644483ebe409dda1f", null ],
      [ "~SpinBox", "classframework_1_1core_1_1_spin_box.html#a83f83509a9fd9cf651683eb78572887d", null ],
      [ "Value", "classframework_1_1core_1_1_spin_box.html#a61b1f9e56fcb71da79c800a3bfba1aa0", null ]
    ] ],
    [ "Tab", "classframework_1_1core_1_1_tab.html", [
      [ "Tab", "classframework_1_1core_1_1_tab.html#a4be833ca205bd067f00d46dc5153e22c", null ],
      [ "~Tab", "classframework_1_1core_1_1_tab.html#aa1a45343e383defe6d479591532928a1", null ]
    ] ],
    [ "TabWidget", "classframework_1_1core_1_1_tab_widget.html", [
      [ "TabPosition_t", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34", [
        [ "North", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34adad34152a2a6b2fd606f624083619457", null ],
        [ "South", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34a826d3aaf70da79bd7f4d9d96f9cacd91", null ],
        [ "West", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34abdd7368bae8016860670373b8ecce600", null ],
        [ "East", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34abab58ad7e9819f71b0ef480ae173752c", null ]
      ] ],
      [ "TabWidget", "classframework_1_1core_1_1_tab_widget.html#a8e00a5f3b6f6666efb26b4cbd85def7a", null ],
      [ "~TabWidget", "classframework_1_1core_1_1_tab_widget.html#a4e0c903c8137436d989e72c1d9e69b71", null ]
    ] ],
    [ "TextEdit", "classframework_1_1core_1_1_text_edit.html", [
      [ "TextEdit", "classframework_1_1core_1_1_text_edit.html#a6ae93a0fa862e168239881537fd93b2a", null ],
      [ "~TextEdit", "classframework_1_1core_1_1_text_edit.html#a181973a7c6febef9c0869151fba3cfd7", null ],
      [ "Append", "classframework_1_1core_1_1_text_edit.html#ae053cc7a87ca8d33438350adc40ea995", null ]
    ] ],
    [ "Thread", "classframework_1_1core_1_1_thread.html", [
      [ "Thread", "classframework_1_1core_1_1_thread.html#adb20cdf1df26689dc45fa701ce4a99f8", null ],
      [ "~Thread", "classframework_1_1core_1_1_thread.html#a27a72866b343b8734fd4427515d1e38f", null ],
      [ "Start", "classframework_1_1core_1_1_thread.html#aeea6854b038e4cc558dfb8dbfc6f47fd", null ],
      [ "SafeStop", "classframework_1_1core_1_1_thread.html#acd61dc2aef8db4619f5d65eb0aca3d40", null ],
      [ "ToBeStopped", "classframework_1_1core_1_1_thread.html#aee57d8d4b7be3bff804f689ed7619c61", null ],
      [ "Join", "classframework_1_1core_1_1_thread.html#a74b2dd5fda4079a47f67d89b832a02d1", null ],
      [ "SetPeriodUS", "classframework_1_1core_1_1_thread.html#a7aefccd25fe4a0dd6fa76d03090b9bb2", null ],
      [ "SetPeriodMS", "classframework_1_1core_1_1_thread.html#a3d7e006f50eb03f350c251ad7b0a8924", null ],
      [ "WaitPeriod", "classframework_1_1core_1_1_thread.html#a5faf16660e7e13a6188523901c315e79", null ],
      [ "WaitUpdate", "classframework_1_1core_1_1_thread.html#a9747144537544afaeb20b4389b92fc7c", null ],
      [ "Suspend", "classframework_1_1core_1_1_thread.html#af9c6b84365e82242f6066d15b783e59c", null ],
      [ "Resume", "classframework_1_1core_1_1_thread.html#a998454819371b74a979ac4e5ba362c51", null ],
      [ "IsSuspended", "classframework_1_1core_1_1_thread.html#a4589e644fcd554ecd4db51ddd55a9744", null ],
      [ "SleepUntil", "classframework_1_1core_1_1_thread.html#ad203bf7f23baa51488c46c712c4f411b", null ],
      [ "SleepUS", "classframework_1_1core_1_1_thread.html#afc85802579cdbf15562d1f2ab0b165c9", null ],
      [ "SleepMS", "classframework_1_1core_1_1_thread.html#a56a248f27a43163af66ddefacabc465a", null ],
      [ "WarnUponSwitches", "classframework_1_1core_1_1_thread.html#a6c6e2c6e3c7b8e5b9730a21eb175c193", null ],
      [ "::Thread_impl", "classframework_1_1core_1_1_thread.html#a8d4dacaaa8b2d282a4c38c48281387c7", null ]
    ] ],
    [ "Widget", "classframework_1_1core_1_1_widget.html", [
      [ "Widget", "classframework_1_1core_1_1_widget.html#aa30cb75a7f8a124b3af428f82a39b4f5", null ],
      [ "~Widget", "classframework_1_1core_1_1_widget.html#aae79ffe891a89051f49c06712504c803", null ],
      [ "setEnabled", "classframework_1_1core_1_1_widget.html#ada1caf49bfb4d7c7deed40c2fd0ee30b", null ],
      [ "isEnabled", "classframework_1_1core_1_1_widget.html#adcbc2dbcfa873b214e073ddf34454941", null ],
      [ "XmlFileNode", "classframework_1_1core_1_1_widget.html#a1f86603bd32daaa93995c787bbc339ac", null ],
      [ "SetXmlProp", "classframework_1_1core_1_1_widget.html#a12b9673b559c5fab03b83b8c53c56be7", null ],
      [ "SetXmlProp", "classframework_1_1core_1_1_widget.html#a57285110293bb4c4b49d369ae5008ac7", null ],
      [ "SetXmlProp", "classframework_1_1core_1_1_widget.html#af5b9680d64193febda2ce8f3bcaef359", null ],
      [ "SetXmlProp", "classframework_1_1core_1_1_widget.html#ab4455e21008c653be1dbb17d28b3f90c", null ],
      [ "ClearXmlProps", "classframework_1_1core_1_1_widget.html#a0fc33ff224c830a3cc87917277466313", null ],
      [ "AddXmlChild", "classframework_1_1core_1_1_widget.html#ab0d4a1076571069fa03ed2cf83dab797", null ],
      [ "SendXml", "classframework_1_1core_1_1_widget.html#aa32e3b844c6de42bbd6e8b480b71f90b", null ],
      [ "XmlEvent", "classframework_1_1core_1_1_widget.html#a914491ccf9073b96920aec5da0e851a1", null ],
      [ "::FrameworkManager_impl", "classframework_1_1core_1_1_widget.html#a85f53b6422522c0e6d8c02a3fda425e8", null ],
      [ "::Widget_impl", "classframework_1_1core_1_1_widget.html#a2a6f0753cfcbf314ad7fa952bfe9bc67", null ]
    ] ],
    [ "operator+", "group___framework.html#gaedd06c17ee66a43da9ef3ebd0c565250", null ],
    [ "operator-", "group___framework.html#ga503269bce7b948ca68588cc7c7d83b59", null ],
    [ "operator/", "group___framework.html#ga3af6cffbce87bb3918c5174f93d86e1e", null ],
    [ "operator*", "group___framework.html#ga9196f753abeb4364834319c665bfa792", null ],
    [ "operator*", "group___framework.html#ga05f06d40e476ca8b5e9fe497420982c8", null ],
    [ "operator+", "group___framework.html#ga3a6fe0d09f1df1f2dd23f1839bd69434", null ],
    [ "operator-", "group___framework.html#ga539344c95d15ef0c57c27e6da2003199", null ],
    [ "operator/", "group___framework.html#gaecac76e8663d9753447aa55be76b1235", null ],
    [ "operator*", "group___framework.html#ga33484545de8b98685219f37ce73d1095", null ],
    [ "operator*", "group___framework.html#gab02387395c046e22c2c2928cdf51a811", null ]
];