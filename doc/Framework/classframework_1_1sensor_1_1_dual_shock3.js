var classframework_1_1sensor_1_1_dual_shock3 =
[
    [ "Led", "classframework_1_1sensor_1_1_dual_shock3.html#a12d4a15438114ca28543330e6992d7f9", [
      [ "led1", "classframework_1_1sensor_1_1_dual_shock3.html#a12d4a15438114ca28543330e6992d7f9a3db486f35ac42174122c5a026daf7ae2", null ],
      [ "led2", "classframework_1_1sensor_1_1_dual_shock3.html#a12d4a15438114ca28543330e6992d7f9a55f794a51aae613d54044a4f0f6af2c8", null ],
      [ "led3", "classframework_1_1sensor_1_1_dual_shock3.html#a12d4a15438114ca28543330e6992d7f9a6f776d53da650d6857b572698a0d423d", null ],
      [ "led4", "classframework_1_1sensor_1_1_dual_shock3.html#a12d4a15438114ca28543330e6992d7f9a378814e64d26bc4d9af6372a3e33b07a", null ]
    ] ],
    [ "ButtonsMask", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6", [
      [ "start", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a3a6c3addeef0c24c5e553c35085695aa", null ],
      [ "select", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a3502b75820b1f7294cb483573096e95c", null ],
      [ "square", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a60f17bb3b04d4f1e15ef58401103fa87", null ],
      [ "triangle", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a2bd30b665994efac1149963b1ecfa454", null ],
      [ "circle", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a51a8f0129ad16c69215a184be30adb56", null ],
      [ "cross", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a5917cb8d8930fc3f8915f8152e1a5972", null ],
      [ "L1", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6aadecdb37f7ea7c09fdf3150cf1e6c342", null ],
      [ "L2", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6aaf44d051001f10371ee033359517191e", null ],
      [ "L3", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a5899e587e7865c29b8c5c86885594688", null ],
      [ "R1", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a2fd39dd49fdad77949e2f65afcda90ed", null ],
      [ "R2", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a1f9c9fa52ad9009acaf58b2389318540", null ],
      [ "R3", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6adc61aac08a044ba079835461e8f1a3d9", null ],
      [ "up", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6ac47daa4bad0b562444f261005b4fc4b6", null ],
      [ "down", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6adf7a9617a61c856f8abce68c66545308", null ],
      [ "left", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a94ef8723105f6f8dce2c82d7f8fb1571", null ],
      [ "right", "classframework_1_1sensor_1_1_dual_shock3.html#a2e3ace37b5ba6e96cadf5bfc2b54dcd6a2bcf64255c1d016fe997f180f63314e9", null ]
    ] ],
    [ "DualShock3", "classframework_1_1sensor_1_1_dual_shock3.html#a1ffb1ccdcb18d4e5de64668889351ad6", null ],
    [ "~DualShock3", "classframework_1_1sensor_1_1_dual_shock3.html#a9b574b60b1f94d99f9fb0c0f9d11436b", null ],
    [ "Roll", "classframework_1_1sensor_1_1_dual_shock3.html#aef65ba7ed38ff8d5868a5b31bbc08699", null ],
    [ "Pitch", "classframework_1_1sensor_1_1_dual_shock3.html#a157e0594dfce6fae75c982a0e7ca229d", null ],
    [ "Yaw", "classframework_1_1sensor_1_1_dual_shock3.html#a814a8bacfc207d0ad5ebcd8d891bd624", null ],
    [ "Thrust", "classframework_1_1sensor_1_1_dual_shock3.html#a420911fa7dcaa41f6496e30e9f918e0b", null ],
    [ "Rumble", "classframework_1_1sensor_1_1_dual_shock3.html#a411f9634be4a85c3d123439ba9575511", null ],
    [ "SetLedON", "classframework_1_1sensor_1_1_dual_shock3.html#a5a189446d97114dd23df579158582a54", null ],
    [ "SetLedOFF", "classframework_1_1sensor_1_1_dual_shock3.html#a10cd771918ac5f012e61caf43997d71d", null ],
    [ "FlashLed", "classframework_1_1sensor_1_1_dual_shock3.html#a8625a3794d17adfea79ffd5169b9fc52", null ],
    [ "IsConnected", "classframework_1_1sensor_1_1_dual_shock3.html#a64c63d32e62c06a508c5d135e2589169", null ],
    [ "GetTab", "classframework_1_1sensor_1_1_dual_shock3.html#aa009682a8b3a0e86d62d42bcf0cd5952", null ],
    [ "ButtonsPressed", "classframework_1_1sensor_1_1_dual_shock3.html#aa2c5c8eda02b0100eff8174e00a2de45", null ],
    [ "::DualShock3_impl", "classframework_1_1sensor_1_1_dual_shock3.html#a10b8e5bc94b21203b9558a6d7a273563", null ]
];