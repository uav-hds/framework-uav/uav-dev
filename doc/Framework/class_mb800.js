var class_mb800 =
[
    [ "FixQuality", "class_mb800.html#af58525605589f875803fa4fee1d29b96", [
      [ "Invalid", "class_mb800.html#af58525605589f875803fa4fee1d29b96aa187f99b7d0c374d4311d759a4530b1e", null ],
      [ "Gps", "class_mb800.html#af58525605589f875803fa4fee1d29b96ac33b05ac62090e2cce00c5268eea5a14", null ],
      [ "DGps", "class_mb800.html#af58525605589f875803fa4fee1d29b96a8a73317accd327dc4038ff40326b1b12", null ],
      [ "Pps", "class_mb800.html#af58525605589f875803fa4fee1d29b96a4a297abf554d0008dc48a0c31cdf73ed", null ],
      [ "Rtk", "class_mb800.html#af58525605589f875803fa4fee1d29b96ab151156c72bcab812d16f7bc7e61d89d", null ],
      [ "RtkFloat", "class_mb800.html#af58525605589f875803fa4fee1d29b96a9de3764c421bb4c76bbaa6368549988c", null ],
      [ "Estimated", "class_mb800.html#af58525605589f875803fa4fee1d29b96accce6e25530a5c02a0e4d8aab14da9b8", null ],
      [ "Manual", "class_mb800.html#af58525605589f875803fa4fee1d29b96ac76edbff0ffcebe49ec43061b0794169", null ],
      [ "Simulation", "class_mb800.html#af58525605589f875803fa4fee1d29b96a2c6c1a2f800803aade0b6c6fe44c24d8", null ]
    ] ],
    [ "NMEAFlags", "class_mb800.html#a644318e16275aef1566e36485c093102", [
      [ "GGA", "class_mb800.html#a644318e16275aef1566e36485c093102a2bb55307628edd0e56cf3acebb573dfb", null ],
      [ "VTG", "class_mb800.html#a644318e16275aef1566e36485c093102a1e93ab863f6e9002bff310930a8c7a5d", null ],
      [ "GST", "class_mb800.html#a644318e16275aef1566e36485c093102ac810eb8d7bbba5d80a8ec9620a2e7974", null ]
    ] ],
    [ "Mb800", "class_mb800.html#ad12e1bf8b67349f01e96657b149b46ab", null ],
    [ "~Mb800", "class_mb800.html#a311d6eadebc06c2db8c6fbbe7f3da0f4", null ],
    [ "UseDefaultPlot", "class_mb800.html#a051526f0784c8d1732df87c74c592995", null ],
    [ "EPlot", "class_mb800.html#acdf30b6c42e3dcc4805faad7f8ee7c4f", null ],
    [ "NPlot", "class_mb800.html#a9b278583df198980d8a592cfbc29471a", null ],
    [ "UPlot", "class_mb800.html#acebb125d35f1f338a750318fe6f9374e", null ],
    [ "VEPlot", "class_mb800.html#ac12cc2c1a2e3320e2b1423968ec036b1", null ],
    [ "VNPlot", "class_mb800.html#a5cbc91d4f1a958b9bbefbcbc8b71d83a", null ],
    [ "MainTab", "class_mb800.html#a526a695e6f80f246c80717d641efcd7b", null ],
    [ "SetupLayout", "class_mb800.html#a826f2751ff7a38c9e4bc92c4e40baecd", null ],
    [ "PlotTab", "class_mb800.html#a49fb0859c78390fcd769584230d09e82", null ],
    [ "NbSat", "class_mb800.html#aa5864025097e5116d828feae1a8f4573", null ],
    [ "Fix", "class_mb800.html#ac48670e4068c89aded2afaab3436eaa1", null ],
    [ "SetRef", "class_mb800.html#adc5ae590395efb32ba8c1377919d8f64", null ],
    [ "GetENUPosition", "class_mb800.html#ab0b46a5277a9c9f9b27bf6cdcba2716f", null ]
];