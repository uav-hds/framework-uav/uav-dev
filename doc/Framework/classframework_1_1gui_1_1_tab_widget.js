var classframework_1_1gui_1_1_tab_widget =
[
    [ "TabPosition_t", "classframework_1_1gui_1_1_tab_widget.html#a1fd66ab0fc37386aec5ac84b0c503f15", [
      [ "North", "classframework_1_1gui_1_1_tab_widget.html#a1fd66ab0fc37386aec5ac84b0c503f15ab72a29c743ce42d87ed177111eb75ba3", null ],
      [ "South", "classframework_1_1gui_1_1_tab_widget.html#a1fd66ab0fc37386aec5ac84b0c503f15a65b697a5afd117dcc0df574097dc028f", null ],
      [ "West", "classframework_1_1gui_1_1_tab_widget.html#a1fd66ab0fc37386aec5ac84b0c503f15a6ae56e8eae04d9b6763c6e36137d1913", null ],
      [ "East", "classframework_1_1gui_1_1_tab_widget.html#a1fd66ab0fc37386aec5ac84b0c503f15affab08f2a7f9d2fe8f183e7c3fd6224a", null ]
    ] ],
    [ "TabWidget", "classframework_1_1gui_1_1_tab_widget.html#a05cd8af1d3f13ea282e218e1d96fefd9", null ],
    [ "~TabWidget", "classframework_1_1gui_1_1_tab_widget.html#abe592a35b9e4de8e7439c8f79652484b", null ]
];