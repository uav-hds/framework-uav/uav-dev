var class_imu_data =
[
    [ "AvailableData_t", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910", [
      [ "HasEulerAngles", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910a09c43eb2b841305f9a6bf1668443f422", null ],
      [ "HasOrientationMatrix", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910a666e8d86f5e913abe49ad26161253e94", null ],
      [ "HasQuaternions", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910ab389624fe6e59c17f43f6287b545fed6", null ],
      [ "HasFilteredAngRates", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910a66ae845f7021f929a2ba9e41c3445a6e", null ],
      [ "HasRawAcc", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910a11054eb393e9b78a4800ad2c98d09ecc", null ],
      [ "HasRawGyr", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910a0539d0b6f00fc6b3eb342ae7ebb53b98", null ],
      [ "HasRawMag", "class_imu_data.html#a471758b895e4926a2b2d447e5a298910a4989fc659c35e3702aa99833725bf4b1", null ]
    ] ],
    [ "PlotableData_t", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2e", [
      [ "Roll", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea8caf3304467884858476fd3d8ce689ed", null ],
      [ "Pitch", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2eae3a2f825607c19d5d2107a732c65b5d0", null ],
      [ "Yaw", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2eac3714062bab80c5d7d950b78584323b6", null ],
      [ "Wx", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea79092f7f3f6b415d10a448dd2b1d1589", null ],
      [ "Wy", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea84ad89eb706220196e8f018943e0a324", null ],
      [ "Wz", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2eaff1d775e1e7f5a077b14591eafb53cd4", null ],
      [ "RawAx", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea897af6988135c48f17a5d4d3e494c4e8", null ],
      [ "RawAy", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea7b4e848f8cb4d02f28ce402721f2499b", null ],
      [ "RawAz", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea3617fed2f40a7233d8cebf78c1ec2e26", null ],
      [ "RawGx", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea5cae382e9770b70fcef745fd10155f49", null ],
      [ "RawGy", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea3dd71fff102191b1f51cc7b1395186e3", null ],
      [ "RawGz", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea3083572cbfa843806d4803a00aa0b284", null ],
      [ "RawMx", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea259e54de962f15f735d7cb4dc13c8174", null ],
      [ "RawMy", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2ea29ff0e5c6afaf148e1d3ab8c8682b190", null ],
      [ "RawMz", "class_imu_data.html#af14bdab2d22d17a91a29726bb65b2c2eac7cc0c1db2c112d8fdc24d2fbeacdc80", null ]
    ] ],
    [ "Conversion_t", "class_imu_data.html#a48880f5dd6f58d8df2dfcd6f9572a993", [
      [ "None", "class_imu_data.html#a48880f5dd6f58d8df2dfcd6f9572a993af67ed50facc7f6808408cf7ed360038e", null ],
      [ "Rad2Deg", "class_imu_data.html#a48880f5dd6f58d8df2dfcd6f9572a993aeba2567886e8d82342c40a6b607a4997", null ]
    ] ],
    [ "ImuData", "class_imu_data.html#ac6bfa7f4c5f02166053863b84c910b78", null ],
    [ "~ImuData", "class_imu_data.html#ab86820b80e8a5279ea300d7899ea59c1", null ],
    [ "AvailableData", "class_imu_data.html#abbdafe31f93725b6acca48b35dd41de0", null ],
    [ "Convert", "class_imu_data.html#ae9c9c706f9ced906ef54103da20ead60", null ],
    [ "EulerAngles", "class_imu_data.html#ad256f0a15648e0608851f84f2a653fe3", null ],
    [ "FilteredAngRates", "class_imu_data.html#a6f06f4a7e47e943891ea52a1a0286d59", null ],
    [ "RawAcc", "class_imu_data.html#a54a873de5be5dfdeb0b839aae0274c61", null ],
    [ "RawGyr", "class_imu_data.html#a7c99d52cd34567d0c37fa041cb9c2559", null ],
    [ "RawMag", "class_imu_data.html#ae39757910b23013ef1c15f873a3c7575", null ]
];