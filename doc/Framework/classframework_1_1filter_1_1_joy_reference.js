var classframework_1_1filter_1_1_joy_reference =
[
    [ "JoyReference", "classframework_1_1filter_1_1_joy_reference.html#a59af735b7143824e0fe3cff0ab267585", null ],
    [ "~JoyReference", "classframework_1_1filter_1_1_joy_reference.html#ae5db72ea69cfaad196c354c18706bd64", null ],
    [ "SetRollAxis", "classframework_1_1filter_1_1_joy_reference.html#aed9b7b4147df96120afa04bae90fa093", null ],
    [ "SetPitchAxis", "classframework_1_1filter_1_1_joy_reference.html#a87725683b98bf5db43c1a2c63e95ec09", null ],
    [ "SetYawAxis", "classframework_1_1filter_1_1_joy_reference.html#a49eb37763c26362392f70c009feec63d", null ],
    [ "SetAltitudeAxis", "classframework_1_1filter_1_1_joy_reference.html#a5058fba00ed373fc387269274461f09b", null ],
    [ "GetReferenceOrientation", "classframework_1_1filter_1_1_joy_reference.html#abf8b2195e8badf782840c53f94d22f7d", null ],
    [ "ZRef", "classframework_1_1filter_1_1_joy_reference.html#a90f400edb993764122e1d8a7540c92b9", null ],
    [ "DzRef", "classframework_1_1filter_1_1_joy_reference.html#a7885c91e037363787d37c7a278e067a8", null ],
    [ "RollTrim", "classframework_1_1filter_1_1_joy_reference.html#a7192add19899c5291e352e6193edd27f", null ],
    [ "PitchTrim", "classframework_1_1filter_1_1_joy_reference.html#af27a681eef9e8874e6b76a9e1d737c20", null ],
    [ "SetYawRef", "classframework_1_1filter_1_1_joy_reference.html#a8da2ccc8a2a0c696b21b27fa36ec53cb", null ],
    [ "SetYawRef", "classframework_1_1filter_1_1_joy_reference.html#a53b8d74d331bc908c6095931e979db09", null ],
    [ "SetZRef", "classframework_1_1filter_1_1_joy_reference.html#af8c6c9ce5441f77339447815439e88e1", null ],
    [ "RollTrimUp", "classframework_1_1filter_1_1_joy_reference.html#ae48a3e27f41d9247ccfade2b9f3fb1f5", null ],
    [ "RollTrimDown", "classframework_1_1filter_1_1_joy_reference.html#a1ab4dc786eb641580b9f7140c7d72f2d", null ],
    [ "PitchTrimUp", "classframework_1_1filter_1_1_joy_reference.html#afafab646ac224c6c0a8627923bee541d", null ],
    [ "PitchTrimDown", "classframework_1_1filter_1_1_joy_reference.html#a320d203d6cfb69a868c1c34b594d0b55", null ],
    [ "Update", "classframework_1_1filter_1_1_joy_reference.html#a788f1ac1ee088fa12ea19061b8572ac6", null ]
];