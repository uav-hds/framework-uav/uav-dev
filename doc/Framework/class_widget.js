var class_widget =
[
    [ "Widget", "class_widget.html#a92227be425c28c8a90a237387a45917f", null ],
    [ "~Widget", "class_widget.html#a714cf798aadb4d615f6f60a355382c02", null ],
    [ "setEnabled", "class_widget.html#a7e0be0e1c644a569fffe5a03880af117", null ],
    [ "isEnabled", "class_widget.html#a012d30c854517c32624b8e6521e5fae1", null ],
    [ "XmlFileNode", "class_widget.html#a48aafd6b6654b5c024252aac4bc70ac5", null ],
    [ "SetXmlProp", "class_widget.html#abbbea3a6749c267547844b347fd71ef6", null ],
    [ "SetXmlProp", "class_widget.html#aebff38bd81ddf78e2429a786b34f2755", null ],
    [ "SetXmlProp", "class_widget.html#a90a0d549ba4b1b7e481af845b40228a9", null ],
    [ "SetXmlProp", "class_widget.html#aad467b6941c623edcbcf445f9ac97f2a", null ],
    [ "ClearXmlProps", "class_widget.html#a1d447fddf5753dba0bb7cf057ab62a60", null ],
    [ "AddXmlChild", "class_widget.html#ae6d73c33036295d6f215e108bfb101e7", null ],
    [ "SendXml", "class_widget.html#a4d1ec3587da07ee941bdb6b1f067b8bd", null ],
    [ "XmlEvent", "class_widget.html#a1b9fe99cf5a0075b0770ba2898a2f5d4", null ],
    [ "FrameworkManager_impl", "class_widget.html#aa1fddf55f23f38ddffc79a935c676e84", null ],
    [ "Widget_impl", "class_widget.html#aea54e136a2f5a2d023439ac4f3ef1593", null ]
];