var classframework_1_1core_1_1_ahrs_data =
[
    [ "Type", "classframework_1_1core_1_1_ahrs_data_1_1_type.html", "classframework_1_1core_1_1_ahrs_data_1_1_type" ],
    [ "PlotableData_t", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14b", [
      [ "Roll", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14bac73bab58b7b9b7d8f5a38ee69185df06", null ],
      [ "Pitch", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba8ceebad98e6c32b48191c1b91911ac69", null ],
      [ "Yaw", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba8ffe883c697f37e8f1a9fd98b908b218", null ],
      [ "RollDeg", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba45b4279c6bca4308c6773b9df96fc315", null ],
      [ "PitchDeg", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba59bebd35dc270fa9209c23c7411027ed", null ],
      [ "YawDeg", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba66d7ed7097ba801a3274d53f7816b7ba", null ],
      [ "Q0", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14baefc1ef210ae6c23d1653608c8af379cf", null ],
      [ "Q1", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba5b17bb92bef6b3b72f9646244c393ded", null ],
      [ "Q2", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba50135c88fabe7aa72dbbd7d0f32a9e0d", null ],
      [ "Q3", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba28c8b81ce4a52a092dc2c9b2ac44bb8a", null ],
      [ "Wx", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba4b35fa6b3cdf75c452864de2729ee897", null ],
      [ "Wy", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14baa3f182d46801dfe271aef0ef11f8c557", null ],
      [ "Wz", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14baa70cc5fd4261e643994c7e3f2d9da19d", null ],
      [ "WxDeg", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba32aa3381d0e9a980b4b314337dcf506d", null ],
      [ "WyDeg", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14bae300333ef9ddbc283fc2ec20e22ec124", null ],
      [ "WzDeg", "classframework_1_1core_1_1_ahrs_data.html#a9730129235a6b9aa030647d7ec0aa14ba046e735068a68eced22640a5c2e14ef3", null ]
    ] ],
    [ "AhrsData", "classframework_1_1core_1_1_ahrs_data.html#a97252d537eb6e8c04e86445dd58cd045", null ],
    [ "~AhrsData", "classframework_1_1core_1_1_ahrs_data.html#a3d4f6da5be75f1bf1983f1d4ec44bc55", null ],
    [ "Element", "classframework_1_1core_1_1_ahrs_data.html#a5118c4a6eb12e298e2844884e18b2117", null ],
    [ "SetQuaternion", "classframework_1_1core_1_1_ahrs_data.html#a795b2e75a29c88989906573ff376f682", null ],
    [ "GetQuaternion", "classframework_1_1core_1_1_ahrs_data.html#a01a88b745d1cb6e844033157a6de7252", null ],
    [ "SetAngularRates", "classframework_1_1core_1_1_ahrs_data.html#a98afe5226b33e47b5613aa0173c587da", null ],
    [ "GetAngularRates", "classframework_1_1core_1_1_ahrs_data.html#a23d98d2f9204e96706b4df4d21e1108e", null ],
    [ "GetQuaternionAndAngularRates", "classframework_1_1core_1_1_ahrs_data.html#a7c6f8419b5f6fc086e31132fecd0f66e", null ],
    [ "SetQuaternionAndAngularRates", "classframework_1_1core_1_1_ahrs_data.html#ace7b631abb013e118a117698c216684c", null ],
    [ "GetDataType", "classframework_1_1core_1_1_ahrs_data.html#aa20a8b209768dcaa2bdaa68d97759334", null ]
];