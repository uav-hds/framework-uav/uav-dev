var classframework_1_1meta_1_1_meta_uav =
[
    [ "AltitudeMode_t", "classframework_1_1meta_1_1_meta_uav.html#a41feb57b95ac86e08b639a1c2621fdc3", [
      [ "Manual", "classframework_1_1meta_1_1_meta_uav.html#a41feb57b95ac86e08b639a1c2621fdc3ae1ba155a9f2e8c3be94020eef32a0301", null ],
      [ "Custom", "classframework_1_1meta_1_1_meta_uav.html#a41feb57b95ac86e08b639a1c2621fdc3a90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "OrientationMode_t", "classframework_1_1meta_1_1_meta_uav.html#a3a4e685462c900ba310e856bae3e29c5", [
      [ "Manual", "classframework_1_1meta_1_1_meta_uav.html#a3a4e685462c900ba310e856bae3e29c5ae1ba155a9f2e8c3be94020eef32a0301", null ],
      [ "Custom", "classframework_1_1meta_1_1_meta_uav.html#a3a4e685462c900ba310e856bae3e29c5a90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "ThrustMode_t", "classframework_1_1meta_1_1_meta_uav.html#a07dc15c60eb144bb431308126501235d", [
      [ "Default", "classframework_1_1meta_1_1_meta_uav.html#a07dc15c60eb144bb431308126501235da7a1920d61156abc05a60135aefe8bc67", null ],
      [ "Custom", "classframework_1_1meta_1_1_meta_uav.html#a07dc15c60eb144bb431308126501235da90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "TorqueMode_t", "classframework_1_1meta_1_1_meta_uav.html#ab5deb9200d641c357177140cd1db341d", [
      [ "Default", "classframework_1_1meta_1_1_meta_uav.html#ab5deb9200d641c357177140cd1db341da7a1920d61156abc05a60135aefe8bc67", null ],
      [ "Custom", "classframework_1_1meta_1_1_meta_uav.html#ab5deb9200d641c357177140cd1db341da90589c47f06eb971d548591f23c285af", null ]
    ] ],
    [ "Event_t", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2", [
      [ "EnteringFailSafeMode", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2acbe7d94f9011b8b4837172c66eda611c", null ],
      [ "EnteringControlLoop", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2affd8e436a03fc288a339dc5799afca23", null ],
      [ "StartLanding", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2a0a0480d215decaf46260e3db52e4dc4c", null ],
      [ "FinishLanding", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2a0912358d703c9a28d93a2707b86161b2", null ],
      [ "Stopped", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2ac23e2b09ebe6bf4cb5e2a9abe85c0be2", null ],
      [ "TakingOff", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2a85916c5f3400cfa25d988f05b6736a94", null ],
      [ "EmergencyStop", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2ac2e719ff1493c7c49d2e5a780450b501", null ],
      [ "Stabilized", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2afda22026db89cdc5e88b262ad9424b41", null ],
      [ "ZTrajectoryFinished", "classframework_1_1meta_1_1_meta_uav.html#ac5bde41fc5c0c91288b66ff374b3fdb2ac5bb3b0bbec39429565b3abce47e5436", null ]
    ] ],
    [ "MetaUav", "classframework_1_1meta_1_1_meta_uav.html#a9a134927d534247e4302811d3cfc8024", null ],
    [ "~MetaUav", "classframework_1_1meta_1_1_meta_uav.html#a80e725a35f74e16403f6b8294344084b", null ],
    [ "GetAltitudeMode", "classframework_1_1meta_1_1_meta_uav.html#a5c83a07286c52ce6a59049953c04ae09", null ],
    [ "SetAltitudeMode", "classframework_1_1meta_1_1_meta_uav.html#a5d81015eb16abf0a70bd2740d897637a", null ],
    [ "GetOrientationMode", "classframework_1_1meta_1_1_meta_uav.html#a87a8035d6e45d98ffbbb9464c3132138", null ],
    [ "SetOrientationMode", "classframework_1_1meta_1_1_meta_uav.html#a5e9a99cb9ac24304b3cb26744416321b", null ],
    [ "GetThrustMode", "classframework_1_1meta_1_1_meta_uav.html#aa1b20ebfba50ba36debd0243539c9dbc", null ],
    [ "SetThrustMode", "classframework_1_1meta_1_1_meta_uav.html#a44f130fc5372b1762a7377697337ae61", null ],
    [ "GetTorqueMode", "classframework_1_1meta_1_1_meta_uav.html#a433d45f7b1efb2202b2973bce4360f96", null ],
    [ "SetTorqueMode", "classframework_1_1meta_1_1_meta_uav.html#af9be462f845f49967f41efbaa4d55715", null ],
    [ "GetCurrentAngles", "classframework_1_1meta_1_1_meta_uav.html#af5c82a6c81175b54987dcd925b396a59", null ],
    [ "GetCurrentAngularSpeed", "classframework_1_1meta_1_1_meta_uav.html#a886caafe3eb5fcabb3d9e74c10121f0d", null ],
    [ "Land", "classframework_1_1meta_1_1_meta_uav.html#a7ba97f29f3643bc9c82d3699325e6f32", null ],
    [ "TakeOff", "classframework_1_1meta_1_1_meta_uav.html#af92ac5726882b7bbd6d638d068146cba", null ],
    [ "EmergencyStop", "classframework_1_1meta_1_1_meta_uav.html#ac2074e85a9f2e8e8282851ea6c24904c", null ],
    [ "SignalEvent", "classframework_1_1meta_1_1_meta_uav.html#aea630b081555aae3ef4072435a46c527", null ],
    [ "AhrsValues", "classframework_1_1meta_1_1_meta_uav.html#ad46a4375f8fba80e3da2eb1443c0632c", null ],
    [ "AltitudeValues", "classframework_1_1meta_1_1_meta_uav.html#a1736f8666230046548528934fbdc510c", null ],
    [ "EnterFailSafeMode", "classframework_1_1meta_1_1_meta_uav.html#a30b52ed2f68360cea126311c06c62c5d", null ],
    [ "ExitFailSafeMode", "classframework_1_1meta_1_1_meta_uav.html#a6b5476f155901139cbcc81bee37f1460", null ],
    [ "FailSafeAhrsValues", "classframework_1_1meta_1_1_meta_uav.html#aa1646a3445748e4ff6e4cecd6c89a4df", null ],
    [ "FailSafeAltitudeValues", "classframework_1_1meta_1_1_meta_uav.html#a6211f239f5f8319bf3749fe00fa3efdb", null ],
    [ "GetButtonsLayout", "classframework_1_1meta_1_1_meta_uav.html#a80af0db56e411d60021fedff32a7c4dc", null ],
    [ "ExtraSecurityCheck", "classframework_1_1meta_1_1_meta_uav.html#a581d7d99c851a6767dfe612a64e325b4", null ],
    [ "ExtraCheckJoystick", "classframework_1_1meta_1_1_meta_uav.html#abe29242d52de3b928e50da4aa2ce7c67", null ],
    [ "ExtraCheckPushButton", "classframework_1_1meta_1_1_meta_uav.html#a5f004fe190352f1487f05412196dcc96", null ],
    [ "GetDefaultReferenceAltitude", "classframework_1_1meta_1_1_meta_uav.html#ab2407870f55e5f8d8335da953a042586", null ],
    [ "GetReferenceAltitude", "classframework_1_1meta_1_1_meta_uav.html#afd151494baff32eb612956b718128a5a", null ],
    [ "GetDefaultThrust", "classframework_1_1meta_1_1_meta_uav.html#aff24844f4234038ae11eb69421be47bc", null ],
    [ "GetThrust", "classframework_1_1meta_1_1_meta_uav.html#a257606ae66e8d278e18ed6da36c7d9fc", null ],
    [ "GetDefaultReferenceAngles", "classframework_1_1meta_1_1_meta_uav.html#a1e778bc4f505fa1e1f3ce937ae9b6e9d", null ],
    [ "GetReferenceAngles", "classframework_1_1meta_1_1_meta_uav.html#a34be7960212b29cc40810faa30d53a84", null ],
    [ "GetDefaultTorques", "classframework_1_1meta_1_1_meta_uav.html#a80ebd6c7cf0c96f49c92b21f4dfac7de", null ],
    [ "GetTorques", "classframework_1_1meta_1_1_meta_uav.html#afd4c687b1a67b583813595f4603f73ad", null ],
    [ "joy", "classframework_1_1meta_1_1_meta_uav.html#a675a960083b0373730dab00e6d294565", null ],
    [ "setup_law_tab", "classframework_1_1meta_1_1_meta_uav.html#a16d5b672f199d385f6fbbfc301c83664", null ],
    [ "graph_law_tab", "classframework_1_1meta_1_1_meta_uav.html#a7826c749e6bf57de005981880e58bffa", null ]
];