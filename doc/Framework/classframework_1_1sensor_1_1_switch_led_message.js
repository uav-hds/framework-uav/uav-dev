var classframework_1_1sensor_1_1_switch_led_message =
[
    [ "SwitchLedMessage", "classframework_1_1sensor_1_1_switch_led_message.html#a1cbc793c4a561335357b4adc3183563d", null ],
    [ "IsOn", "classframework_1_1sensor_1_1_switch_led_message.html#ad53418c9ba7c8fe62cdcc65cfc7f5911", null ],
    [ "GetLedId", "classframework_1_1sensor_1_1_switch_led_message.html#a179bf8898613b221148d86dfbcdcc075", null ],
    [ "SetOn", "classframework_1_1sensor_1_1_switch_led_message.html#ae6dd84070931ded583848f45456358ea", null ],
    [ "SetOff", "classframework_1_1sensor_1_1_switch_led_message.html#a7acb9fcff49fef45f428c68aed75ed6e", null ],
    [ "SetLedId", "classframework_1_1sensor_1_1_switch_led_message.html#ad4953c596b6f55dc0a357bc72770fb20", null ]
];