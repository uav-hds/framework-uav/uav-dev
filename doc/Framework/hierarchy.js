var hierarchy =
[
    [ "framework::core::cvmatrix_descriptor", "classframework_1_1core_1_1cvmatrix__descriptor.html", null ],
    [ "framework::core::DataType", "classframework_1_1core_1_1_data_type.html", [
      [ "framework::core::AhrsData::Type", "classframework_1_1core_1_1_ahrs_data_1_1_type.html", null ],
      [ "framework::core::cvimage::Type", "classframework_1_1core_1_1cvimage_1_1_type.html", null ],
      [ "framework::core::cvmatrix::Type", "classframework_1_1core_1_1cvmatrix_1_1_type.html", null ],
      [ "framework::core::DummyType", "classframework_1_1core_1_1_dummy_type.html", null ],
      [ "framework::core::GeoCoordinate::Type", "classframework_1_1core_1_1_geo_coordinate_1_1_type.html", null ],
      [ "framework::core::ImuData::Type", "classframework_1_1core_1_1_imu_data_1_1_type.html", null ],
      [ "framework::core::ScalarType", "classframework_1_1core_1_1_scalar_type.html", [
        [ "framework::core::FloatType", "classframework_1_1core_1_1_float_type.html", null ],
        [ "framework::core::SignedIntegerType", "classframework_1_1core_1_1_signed_integer_type.html", null ]
      ] ],
      [ "framework::filter::OpticalFlowData::Type", "classframework_1_1filter_1_1_optical_flow_data_1_1_type.html", null ]
    ] ],
    [ "framework::simulator::DiscreteTimeVariable< T, size >", "classframework_1_1simulator_1_1_discrete_time_variable.html", null ],
    [ "framework::simulator::DiscreteTimeVariable< framework::simulator::Model::simu_state, 3 >", "classframework_1_1simulator_1_1_discrete_time_variable.html", null ],
    [ "framework::core::Euler", "classframework_1_1core_1_1_euler.html", null ],
    [ "IMeshSceneNode", null, [
      [ "framework::simulator::MeshSceneNode", "classframework_1_1simulator_1_1_mesh_scene_node.html", null ]
    ] ],
    [ "ISceneNode", null, [
      [ "framework::simulator::Blade", "classframework_1_1simulator_1_1_blade.html", null ]
    ] ],
    [ "ISceneNodeAnimator", null, [
      [ "framework::sensor::SimuCameraGL", "classframework_1_1sensor_1_1_simu_camera_g_l.html", null ]
    ] ],
    [ "framework::gui::LayoutPosition", "classframework_1_1gui_1_1_layout_position.html", null ],
    [ "framework::core::Message", "classframework_1_1core_1_1_message.html", [
      [ "framework::sensor::FlashLedMessage", "classframework_1_1sensor_1_1_flash_led_message.html", null ],
      [ "framework::sensor::RumbleMessage", "classframework_1_1sensor_1_1_rumble_message.html", null ],
      [ "framework::sensor::SwitchLedMessage", "classframework_1_1sensor_1_1_switch_led_message.html", null ]
    ] ],
    [ "framework::core::Object", "classframework_1_1core_1_1_object.html", [
      [ "framework::core::ConnectedSocket", "classframework_1_1core_1_1_connected_socket.html", [
        [ "framework::core::TcpSocket", "classframework_1_1core_1_1_tcp_socket.html", null ],
        [ "framework::core::UdtSocket", "classframework_1_1core_1_1_udt_socket.html", null ]
      ] ],
      [ "framework::core::FrameworkManager", "classframework_1_1core_1_1_framework_manager.html", [
        [ "framework::simulator::Simulator", "classframework_1_1simulator_1_1_simulator.html", null ]
      ] ],
      [ "framework::core::IODataElement", "classframework_1_1core_1_1_i_o_data_element.html", null ],
      [ "framework::core::IODevice", "classframework_1_1core_1_1_i_o_device.html", [
        [ "framework::actuator::BlCtrlV2_x4_speed", "classframework_1_1actuator_1_1_bl_ctrl_v2__x4__speed.html", null ],
        [ "framework::actuator::Bldc", "classframework_1_1actuator_1_1_bldc.html", [
          [ "framework::actuator::AfroBldc", "classframework_1_1actuator_1_1_afro_bldc.html", null ],
          [ "framework::actuator::BlCtrlV2", "classframework_1_1actuator_1_1_bl_ctrl_v2.html", null ],
          [ "framework::actuator::ParrotBldc", "classframework_1_1actuator_1_1_parrot_bldc.html", null ],
          [ "framework::actuator::SimuBldc", "classframework_1_1actuator_1_1_simu_bldc.html", null ],
          [ "framework::actuator::XBldc", "classframework_1_1actuator_1_1_x_bldc.html", null ]
        ] ],
        [ "framework::filter::Ahrs", "classframework_1_1filter_1_1_ahrs.html", [
          [ "framework::filter::AhrsComplementaryFilter", "classframework_1_1filter_1_1_ahrs_complementary_filter.html", null ],
          [ "framework::filter::AhrsKalman", "classframework_1_1filter_1_1_ahrs_kalman.html", null ],
          [ "framework::filter::Gx3_25_ahrs", "classframework_1_1filter_1_1_gx3__25__ahrs.html", null ],
          [ "framework::filter::SimuAhrs", "classframework_1_1filter_1_1_simu_ahrs.html", null ]
        ] ],
        [ "framework::filter::ButterworthLowPass", "classframework_1_1filter_1_1_butterworth_low_pass.html", null ],
        [ "framework::filter::ControlLaw", "classframework_1_1filter_1_1_control_law.html", [
          [ "framework::filter::NestedSat", "classframework_1_1filter_1_1_nested_sat.html", null ],
          [ "framework::filter::Pid", "classframework_1_1filter_1_1_pid.html", null ],
          [ "framework::filter::PidThrust", "classframework_1_1filter_1_1_pid_thrust.html", null ]
        ] ],
        [ "framework::filter::CvtColor", "classframework_1_1filter_1_1_cvt_color.html", null ],
        [ "framework::filter::EulerDerivative", "classframework_1_1filter_1_1_euler_derivative.html", null ],
        [ "framework::filter::HoughLines", "classframework_1_1filter_1_1_hough_lines.html", null ],
        [ "framework::filter::ImgThreshold", "classframework_1_1filter_1_1_img_threshold.html", null ],
        [ "framework::filter::JoyReference", "classframework_1_1filter_1_1_joy_reference.html", null ],
        [ "framework::filter::LowPassFilter", "classframework_1_1filter_1_1_low_pass_filter.html", null ],
        [ "framework::filter::OpticalFlow", "classframework_1_1filter_1_1_optical_flow.html", null ],
        [ "framework::filter::OpticalFlowSpeed", "classframework_1_1filter_1_1_optical_flow_speed.html", null ],
        [ "framework::filter::Sobel", "classframework_1_1filter_1_1_sobel.html", null ],
        [ "framework::filter::TrajectoryGenerator1D", "classframework_1_1filter_1_1_trajectory_generator1_d.html", null ],
        [ "framework::filter::TrajectoryGenerator2DCircle", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html", null ],
        [ "framework::filter::UavMultiplex", "classframework_1_1filter_1_1_uav_multiplex.html", [
          [ "framework::filter::X4X8Multiplex", "classframework_1_1filter_1_1_x4_x8_multiplex.html", null ]
        ] ],
        [ "framework::sensor::Camera", "classframework_1_1sensor_1_1_camera.html", [
          [ "framework::sensor::SimuCamera", "classframework_1_1sensor_1_1_simu_camera.html", [
            [ "framework::sensor::SimuCameraGL", "classframework_1_1sensor_1_1_simu_camera_g_l.html", null ]
          ] ],
          [ "framework::sensor::V4LCamera", "classframework_1_1sensor_1_1_v4_l_camera.html", [
            [ "framework::sensor::ParrotCamH", "classframework_1_1sensor_1_1_parrot_cam_h.html", null ],
            [ "framework::sensor::ParrotCamV", "classframework_1_1sensor_1_1_parrot_cam_v.html", null ],
            [ "framework::sensor::Ps3Eye", "classframework_1_1sensor_1_1_ps3_eye.html", null ]
          ] ]
        ] ],
        [ "framework::sensor::Gps", "classframework_1_1sensor_1_1_gps.html", [
          [ "framework::sensor::Mb800", "classframework_1_1sensor_1_1_mb800.html", null ],
          [ "framework::sensor::Novatel", "classframework_1_1sensor_1_1_novatel.html", null ],
          [ "framework::sensor::ParrotGps", "classframework_1_1sensor_1_1_parrot_gps.html", null ],
          [ "framework::sensor::SimuGps", "classframework_1_1sensor_1_1_simu_gps.html", null ]
        ] ],
        [ "framework::sensor::HostEthController", "classframework_1_1sensor_1_1_host_eth_controller.html", null ],
        [ "framework::sensor::Imu", "classframework_1_1sensor_1_1_imu.html", [
          [ "framework::sensor::Gx3_25_imu", "classframework_1_1sensor_1_1_gx3__25__imu.html", null ],
          [ "framework::sensor::SimuImu", "classframework_1_1sensor_1_1_simu_imu.html", null ]
        ] ],
        [ "framework::sensor::LaserRangeFinder", "classframework_1_1sensor_1_1_laser_range_finder.html", [
          [ "framework::sensor::HokuyoUTM30Lx", "classframework_1_1sensor_1_1_hokuyo_u_t_m30_lx.html", null ],
          [ "framework::sensor::SimuLaser", "classframework_1_1sensor_1_1_simu_laser.html", [
            [ "framework::sensor::SimuLaserGL", "classframework_1_1sensor_1_1_simu_laser_g_l.html", null ]
          ] ]
        ] ],
        [ "framework::sensor::RadioReceiver", "classframework_1_1sensor_1_1_radio_receiver.html", null ],
        [ "framework::sensor::TargetController", "classframework_1_1sensor_1_1_target_controller.html", [
          [ "framework::sensor::TargetEthController", "classframework_1_1sensor_1_1_target_eth_controller.html", [
            [ "framework::meta::MetaDualShock3", "classframework_1_1meta_1_1_meta_dual_shock3.html", null ]
          ] ]
        ] ],
        [ "framework::sensor::UsRangeFinder", "classframework_1_1sensor_1_1_us_range_finder.html", [
          [ "framework::sensor::SimuUs", "classframework_1_1sensor_1_1_simu_us.html", [
            [ "framework::sensor::SimuUsGL", "classframework_1_1sensor_1_1_simu_us_g_l.html", null ]
          ] ],
          [ "framework::sensor::Srf08", "classframework_1_1sensor_1_1_srf08.html", null ]
        ] ],
        [ "framework::sensor::VrpnObject", "classframework_1_1sensor_1_1_vrpn_object.html", [
          [ "framework::meta::MetaVrpnObject", "classframework_1_1meta_1_1_meta_vrpn_object.html", null ]
        ] ],
        [ "framework::simulator::Model", "classframework_1_1simulator_1_1_model.html", [
          [ "framework::simulator::Man", "classframework_1_1simulator_1_1_man.html", null ],
          [ "framework::simulator::X4", "classframework_1_1simulator_1_1_x4.html", null ],
          [ "framework::simulator::X8", "classframework_1_1simulator_1_1_x8.html", null ]
        ] ]
      ] ],
      [ "framework::core::Mutex", "classframework_1_1core_1_1_mutex.html", [
        [ "framework::core::ConditionVariable", "classframework_1_1core_1_1_condition_variable.html", null ],
        [ "framework::core::I2cPort", "classframework_1_1core_1_1_i2c_port.html", [
          [ "framework::core::RTDM_I2cPort", "classframework_1_1core_1_1_r_t_d_m___i2c_port.html", null ],
          [ "framework::core::Unix_I2cPort", "classframework_1_1core_1_1_unix___i2c_port.html", null ]
        ] ],
        [ "framework::core::io_data", "classframework_1_1core_1_1io__data.html", [
          [ "framework::core::AhrsData", "classframework_1_1core_1_1_ahrs_data.html", null ],
          [ "framework::core::cvimage", "classframework_1_1core_1_1cvimage.html", null ],
          [ "framework::core::cvmatrix", "classframework_1_1core_1_1cvmatrix.html", null ],
          [ "framework::core::GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html", null ],
          [ "framework::core::ImuData", "classframework_1_1core_1_1_imu_data.html", null ],
          [ "framework::core::Vector3Ddata", "classframework_1_1core_1_1_vector3_ddata.html", null ],
          [ "framework::filter::OpticalFlowData", "classframework_1_1filter_1_1_optical_flow_data.html", null ]
        ] ]
      ] ],
      [ "framework::core::SerialPort", "classframework_1_1core_1_1_serial_port.html", [
        [ "framework::core::RTDM_SerialPort", "classframework_1_1core_1_1_r_t_d_m___serial_port.html", null ],
        [ "framework::core::Unix_SerialPort", "classframework_1_1core_1_1_unix___serial_port.html", null ]
      ] ],
      [ "framework::core::SharedMem", "classframework_1_1core_1_1_shared_mem.html", null ],
      [ "framework::core::Socket", "classframework_1_1core_1_1_socket.html", null ],
      [ "framework::core::Thread", "classframework_1_1core_1_1_thread.html", [
        [ "framework::actuator::BlCtrlV2_x4_speed", "classframework_1_1actuator_1_1_bl_ctrl_v2__x4__speed.html", null ],
        [ "framework::core::Watchdog", "classframework_1_1core_1_1_watchdog.html", null ],
        [ "framework::meta::UavStateMachine", "classframework_1_1meta_1_1_uav_state_machine.html", null ],
        [ "framework::sensor::Gx3_25_imu", "classframework_1_1sensor_1_1_gx3__25__imu.html", null ],
        [ "framework::sensor::HokuyoUTM30Lx", "classframework_1_1sensor_1_1_hokuyo_u_t_m30_lx.html", null ],
        [ "framework::sensor::HostEthController", "classframework_1_1sensor_1_1_host_eth_controller.html", null ],
        [ "framework::sensor::Mb800", "classframework_1_1sensor_1_1_mb800.html", null ],
        [ "framework::sensor::Novatel", "classframework_1_1sensor_1_1_novatel.html", null ],
        [ "framework::sensor::ParrotBatteryMonitor", "classframework_1_1sensor_1_1_parrot_battery_monitor.html", null ],
        [ "framework::sensor::ParrotGps", "classframework_1_1sensor_1_1_parrot_gps.html", null ],
        [ "framework::sensor::ParrotNavBoard", "classframework_1_1sensor_1_1_parrot_nav_board.html", null ],
        [ "framework::sensor::SimuCamera", "classframework_1_1sensor_1_1_simu_camera.html", null ],
        [ "framework::sensor::SimuGps", "classframework_1_1sensor_1_1_simu_gps.html", null ],
        [ "framework::sensor::SimuImu", "classframework_1_1sensor_1_1_simu_imu.html", null ],
        [ "framework::sensor::SimuLaser", "classframework_1_1sensor_1_1_simu_laser.html", null ],
        [ "framework::sensor::SimuUs", "classframework_1_1sensor_1_1_simu_us.html", null ],
        [ "framework::sensor::Srf08", "classframework_1_1sensor_1_1_srf08.html", null ],
        [ "framework::sensor::TargetController", "classframework_1_1sensor_1_1_target_controller.html", null ],
        [ "framework::sensor::V4LCamera", "classframework_1_1sensor_1_1_v4_l_camera.html", null ],
        [ "framework::sensor::VrpnClient", "classframework_1_1sensor_1_1_vrpn_client.html", null ]
      ] ],
      [ "framework::gui::Widget", "classframework_1_1gui_1_1_widget.html", [
        [ "framework::gui::Box", "classframework_1_1gui_1_1_box.html", [
          [ "framework::gui::CheckBox", "classframework_1_1gui_1_1_check_box.html", null ],
          [ "framework::gui::ComboBox", "classframework_1_1gui_1_1_combo_box.html", null ],
          [ "framework::gui::DoubleSpinBox", "classframework_1_1gui_1_1_double_spin_box.html", null ],
          [ "framework::gui::SpinBox", "classframework_1_1gui_1_1_spin_box.html", null ],
          [ "framework::gui::Vector3DSpinBox", "classframework_1_1gui_1_1_vector3_d_spin_box.html", null ]
        ] ],
        [ "framework::gui::Label", "classframework_1_1gui_1_1_label.html", null ],
        [ "framework::gui::Layout", "classframework_1_1gui_1_1_layout.html", [
          [ "framework::gui::GridLayout", "classframework_1_1gui_1_1_grid_layout.html", null ],
          [ "framework::gui::GroupBox", "classframework_1_1gui_1_1_group_box.html", [
            [ "framework::core::OneAxisRotation", "classframework_1_1core_1_1_one_axis_rotation.html", null ],
            [ "framework::sensor::BatteryMonitor", "classframework_1_1sensor_1_1_battery_monitor.html", [
              [ "framework::sensor::ParrotBatteryMonitor", "classframework_1_1sensor_1_1_parrot_battery_monitor.html", null ]
            ] ]
          ] ],
          [ "framework::gui::Tab", "classframework_1_1gui_1_1_tab.html", null ]
        ] ],
        [ "framework::gui::PushButton", "classframework_1_1gui_1_1_push_button.html", null ],
        [ "framework::gui::SendData", "classframework_1_1gui_1_1_send_data.html", [
          [ "framework::gui::DataPlot", "classframework_1_1gui_1_1_data_plot.html", [
            [ "framework::gui::DataPlot1D", "classframework_1_1gui_1_1_data_plot1_d.html", null ],
            [ "framework::gui::DataPlot2D", "classframework_1_1gui_1_1_data_plot2_d.html", null ]
          ] ],
          [ "framework::gui::Map", "classframework_1_1gui_1_1_map.html", null ],
          [ "framework::gui::Picture", "classframework_1_1gui_1_1_picture.html", null ],
          [ "framework::gui::RangeFinderPlot", "classframework_1_1gui_1_1_range_finder_plot.html", null ]
        ] ],
        [ "framework::gui::TabWidget", "classframework_1_1gui_1_1_tab_widget.html", null ],
        [ "framework::gui::TextEdit", "classframework_1_1gui_1_1_text_edit.html", null ]
      ] ],
      [ "framework::meta::MetaUsRangeFinder", "classframework_1_1meta_1_1_meta_us_range_finder.html", null ],
      [ "framework::meta::Uav", "classframework_1_1meta_1_1_uav.html", [
        [ "framework::meta::ArDrone2", "classframework_1_1meta_1_1_ar_drone2.html", null ],
        [ "framework::meta::HdsX8", "classframework_1_1meta_1_1_hds_x8.html", null ],
        [ "framework::meta::SimuX4", "classframework_1_1meta_1_1_simu_x4.html", null ],
        [ "framework::meta::SimuX8", "classframework_1_1meta_1_1_simu_x8.html", null ],
        [ "framework::meta::XAir", "classframework_1_1meta_1_1_x_air.html", null ]
      ] ],
      [ "framework::simulator::Gui", "classframework_1_1simulator_1_1_gui.html", [
        [ "framework::simulator::Castle", "classframework_1_1simulator_1_1_castle.html", null ],
        [ "framework::simulator::Parser", "classframework_1_1simulator_1_1_parser.html", null ]
      ] ]
    ] ],
    [ "framework::core::Quaternion", "classframework_1_1core_1_1_quaternion.html", null ],
    [ "regval_list", "structregval__list.html", null ],
    [ "framework::core::RotationMatrix", "classframework_1_1core_1_1_rotation_matrix.html", null ],
    [ "framework::sensor::SensorGL", "classframework_1_1sensor_1_1_sensor_g_l.html", [
      [ "framework::sensor::SimuCameraGL", "classframework_1_1sensor_1_1_simu_camera_g_l.html", null ],
      [ "framework::sensor::SimuLaserGL", "classframework_1_1sensor_1_1_simu_laser_g_l.html", null ],
      [ "framework::sensor::SimuUsGL", "classframework_1_1sensor_1_1_simu_us_g_l.html", null ]
    ] ],
    [ "framework::simulator::Model::simu_state", "structframework_1_1simulator_1_1_model_1_1simu__state.html", null ],
    [ "framework::core::Vector2D", "classframework_1_1core_1_1_vector2_d.html", null ],
    [ "framework::core::Vector3D", "classframework_1_1core_1_1_vector3_d.html", [
      [ "framework::core::Vector3Ddata", "classframework_1_1core_1_1_vector3_ddata.html", null ]
    ] ]
];