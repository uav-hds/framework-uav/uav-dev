var classframework_1_1sensor_1_1_v4_l_camera =
[
    [ "V4LCamera", "classframework_1_1sensor_1_1_v4_l_camera.html#af0651757a66042aea0d80aaf619d06a2", null ],
    [ "~V4LCamera", "classframework_1_1sensor_1_1_v4_l_camera.html#a43e5f0f48acf2e8da0350a8ac4d44613", null ],
    [ "SetGain", "classframework_1_1sensor_1_1_v4_l_camera.html#adda281260c714f284a343a29553dd47e", null ],
    [ "SetAutoGain", "classframework_1_1sensor_1_1_v4_l_camera.html#a9096f79d510d19280ac9c2846fda9758", null ],
    [ "SetExposure", "classframework_1_1sensor_1_1_v4_l_camera.html#a1d5b0a8913733b805bcfa1cfbfd14d0a", null ],
    [ "SetAutoExposure", "classframework_1_1sensor_1_1_v4_l_camera.html#a54d5ca384d96884bb4a3bd78e4fb3fbb", null ],
    [ "SetBrightness", "classframework_1_1sensor_1_1_v4_l_camera.html#a2b572f107602b006e12ec561d4fc0dac", null ],
    [ "SetSaturation", "classframework_1_1sensor_1_1_v4_l_camera.html#a02ab8b4408c9c3639c04ba9507db329b", null ],
    [ "SetHue", "classframework_1_1sensor_1_1_v4_l_camera.html#a36a9fef11322d359951f6e6b6034c95b", null ],
    [ "SetContrast", "classframework_1_1sensor_1_1_v4_l_camera.html#a5b9e29dff0ec8e092a87533fb0d3c5b8", null ]
];