var class_send_data =
[
    [ "SendData", "class_send_data.html#adffd60d334dcf48b75cc66135777a0d9", null ],
    [ "~SendData", "class_send_data.html#aed87e72a8a4b9f3ab7423c97cbddc509", null ],
    [ "SendSize", "class_send_data.html#a9151fbd66b0c68d1e26dfc53a8938981", null ],
    [ "SendPeriod", "class_send_data.html#a14624568217a86241fd98d7811f6bdee", null ],
    [ "IsEnabled", "class_send_data.html#a5511ade44e7f575cb44a235ac3779104", null ],
    [ "SetEnabled", "class_send_data.html#a814790a667fa1a42b80e6e6f42bc5e96", null ],
    [ "SetSendSize", "class_send_data.html#afb927c339657362260ed8fd8f93822ff", null ],
    [ "SetSendPeriod", "class_send_data.html#a58a9e2a2aa8023800f05e2e0c40b3e30", null ],
    [ "ui_com", "class_send_data.html#a360cda69747acd51ff2acc89b2e5410f", null ]
];