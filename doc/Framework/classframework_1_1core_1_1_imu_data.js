var classframework_1_1core_1_1_imu_data =
[
    [ "Type", "classframework_1_1core_1_1_imu_data_1_1_type.html", "classframework_1_1core_1_1_imu_data_1_1_type" ],
    [ "PlotableData_t", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8", [
      [ "RawAx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a4d5057af07548b6b34e160ef48004cf0", null ],
      [ "RawAy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8adf083b1df8e937957c759ad93ad3028e", null ],
      [ "RawAz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a4f359d1e7d7b978325f85166e529286c", null ],
      [ "RawGx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a16c71ada258e96a3f0880fc2e431b9c5", null ],
      [ "RawGy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a596ddf0a2379801d40a5251e341c92d3", null ],
      [ "RawGz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8ab0dfaab3b5d46ed0b4ead0735a1fa602", null ],
      [ "RawGxDeg", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a5557edd930fd35142c43bcdc0321f572", null ],
      [ "RawGyDeg", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a16ce12331cc350477a10b1d7866fecc8", null ],
      [ "RawGzDeg", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a36eccd43da83d79c8b6c3563275ec580", null ],
      [ "RawMx", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8af223557b356e0d40a37592f143f80a37", null ],
      [ "RawMy", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a61646a6c036574ae54037126ee5b0805", null ],
      [ "RawMz", "classframework_1_1core_1_1_imu_data.html#a68dd279898f5c11b57c6169b0168e2c8a7576067451da5dfc71410871b9feca6c", null ]
    ] ],
    [ "ImuData", "classframework_1_1core_1_1_imu_data.html#ad1f5ef8390730dbe868f25fba9248cd9", null ],
    [ "~ImuData", "classframework_1_1core_1_1_imu_data.html#ae8ec0cd1606d05b577c36f2203bf9421", null ],
    [ "Element", "classframework_1_1core_1_1_imu_data.html#a5f98f9e7793b6b7fe45e87855f641756", null ],
    [ "GetRawAcc", "classframework_1_1core_1_1_imu_data.html#a11e0b74e5f56a562577a7e3a51487bc2", null ],
    [ "GetRawMag", "classframework_1_1core_1_1_imu_data.html#a8559a0720a5ea626721af362d033d529", null ],
    [ "GetRawGyr", "classframework_1_1core_1_1_imu_data.html#a019b3cf7520f38fe0c9c53528f27c369", null ],
    [ "GetRawAccMagAndGyr", "classframework_1_1core_1_1_imu_data.html#a4f90b31ee61c5927c1050d952e9ff1c3", null ],
    [ "SetRawAcc", "classframework_1_1core_1_1_imu_data.html#ab9741cbe12d85950663376dc226ec26c", null ],
    [ "SetRawMag", "classframework_1_1core_1_1_imu_data.html#a302ec9d26ea8bfc454a4f81e345d3761", null ],
    [ "SetRawGyr", "classframework_1_1core_1_1_imu_data.html#a214b2b4bf48e50cb94bb971cc35e0802", null ],
    [ "SetRawAccMagAndGyr", "classframework_1_1core_1_1_imu_data.html#a17036abb2356046d5e12f427ac0549d3", null ],
    [ "GetDataType", "classframework_1_1core_1_1_imu_data.html#a89798a73e1f373bfc244f4e66b8aae19", null ]
];