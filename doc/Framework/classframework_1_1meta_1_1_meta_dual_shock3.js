var classframework_1_1meta_1_1_meta_dual_shock3 =
[
    [ "MetaDualShock3", "classframework_1_1meta_1_1_meta_dual_shock3.html#af54209ab17382941d68ce4344d97b339", null ],
    [ "~MetaDualShock3", "classframework_1_1meta_1_1_meta_dual_shock3.html#add44cff8d356182cc615726bde4ce09e", null ],
    [ "GetReferenceOrientation", "classframework_1_1meta_1_1_meta_dual_shock3.html#a0ea02ac42643fb4338fade16f8f86e41", null ],
    [ "ZRef", "classframework_1_1meta_1_1_meta_dual_shock3.html#a3d503b35cbe5be2bd070627a9b32f132", null ],
    [ "DzRef", "classframework_1_1meta_1_1_meta_dual_shock3.html#aedad8657e29d48badb6dbc6fba059ec1", null ],
    [ "SetYawRef", "classframework_1_1meta_1_1_meta_dual_shock3.html#a6fb01c19d9a313e55fe3da58eeae4c5a", null ],
    [ "SetYawRef", "classframework_1_1meta_1_1_meta_dual_shock3.html#addeb93ccdd6364a12218e6fdeb472425", null ],
    [ "SetZRef", "classframework_1_1meta_1_1_meta_dual_shock3.html#ac3b229a274963d94c6043235a60e2b7c", null ],
    [ "RollTrim", "classframework_1_1meta_1_1_meta_dual_shock3.html#a2c260aa64908fb580b20656fcc7a4b42", null ],
    [ "PitchTrim", "classframework_1_1meta_1_1_meta_dual_shock3.html#a6f6a7e8e58f5eefb31821ec036ce4c27", null ],
    [ "ErrorNotify", "classframework_1_1meta_1_1_meta_dual_shock3.html#af4670b84dcc96f9944a3fcc2a2aff71e", null ],
    [ "Rumble", "classframework_1_1meta_1_1_meta_dual_shock3.html#a975bbb82f2786491cec6de6fe707689d", null ],
    [ "SetLedON", "classframework_1_1meta_1_1_meta_dual_shock3.html#a2dfaccb9b92cb7f7502ea402c0f95f91", null ],
    [ "SetLedOFF", "classframework_1_1meta_1_1_meta_dual_shock3.html#a05545c9c200aea24b409b63bd8b78559", null ],
    [ "FlashLed", "classframework_1_1meta_1_1_meta_dual_shock3.html#aeac2eb76671fdaadb959a256578b410b", null ],
    [ "::MetaDualShock3_impl", "classframework_1_1meta_1_1_meta_dual_shock3.html#a7d63c83eb8829a258041bf1946202380", null ]
];