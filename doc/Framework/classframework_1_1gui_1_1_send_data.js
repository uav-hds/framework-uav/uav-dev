var classframework_1_1gui_1_1_send_data =
[
    [ "SendData", "classframework_1_1gui_1_1_send_data.html#aa5982c3e0fc611405a7dee2a7b2f553c", null ],
    [ "~SendData", "classframework_1_1gui_1_1_send_data.html#a4ad3437118d864ef8118f4a2b6cbb092", null ],
    [ "CopyDatas", "classframework_1_1gui_1_1_send_data.html#a9995c7d9fa7f3fca5122f4dd1b5240ba", null ],
    [ "SendSize", "classframework_1_1gui_1_1_send_data.html#ad61c6d3c28a73c885c6abe582c1a0bf3", null ],
    [ "SendPeriod", "classframework_1_1gui_1_1_send_data.html#a002d3a6bde703721e40846f7672066d4", null ],
    [ "IsEnabled", "classframework_1_1gui_1_1_send_data.html#ac692e312d8a1131339925ecb14e38329", null ],
    [ "SetSendSize", "classframework_1_1gui_1_1_send_data.html#acc01e037a02e1ccd532fb41ad6805edc", null ],
    [ "ExtraXmlEvent", "classframework_1_1gui_1_1_send_data.html#a08a690a0cc248798993bd8b82e81217d", null ]
];