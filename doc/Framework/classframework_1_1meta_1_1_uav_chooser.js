var classframework_1_1meta_1_1_uav_chooser =
[
    [ "VRPNType_t", "classframework_1_1meta_1_1_uav_chooser.html#a55037f5f4dcfcc0fbb9e5757bc09bc0a", [
      [ "None", "classframework_1_1meta_1_1_uav_chooser.html#a55037f5f4dcfcc0fbb9e5757bc09bc0aa770a93459b613f2f178050d38a224021", null ],
      [ "Auto", "classframework_1_1meta_1_1_uav_chooser.html#a55037f5f4dcfcc0fbb9e5757bc09bc0aaec3bb3580ce070b89ada437d571923c8", null ],
      [ "AutoIP", "classframework_1_1meta_1_1_uav_chooser.html#a55037f5f4dcfcc0fbb9e5757bc09bc0aabdeebf7a1f9cf5044f63064e98272f62", null ],
      [ "UserDefinedIP", "classframework_1_1meta_1_1_uav_chooser.html#a55037f5f4dcfcc0fbb9e5757bc09bc0aa22e34d686526ef684242815a2564a479", null ],
      [ "AutoSerialPort", "classframework_1_1meta_1_1_uav_chooser.html#a55037f5f4dcfcc0fbb9e5757bc09bc0aa49e6601b351cddb2c515aa00f9f87b29", null ]
    ] ],
    [ "UavChooser", "classframework_1_1meta_1_1_uav_chooser.html#aa4335bcfa12396d39e69728692d9dd1a", null ],
    [ "~UavChooser", "classframework_1_1meta_1_1_uav_chooser.html#a53f3d296b477283bb1bf5bbed61e8570", null ],
    [ "StartSensors", "classframework_1_1meta_1_1_uav_chooser.html#a51d5efdab7d099ffef01ac220b4f3baa", null ],
    [ "UseDefaultPlot", "classframework_1_1meta_1_1_uav_chooser.html#a501c8ae949b8687d3b8cc6c08d3a027c", null ],
    [ "GetBldc", "classframework_1_1meta_1_1_uav_chooser.html#ae0495d392b00d58a0e809794d7dbca8e", null ],
    [ "GetUavMultiplex", "classframework_1_1meta_1_1_uav_chooser.html#af7bbd4403a7f297cd1bfa6fa51af18b6", null ],
    [ "GetImu", "classframework_1_1meta_1_1_uav_chooser.html#acc56f5052a650bddd574c047d9000b15", null ],
    [ "GetAhrs", "classframework_1_1meta_1_1_uav_chooser.html#ae4bc33ff3660d20ee3295c9bb9ae0d64", null ],
    [ "GetMetaUsRangeFinder", "classframework_1_1meta_1_1_uav_chooser.html#a706e40d56f1d43e874f5718e2f06c55f", null ],
    [ "GetBatteryMonitor", "classframework_1_1meta_1_1_uav_chooser.html#a7ecbb55305b14070aeb93bbf54a04237", null ],
    [ "GetVrpnClient", "classframework_1_1meta_1_1_uav_chooser.html#ac2c3f9a84f368cda24872af0c45cff24", null ],
    [ "GetVrpnObject", "classframework_1_1meta_1_1_uav_chooser.html#a22e751ffb265e77e2836b7aa2d5203d7", null ],
    [ "verticalCamera", "classframework_1_1meta_1_1_uav_chooser.html#a7e06eec44cbd9f1722558d82f828b691", null ]
];