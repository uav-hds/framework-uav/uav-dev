var class_ahrs =
[
    [ "Ahrs", "class_ahrs.html#a1dd4499623552d17736ad8db79fc8a93", null ],
    [ "~Ahrs", "class_ahrs.html#a92745a9ab887a3af4214e2ffbfc35626", null ],
    [ "GetEuler", "class_ahrs.html#a276c8c1f18997f52f7718a4de86ab370", null ],
    [ "GetAngSpeed", "class_ahrs.html#a1a66a3737f45c8141fbd14c5d03160b4", null ],
    [ "Lock", "class_ahrs.html#af84cf92a3bc45745b3efc8a9989177ee", null ],
    [ "Unlock", "class_ahrs.html#aa2eab7ad5c8ad4a3a414785eeb9a0f3a", null ],
    [ "UseDefaultPlot", "class_ahrs.html#acef4baa7d22799b6ff09e6350643690a", null ],
    [ "PlotTab", "class_ahrs.html#a3fcfc6df03f6210a76c1ee3e91bfded9", null ],
    [ "RollPlot", "class_ahrs.html#ac565b546b67db744ad34dc1cdbb12916", null ],
    [ "PitchPlot", "class_ahrs.html#a1eaacd06656cd82bf744be636d26640e", null ],
    [ "YawPlot", "class_ahrs.html#a7546c7b8f7c5e969af3a409e1bb00a0a", null ],
    [ "WXPlot", "class_ahrs.html#abdb5c1ea49f5ed6325860882ae9f0422", null ],
    [ "WYPlot", "class_ahrs.html#a96a1202dfdc4bbb1bb304de0286c3972", null ],
    [ "WZPlot", "class_ahrs.html#ab7de4d0b200f3c23a7d60c9a6f910abf", null ],
    [ "imudata", "class_ahrs.html#a7976c48ce0f386af803208a27c1d7991", null ]
];