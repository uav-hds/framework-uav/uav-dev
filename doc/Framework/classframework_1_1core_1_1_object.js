var classframework_1_1core_1_1_object =
[
    [ "color_t", "classframework_1_1core_1_1_object.html#a7201b52b50f64586d1ea82cc1f6a2e6f", [
      [ "Auto", "classframework_1_1core_1_1_object.html#a7201b52b50f64586d1ea82cc1f6a2e6fa386dcb3e61b661df3b73eea89d423262", null ],
      [ "Red", "classframework_1_1core_1_1_object.html#a7201b52b50f64586d1ea82cc1f6a2e6fad56c14c9ae2b39bada717f104359da30", null ],
      [ "Green", "classframework_1_1core_1_1_object.html#a7201b52b50f64586d1ea82cc1f6a2e6fae82405d34db3703fab1160b8cc29a06f", null ],
      [ "Orange", "classframework_1_1core_1_1_object.html#a7201b52b50f64586d1ea82cc1f6a2e6faa3e2a2c4f010bef9031278b6bbabab9c", null ]
    ] ],
    [ "Object", "classframework_1_1core_1_1_object.html#a97617b01df8aedc07b2ba46ef4988994", null ],
    [ "~Object", "classframework_1_1core_1_1_object.html#ac934815b59eb1cf1a47bdb114fa86da0", null ],
    [ "ObjectName", "classframework_1_1core_1_1_object.html#ae19cf04d95b4cabb8c6064375b15337f", null ],
    [ "ObjectType", "classframework_1_1core_1_1_object.html#a49660b572cdbedc6a665d5d396ed00a1", null ],
    [ "Parent", "classframework_1_1core_1_1_object.html#a8dda2eca639eb56bb2c3f26de04f2678", null ],
    [ "TypeChilds", "classframework_1_1core_1_1_object.html#a7fb6234586cd406b3bea93aadf01ba16", null ],
    [ "Childs", "classframework_1_1core_1_1_object.html#aee8de0c31d51e165fed466a4c8bcf6c4", null ],
    [ "Information", "classframework_1_1core_1_1_object.html#aec1974099e78e731ba9c3dd1ed05de94", null ],
    [ "Warning", "classframework_1_1core_1_1_object.html#abb73af04b52a2ba67a2d2ce1a74330a2", null ],
    [ "Error", "classframework_1_1core_1_1_object.html#a6f48a765168b2fe4a629234e0b778559", null ],
    [ "ErrorOccured", "classframework_1_1core_1_1_object.html#a56a6221f2ce124d7c53b15a423a47b08", null ],
    [ "::Widget_impl", "classframework_1_1core_1_1_object.html#a2a6f0753cfcbf314ad7fa952bfe9bc67", null ]
];