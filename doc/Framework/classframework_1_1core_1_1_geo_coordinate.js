var classframework_1_1core_1_1_geo_coordinate =
[
    [ "Type", "classframework_1_1core_1_1_geo_coordinate_1_1_type.html", "classframework_1_1core_1_1_geo_coordinate_1_1_type" ],
    [ "GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html#a1ffa3e3d7935ae43a50cb2149bded4a2", null ],
    [ "GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html#ae5ec18c7cbc5235fa80dc1e96a4e72c7", null ],
    [ "~GeoCoordinate", "classframework_1_1core_1_1_geo_coordinate.html#ad5c8bedb43f17d2b05be9bd4d8b3367a", null ],
    [ "CopyFrom", "classframework_1_1core_1_1_geo_coordinate.html#ad5d522e9bdb3e197c808e8d157c4d703", null ],
    [ "SetCoordinates", "classframework_1_1core_1_1_geo_coordinate.html#a7b72cda95437b7e8e23fc26d710b13ec", null ],
    [ "GetCoordinates", "classframework_1_1core_1_1_geo_coordinate.html#a0df0267cc919968ecd9ba2025f1234a6", null ],
    [ "GetDataType", "classframework_1_1core_1_1_geo_coordinate.html#a1f4485ae1521e3c3e8584333db6ef4b8", null ]
];