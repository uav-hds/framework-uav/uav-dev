var class_tab_widget =
[
    [ "TabPosition_t", "class_tab_widget.html#aceebdce502c8d7e5dadcd6b5d675d029", [
      [ "North", "class_tab_widget.html#aceebdce502c8d7e5dadcd6b5d675d029a972ed41f6be8b39f9b90fef3814c0444", null ],
      [ "South", "class_tab_widget.html#aceebdce502c8d7e5dadcd6b5d675d029ae97cd2ad5cae84f23327ebd4a46b9e38", null ],
      [ "West", "class_tab_widget.html#aceebdce502c8d7e5dadcd6b5d675d029a8864d447b94c0acb25d90a3769cccb0a", null ],
      [ "East", "class_tab_widget.html#aceebdce502c8d7e5dadcd6b5d675d029a7c6d290c2bee016ff0e2a8ae0dd67a9c", null ]
    ] ],
    [ "TabWidget", "class_tab_widget.html#a3fbd72450f7161ae357629c179b6c8c0", null ],
    [ "~TabWidget", "class_tab_widget.html#a520ed695e76930479c722ce0a3754180", null ]
];