var classframework_1_1sensor_1_1_joy_reference =
[
    [ "JoyReference", "classframework_1_1sensor_1_1_joy_reference.html#af7e7ad7389edb9c5aee043017161e946", null ],
    [ "~JoyReference", "classframework_1_1sensor_1_1_joy_reference.html#a568e6b551b56ef7dfefe73c80fc58f0e", null ],
    [ "SetRollAxis", "classframework_1_1sensor_1_1_joy_reference.html#a5c6a901d5e4fb9b04871020f4e9aaff6", null ],
    [ "SetPitchAxis", "classframework_1_1sensor_1_1_joy_reference.html#ad7ea452d0ffd8bb5852d68db3333c38a", null ],
    [ "SetYawAxis", "classframework_1_1sensor_1_1_joy_reference.html#a7465c06915d68feb443faff90dffb5fc", null ],
    [ "SetAltitudeAxis", "classframework_1_1sensor_1_1_joy_reference.html#a3f64478de309fdac809acb869e142b6d", null ],
    [ "QuaternionRef", "classframework_1_1sensor_1_1_joy_reference.html#a2e18dfe709d1f4ddea410eb5a1258b81", null ],
    [ "WzRef", "classframework_1_1sensor_1_1_joy_reference.html#aa6109ba59cc6da5a4a8e4b1a473eaf67", null ],
    [ "ZRef", "classframework_1_1sensor_1_1_joy_reference.html#a12e5ab81958567ca3b573dc556c468ef", null ],
    [ "DzRef", "classframework_1_1sensor_1_1_joy_reference.html#ac1c845dcaffafa00ca26bf68f5159716", null ],
    [ "RollTrim", "classframework_1_1sensor_1_1_joy_reference.html#aa8f84b283f942b0cf3bd06330f74b368", null ],
    [ "PitchTrim", "classframework_1_1sensor_1_1_joy_reference.html#a5a8d0fbb7d2d73bde828eb95da1cb6d4", null ],
    [ "SetYawRef", "classframework_1_1sensor_1_1_joy_reference.html#a50fecb8409dfe8d2887abec507eda8b8", null ],
    [ "SetYawRef", "classframework_1_1sensor_1_1_joy_reference.html#a15f682625ebab5d425a07679c9742711", null ],
    [ "SetZRef", "classframework_1_1sensor_1_1_joy_reference.html#ab0c475744dd24a148f5cf02900c51547", null ],
    [ "RollTrimUp", "classframework_1_1sensor_1_1_joy_reference.html#a178b15ed7b9948258acaeb3bfe943922", null ],
    [ "RollTrimDown", "classframework_1_1sensor_1_1_joy_reference.html#ae0738048547a2f069cdc02cfd9d2ed76", null ],
    [ "PitchTrimUp", "classframework_1_1sensor_1_1_joy_reference.html#a0f37b3dff0b30514141353f0c3067f6f", null ],
    [ "PitchTrimDown", "classframework_1_1sensor_1_1_joy_reference.html#a0ba2576ba656f40b2bc0a7cf7f0ecb97", null ],
    [ "State", "classframework_1_1sensor_1_1_joy_reference.html#a586a8834831499a48e57443f78ed918f", null ],
    [ "Update", "classframework_1_1sensor_1_1_joy_reference.html#acdcd107c81c7e8803ae2290f2c6877aa", null ]
];