var class_gx3__25__imu =
[
    [ "Command", "class_gx3__25__imu.html#a7ddb4a17994765c8972c86ae74ae95b5", [
      [ "EulerAnglesandAngularRates", "class_gx3__25__imu.html#a7ddb4a17994765c8972c86ae74ae95b5a89b2088edb3a2f55ee4c806f9471d37e", null ],
      [ "AccelerationAngularRateandOrientationMatrix", "class_gx3__25__imu.html#a7ddb4a17994765c8972c86ae74ae95b5ab3481172dde6ed4451a779b10dd250d4", null ]
    ] ],
    [ "Gx3_25_imu", "class_gx3__25__imu.html#adbf702b4305637cddf300d99351e2019", null ],
    [ "~Gx3_25_imu", "class_gx3__25__imu.html#ad9107dc330804417ca759cf476280393", null ],
    [ "Gx3_25_imu_impl", "class_gx3__25__imu.html#a580202aa58d4b1f3ac2ffa4f3c0a9f40", null ]
];