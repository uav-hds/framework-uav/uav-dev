var class_meta_dual_shock3 =
[
    [ "MetaDualShock3", "class_meta_dual_shock3.html#adfc2a677231c70aa53af1159e31e5159", null ],
    [ "~MetaDualShock3", "class_meta_dual_shock3.html#a5efa2d41bea9d14882a137101c27a529", null ],
    [ "ButtonsPressed", "class_meta_dual_shock3.html#aae7cf09d89e04c2de73db704efc24785", null ],
    [ "RollRef", "class_meta_dual_shock3.html#ab598bf9898b3b4b27371954d433096ad", null ],
    [ "PitchRef", "class_meta_dual_shock3.html#ace382588cd75e2cce3b9e3a1f679fa4f", null ],
    [ "YawRef", "class_meta_dual_shock3.html#a108f41f967957fc955b9db96c635b8c2", null ],
    [ "ZRef", "class_meta_dual_shock3.html#a502695a3e7fd42aab481406885434af6", null ],
    [ "dYawRef", "class_meta_dual_shock3.html#a200264b67f8f71b5f97505529855253f", null ],
    [ "dZRef", "class_meta_dual_shock3.html#a67853160334b78b6a94411f54691f991", null ],
    [ "SetYawRef", "class_meta_dual_shock3.html#aa15220d91e5d212d691994429127bfd1", null ],
    [ "SetZRef", "class_meta_dual_shock3.html#a4baee4880ea057a66a8538ed3c845139", null ],
    [ "RollTrim", "class_meta_dual_shock3.html#a9de2d7e5cdf9b6f6ede6ee51ba7fa3b0", null ],
    [ "PitchTrim", "class_meta_dual_shock3.html#a233664ac9b59fbd557bf332ba2183068", null ],
    [ "State", "class_meta_dual_shock3.html#a35f63d73cd3d6e71e5556ba2151a4c46", null ],
    [ "ErrorNotify", "class_meta_dual_shock3.html#ac89ba5ceeeb08c37367a39a59c607d66", null ],
    [ "Rumble", "class_meta_dual_shock3.html#aca5030b4d4e79b43153ac88c9ec9ba51", null ],
    [ "SetLedON", "class_meta_dual_shock3.html#a5a653549a133f754377cf363890bb41f", null ],
    [ "SetLedOFF", "class_meta_dual_shock3.html#ae12c4e66b58f680fbda2dcd8be1ee1d8", null ],
    [ "SetLed", "class_meta_dual_shock3.html#a5ac97a0fae4be33a2e14230127530d3a", null ],
    [ "MetaDualShock3_impl", "class_meta_dual_shock3.html#af9ff6e8526ea5bfd062511da2be59800", null ]
];