var classframework_1_1sensor_1_1_gx3__25__imu =
[
    [ "Command_t", "classframework_1_1sensor_1_1_gx3__25__imu.html#ae1676026bff5a449ddcc8ec1103c2a18", [
      [ "EulerAnglesAndAngularRates", "classframework_1_1sensor_1_1_gx3__25__imu.html#ae1676026bff5a449ddcc8ec1103c2a18add642e5051d2322191b22b1b440afe35", null ],
      [ "AccelerationAngularRateAndOrientationMatrix", "classframework_1_1sensor_1_1_gx3__25__imu.html#ae1676026bff5a449ddcc8ec1103c2a18a310144ae158aa4db8307d62c57610260", null ]
    ] ],
    [ "Gx3_25_imu", "classframework_1_1sensor_1_1_gx3__25__imu.html#a86bd9fcdebf6a8a390a90301f73f0102", null ],
    [ "~Gx3_25_imu", "classframework_1_1sensor_1_1_gx3__25__imu.html#a9b548c12768242c5ee58d0b5585f8b63", null ],
    [ "::Gx3_25_imu_impl", "classframework_1_1sensor_1_1_gx3__25__imu.html#aced28a91da3d8a16423107ad05ba2c10", null ]
];