var class_dual_shock3__io =
[
    [ "DualShock3_io", "class_dual_shock3__io.html#a9fff334f056c3ce74f62af9ea19d794a", null ],
    [ "~DualShock3_io", "class_dual_shock3__io.html#ad152b3148ea6c085908da5d44cef3d32", null ],
    [ "Roll", "class_dual_shock3__io.html#a52d88b3f75ab0843036a439a64fec743", null ],
    [ "Pitch", "class_dual_shock3__io.html#aaa586067861d07870d8d99f9049d734f", null ],
    [ "Yaw", "class_dual_shock3__io.html#ae8ad7d8460e6b004349a10f0871d665d", null ],
    [ "Gaz", "class_dual_shock3__io.html#ade774b294734b68153d969e5224d4686", null ],
    [ "ConnectionInit", "class_dual_shock3__io.html#a28766e016072a21a9557ab9de5bd47a3", null ],
    [ "Buttons", "class_dual_shock3__io.html#a8611dd5943eed07e949fe563d598f6d6", null ]
];