var group___filter =
[
    [ "Ahrs", "classframework_1_1filter_1_1_ahrs.html", [
      [ "Ahrs", "classframework_1_1filter_1_1_ahrs.html#a34f4ea6b1b8358afd468d424b18acae3", null ],
      [ "~Ahrs", "classframework_1_1filter_1_1_ahrs.html#a612a8b455de26e6b58f443aecf43e358", null ],
      [ "GetEuler", "classframework_1_1filter_1_1_ahrs.html#ae31c67d98ce3755c04a41ae015aa6a30", null ],
      [ "GetAngSpeed", "classframework_1_1filter_1_1_ahrs.html#ac6597b57b8fa256b99e228db66f2b696", null ],
      [ "Lock", "classframework_1_1filter_1_1_ahrs.html#a962e1a37ac4bab343405ad91dd12e1b3", null ],
      [ "Unlock", "classframework_1_1filter_1_1_ahrs.html#a50321650f42782b6f4b949f781b89ad2", null ],
      [ "UseDefaultPlot", "classframework_1_1filter_1_1_ahrs.html#abef8aab89f59d74586fddd1badbc28ec", null ],
      [ "PlotTab", "classframework_1_1filter_1_1_ahrs.html#a40e53701cedb15e7f83c231505b75adb", null ],
      [ "RollPlot", "classframework_1_1filter_1_1_ahrs.html#ae1b108716a979f36e09de14368d7c06d", null ],
      [ "PitchPlot", "classframework_1_1filter_1_1_ahrs.html#ace24e522049bdcc79bc7cbd65e2bdb1d", null ],
      [ "YawPlot", "classframework_1_1filter_1_1_ahrs.html#a4378003213e0bfdf8260553c615f34a3", null ],
      [ "WXPlot", "classframework_1_1filter_1_1_ahrs.html#a306dbb12d7230eb1fa78123fb23b9db1", null ],
      [ "WYPlot", "classframework_1_1filter_1_1_ahrs.html#a845fc35ab110c5aad43ea9570e2dd440", null ],
      [ "WZPlot", "classframework_1_1filter_1_1_ahrs.html#aab10ff3b2b2b93070d13f4eac738484f", null ],
      [ "imudata", "classframework_1_1filter_1_1_ahrs.html#ad9fec34317180c93ee1b3dcceb1535b4", null ]
    ] ],
    [ "AhrsKalman", "classframework_1_1filter_1_1_ahrs_kalman.html", [
      [ "AhrsKalman", "classframework_1_1filter_1_1_ahrs_kalman.html#ac6749f2250c09aba278fbb22df0c570d", null ],
      [ "~AhrsKalman", "classframework_1_1filter_1_1_ahrs_kalman.html#a498062fd14508e4757323a6bad5acdc0", null ]
    ] ],
    [ "ButterworthLowPass", "classframework_1_1filter_1_1_butterworth_low_pass.html", [
      [ "ButterworthLowPass", "classframework_1_1filter_1_1_butterworth_low_pass.html#af689d9f2c6875b1c11f58ff9cc837f98", null ],
      [ "~ButterworthLowPass", "classframework_1_1filter_1_1_butterworth_low_pass.html#a07e775576c8a7d8a781ea7db45d3d4b2", null ],
      [ "Output", "classframework_1_1filter_1_1_butterworth_low_pass.html#af4b8d37152887e09cec2de1e393400c8", null ],
      [ "Matrix", "classframework_1_1filter_1_1_butterworth_low_pass.html#a41a27aab252d7c91bc4e29d5195d6910", null ]
    ] ],
    [ "EulerDerivative", "classframework_1_1filter_1_1_euler_derivative.html", [
      [ "EulerDerivative", "classframework_1_1filter_1_1_euler_derivative.html#aa4ee46cdceeca247b6a11f061bef2305", null ],
      [ "~EulerDerivative", "classframework_1_1filter_1_1_euler_derivative.html#a0491586dc39293fc06a45d6ea9847a8d", null ],
      [ "Output", "classframework_1_1filter_1_1_euler_derivative.html#ae374c1adc1e8b7ecd4c16cb2dd85e7dd", null ],
      [ "Matrix", "classframework_1_1filter_1_1_euler_derivative.html#a9515323e80a70b7579a5b631a39dfc4e", null ]
    ] ],
    [ "LowPassFilter", "classframework_1_1filter_1_1_low_pass_filter.html", [
      [ "LowPassFilter", "classframework_1_1filter_1_1_low_pass_filter.html#ac041cbe4f8d2e1c57ab78d118edc4762", null ],
      [ "~LowPassFilter", "classframework_1_1filter_1_1_low_pass_filter.html#a8f441ec5993d2333932a348df78ff90c", null ],
      [ "Output", "classframework_1_1filter_1_1_low_pass_filter.html#a51c43ecb35efbd4e6ce2b06105b30167", null ],
      [ "Matrix", "classframework_1_1filter_1_1_low_pass_filter.html#a1f46c32a658a38de253da59de82b6c2f", null ]
    ] ],
    [ "NestedSat", "classframework_1_1filter_1_1_nested_sat.html", [
      [ "NestedSat", "classframework_1_1filter_1_1_nested_sat.html#a80cc4ee413948ddcfcf4fa665e88923d", null ],
      [ "~NestedSat", "classframework_1_1filter_1_1_nested_sat.html#a24e7d4cee27a325f0063c1b1ca3cf4f3", null ],
      [ "SetValues", "classframework_1_1filter_1_1_nested_sat.html#a9f919599c677e50584021cad466610e1", null ],
      [ "Value", "classframework_1_1filter_1_1_nested_sat.html#ace71b9b1ccf31164a899e4bae5c83f2e", null ],
      [ "UseDefaultPlot", "classframework_1_1filter_1_1_nested_sat.html#af3b5fa2e89a9549360549661711332e5", null ],
      [ "Update", "classframework_1_1filter_1_1_nested_sat.html#afd822227c32681365120c303a0fa7d90", null ],
      [ "ConvertSatFromDegToRad", "classframework_1_1filter_1_1_nested_sat.html#a4d8d13b3c4342af3c03b29a81f84a85c", null ]
    ] ],
    [ "OpticalFlow", "classframework_1_1filter_1_1_optical_flow.html", [
      [ "OpticalFlow", "classframework_1_1filter_1_1_optical_flow.html#a60bc2ad076540e11432978fbdb2e9d64", null ],
      [ "~OpticalFlow", "classframework_1_1filter_1_1_optical_flow.html#a0ee5eb8b3c08a644e455973e01c5c554", null ]
    ] ],
    [ "OpticalFlowData", "classframework_1_1filter_1_1_optical_flow_data.html", [
      [ "OpticalFlowData", "classframework_1_1filter_1_1_optical_flow_data.html#a5df181b433250edcae74ec8fd888922a", null ],
      [ "~OpticalFlowData", "classframework_1_1filter_1_1_optical_flow_data.html#af48c6b4c7054017216fb96a84a690d0e", null ],
      [ "PointsA", "classframework_1_1filter_1_1_optical_flow_data.html#a9e2cb433f0f6e9f5cb551ce65fad6f73", null ],
      [ "PointsB", "classframework_1_1filter_1_1_optical_flow_data.html#a49ab43635b5624e9cc1b66b9de0778e5", null ],
      [ "FoundFeature", "classframework_1_1filter_1_1_optical_flow_data.html#a61800b5399a185a5efe249bec8214b38", null ],
      [ "FeatureError", "classframework_1_1filter_1_1_optical_flow_data.html#ac2c5e6e49fd1a4af52083c5d2c9062fa", null ],
      [ "SetPointsA", "classframework_1_1filter_1_1_optical_flow_data.html#aecf88a6f74c3a766e03125ad43a66721", null ],
      [ "SetPointsB", "classframework_1_1filter_1_1_optical_flow_data.html#aab62bf77cd0a47f659d1b4410ea47532", null ],
      [ "SetFoundFeature", "classframework_1_1filter_1_1_optical_flow_data.html#ae4534550cf68da5a25b888e31c4399f4", null ],
      [ "SetFeatureError", "classframework_1_1filter_1_1_optical_flow_data.html#a719105ac3cd949386b6b21122c82e4dd", null ],
      [ "MaxFeatures", "classframework_1_1filter_1_1_optical_flow_data.html#a529090ec9b80e81c4e0b875ad4e5991f", null ],
      [ "NbFeatures", "classframework_1_1filter_1_1_optical_flow_data.html#a556cfdb27ba880b0e9f41ce1daae2cae", null ],
      [ "SetNbFeatures", "classframework_1_1filter_1_1_optical_flow_data.html#a93c91a23ba81d8b90681b14e489534fd", null ],
      [ "Resize", "classframework_1_1filter_1_1_optical_flow_data.html#aa223eabacaa758abde6f502bda70cd3f", null ]
    ] ],
    [ "OpticalFlowSpeed", "classframework_1_1filter_1_1_optical_flow_speed.html", [
      [ "OpticalFlowSpeed", "classframework_1_1filter_1_1_optical_flow_speed.html#a91df89ea8a01bf6be60b94b6d41a7e19", null ],
      [ "~OpticalFlowSpeed", "classframework_1_1filter_1_1_optical_flow_speed.html#a09a1073c45d1266d210532b156562e0c", null ],
      [ "Vx", "classframework_1_1filter_1_1_optical_flow_speed.html#a2de34047a0fcafee231f345263fa2974", null ],
      [ "Vy", "classframework_1_1filter_1_1_optical_flow_speed.html#a5d2c437eaf93547837405b0ecf194a88", null ],
      [ "output", "classframework_1_1filter_1_1_optical_flow_speed.html#a13a3800055a44c74a80b346b126fad71", null ]
    ] ],
    [ "Pid", "classframework_1_1filter_1_1_pid.html", [
      [ "Pid", "classframework_1_1filter_1_1_pid.html#a797f541a53abd5ec10f542e927d23028", null ],
      [ "~Pid", "classframework_1_1filter_1_1_pid.html#a3b896e42db3a07de931dd0e355d5d913", null ],
      [ "ResetI", "classframework_1_1filter_1_1_pid.html#a2499680a8cc1be6619f413f379581859", null ],
      [ "SetValues", "classframework_1_1filter_1_1_pid.html#adb2e92436d2ea3a1a29275c34602fa6b", null ],
      [ "Value", "classframework_1_1filter_1_1_pid.html#a77ed9b7e2e4bcd532bd599b3a26a31b1", null ],
      [ "UseDefaultPlot", "classframework_1_1filter_1_1_pid.html#aa66746b122cd410485c9010fd03ae4df", null ],
      [ "Update", "classframework_1_1filter_1_1_pid.html#a2dd2ff6e068e7ac58a475ec34115990b", null ]
    ] ],
    [ "TrajectoryGenerator1D", "classframework_1_1filter_1_1_trajectory_generator1_d.html", [
      [ "TrajectoryGenerator1D", "classframework_1_1filter_1_1_trajectory_generator1_d.html#afaaa3b83b3cdfaeba5645b799f7f7fcd", null ],
      [ "~TrajectoryGenerator1D", "classframework_1_1filter_1_1_trajectory_generator1_d.html#ab009678cc37138a00957bc6e3e82fea5", null ],
      [ "StartTraj", "classframework_1_1filter_1_1_trajectory_generator1_d.html#ae1b33a9354387bd33209f31003b8f1fc", null ],
      [ "StopTraj", "classframework_1_1filter_1_1_trajectory_generator1_d.html#ac9be2ce2b53e7ef5866f459284823512", null ],
      [ "SetPositionOffset", "classframework_1_1filter_1_1_trajectory_generator1_d.html#add9eb592c445f141fb5eb39d8a7fa1be", null ],
      [ "SetSpeedOffset", "classframework_1_1filter_1_1_trajectory_generator1_d.html#a96ff50cc44677262a585db2aa4c2d0af", null ],
      [ "Update", "classframework_1_1filter_1_1_trajectory_generator1_d.html#a28a92127ec1e56b5a1ab458ea71aded5", null ],
      [ "Position", "classframework_1_1filter_1_1_trajectory_generator1_d.html#afb2928a0f17c83d78898cbe90682a090", null ],
      [ "Speed", "classframework_1_1filter_1_1_trajectory_generator1_d.html#a4632318e5dab0bf972c4a9733f6c0bc7", null ],
      [ "Matrix", "classframework_1_1filter_1_1_trajectory_generator1_d.html#a65395e3154b0b178c31984b1b9718be0", null ]
    ] ],
    [ "TrajectoryGenerator2DCircle", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html", [
      [ "TrajectoryGenerator2DCircle", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a0de88d3cc8e762ef496858f7889eb969", null ],
      [ "~TrajectoryGenerator2DCircle", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a8d7b9503d7047f8194cc719aed6a16d0", null ],
      [ "StartTraj", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#acc50f61af53f4e9465c12e978b0d244d", null ],
      [ "StopTraj", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a8b82467d7e945375968382283436bd7a", null ],
      [ "FinishTraj", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a15ef6736db05be2db9643f4cca9e8a2b", null ],
      [ "SetCenter", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a2526c4f90651525f1a270bc2dec1d5ae", null ],
      [ "SetCenterSpeed", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a9c52f4ce1e23accf82b691b1a9027381", null ],
      [ "Update", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a6748d0e4f698aab1aa0f65f6af83d4d7", null ],
      [ "GetPosition", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a5d0bcabb6193f6d55e7a39a1e4334e88", null ],
      [ "GetSpeed", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#ad60fd428031dbe85a3ccbca109296592", null ],
      [ "Matrix", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#aea41006b16546aa9bf3a2bfe1dd9c189", null ],
      [ "IsRunning", "classframework_1_1filter_1_1_trajectory_generator2_d_circle.html#a586f1c0a418e76e087f9473f2ab80660", null ]
    ] ]
];