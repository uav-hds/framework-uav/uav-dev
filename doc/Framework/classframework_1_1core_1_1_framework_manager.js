var classframework_1_1core_1_1_framework_manager =
[
    [ "FrameworkManager", "classframework_1_1core_1_1_framework_manager.html#ada7db9f85fae36f27d6b6384971c09c5", null ],
    [ "~FrameworkManager", "classframework_1_1core_1_1_framework_manager.html#aa450d1de1dfd800a375793f516778dc0", null ],
    [ "SetupConnection", "classframework_1_1core_1_1_framework_manager.html#ad1da240f00558ad89896b213a9ac9fef", null ],
    [ "SetupUserInterface", "classframework_1_1core_1_1_framework_manager.html#ab50a61d52acbd0577e1929ffe56fe1ea", null ],
    [ "GetTabWidget", "classframework_1_1core_1_1_framework_manager.html#abcdd863f5fe4dc6f63e2f2c6452024d7", null ],
    [ "SetupLogger", "classframework_1_1core_1_1_framework_manager.html#a1193a30091d4ea54cdb9629ff05aac3e", null ],
    [ "AddDeviceToLog", "classframework_1_1core_1_1_framework_manager.html#a6056cf9ac936155c49613c672ade55ed", null ],
    [ "StartLog", "classframework_1_1core_1_1_framework_manager.html#ac9ace8e2d4239e880d40a0dbf0730cc1", null ],
    [ "StopLog", "classframework_1_1core_1_1_framework_manager.html#a20b5c34e89f2254822c630f231ac8547", null ],
    [ "IsLogging", "classframework_1_1core_1_1_framework_manager.html#a2877c46562eb2f44d45806f24883f463", null ],
    [ "SetupDSP", "classframework_1_1core_1_1_framework_manager.html#addb0842421d006e0768781c056ed05be", null ],
    [ "UpdateSendData", "classframework_1_1core_1_1_framework_manager.html#a5009e267fa360f05d26530375a05cd36", null ],
    [ "BlockCom", "classframework_1_1core_1_1_framework_manager.html#a2848deb4f447f069240886a053335de8", null ],
    [ "UnBlockCom", "classframework_1_1core_1_1_framework_manager.html#aca78bacec4bcddb9201df47abe1e14e9", null ],
    [ "ConnectionLost", "classframework_1_1core_1_1_framework_manager.html#ad2996266061e7f3062347ff71e605617", null ],
    [ "DisableErrorsDisplay", "classframework_1_1core_1_1_framework_manager.html#acd9d7283180122a3254f34e052591fcc", null ],
    [ "IsDisplayingErrors", "classframework_1_1core_1_1_framework_manager.html#a3d58cacaea8f578ecd99ccfa97978f80", null ]
];