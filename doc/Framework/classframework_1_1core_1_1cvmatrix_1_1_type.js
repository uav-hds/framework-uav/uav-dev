var classframework_1_1core_1_1cvmatrix_1_1_type =
[
    [ "Type", "classframework_1_1core_1_1cvmatrix_1_1_type.html#ab22b74e8e7049d5deb7b54f1f3d44b2e", null ],
    [ "GetSize", "classframework_1_1core_1_1cvmatrix_1_1_type.html#a554410f9f91c3cc2a0c92bb49210d786", null ],
    [ "GetDescription", "classframework_1_1core_1_1cvmatrix_1_1_type.html#ae56db5b1c41abc6253823416105f0573", null ],
    [ "GetNbRows", "classframework_1_1core_1_1cvmatrix_1_1_type.html#a3a96d996ea1dc905e4efb449e11c90cf", null ],
    [ "GetNbCols", "classframework_1_1core_1_1cvmatrix_1_1_type.html#a94b95f9bb8a0d929007a9bfc563abed4", null ],
    [ "GetElementDataType", "classframework_1_1core_1_1cvmatrix_1_1_type.html#af8a125997f650282b4268bb74d009d3f", null ]
];