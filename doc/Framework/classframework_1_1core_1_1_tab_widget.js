var classframework_1_1core_1_1_tab_widget =
[
    [ "TabPosition_t", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34", [
      [ "North", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34adad34152a2a6b2fd606f624083619457", null ],
      [ "South", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34a826d3aaf70da79bd7f4d9d96f9cacd91", null ],
      [ "West", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34abdd7368bae8016860670373b8ecce600", null ],
      [ "East", "classframework_1_1core_1_1_tab_widget.html#a4304e5e47178127354ea0dffff110c34abab58ad7e9819f71b0ef480ae173752c", null ]
    ] ],
    [ "TabWidget", "classframework_1_1core_1_1_tab_widget.html#a8e00a5f3b6f6666efb26b4cbd85def7a", null ],
    [ "~TabWidget", "classframework_1_1core_1_1_tab_widget.html#a4e0c903c8137436d989e72c1d9e69b71", null ]
];