var classcvmatrix =
[
    [ "cvmatrix", "classcvmatrix.html#ac9dea5bab7a8853d2dadef7d3b9ba5d1", null ],
    [ "cvmatrix", "classcvmatrix.html#aab591c846a9882e0fd7e552a32b289ba", null ],
    [ "~cvmatrix", "classcvmatrix.html#ac7480fc56cf815a767dfa988884f2622", null ],
    [ "Value", "classcvmatrix.html#a55a30639c0a2ebb3c2c226ff35f4db9e", null ],
    [ "ValueNoMutex", "classcvmatrix.html#a48909651e920accb68b5cb80e047e6f6", null ],
    [ "SetValue", "classcvmatrix.html#a040bfda9540670cc9a2144c013629006", null ],
    [ "SetValueNoMutex", "classcvmatrix.html#a37665e62719701041085f7ebeddff955", null ],
    [ "Name", "classcvmatrix.html#ab7ada1ffe138215dda647fce8e7433ed", null ],
    [ "Rows", "classcvmatrix.html#a408ae007d134e1dfd77c048d1b5bbfac", null ],
    [ "Cols", "classcvmatrix.html#af7538a1477b0c5964d82e4661661673a", null ],
    [ "DataType", "classcvmatrix.html#a36cfa09da7e8cf74cd89298b974b49ce", null ]
];