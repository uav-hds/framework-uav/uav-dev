var classframework_1_1core_1_1cvimage_1_1_type =
[
    [ "Format", "classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528e", [
      [ "YUYV", "classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528ea52c8796e733cb603dc16270c874e91d7", null ],
      [ "UYVY", "classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528ea87261c567c007f283e17a5fc8c259296", null ],
      [ "BGR", "classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528ea2ad5640ebdec72fc79531d1778c6c2dc", null ],
      [ "GRAY", "classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528ea48bf014c704c9eaae100a98006a37bf7", null ]
    ] ],
    [ "Type", "classframework_1_1core_1_1cvimage_1_1_type.html#a55cabc64afa4c9ac607cf6876a210e2f", null ],
    [ "GetSize", "classframework_1_1core_1_1cvimage_1_1_type.html#aa00a78ce319dd02b87affe26659715ed", null ],
    [ "GetDescription", "classframework_1_1core_1_1cvimage_1_1_type.html#a5eeec67f40f66e83f608655e9d30f519", null ],
    [ "GetFormat", "classframework_1_1core_1_1cvimage_1_1_type.html#a538379ed69deee068ddf9392dd0eb83d", null ],
    [ "GetWidth", "classframework_1_1core_1_1cvimage_1_1_type.html#a997b2dc3743aec0da08ee1aed8523ff3", null ],
    [ "GetHeight", "classframework_1_1core_1_1cvimage_1_1_type.html#ad8b4d3c92eecc71d88da1c1cd04e7060", null ]
];