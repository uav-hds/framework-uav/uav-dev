var class_framework_manager =
[
    [ "FrameworkManager", "class_framework_manager.html#abbd3d26a2a817a31da27811d0e3735b1", null ],
    [ "~FrameworkManager", "class_framework_manager.html#a6606120259c4c5df1d6420800e9b14b5", null ],
    [ "SetupLogger", "class_framework_manager.html#ae6c2737878f8195804220765953cc35b", null ],
    [ "AddDeviceToLog", "class_framework_manager.html#ad84a415607631a5541583dda01b2f899", null ],
    [ "StartLog", "class_framework_manager.html#ae9024d859a65fd776139cedabeb8c88e", null ],
    [ "StopLog", "class_framework_manager.html#a542d57bf3258461cc7701970cf61d2d2", null ],
    [ "IsLogging", "class_framework_manager.html#a92728ea490efed6a2281c70bd6f9bc5e", null ],
    [ "SetupDSP", "class_framework_manager.html#a6d9aceb4435b617d20c17f3ce659d425", null ],
    [ "AddSendData", "class_framework_manager.html#afefa6672f4b86a98cdb736c9afc03008", null ],
    [ "RemoveSendData", "class_framework_manager.html#a91d1bbac43f0a10b3674a5ab85f12c4d", null ],
    [ "UpdateSendData", "class_framework_manager.html#ae56888e796814c6178aade3a99582069", null ],
    [ "UpdateDataToSendSize", "class_framework_manager.html#a27dc7787fd3a3b0925f5b789dbfff217", null ],
    [ "BlockCom", "class_framework_manager.html#ad7bd2f452d5af3d4d66a5d6284fa497f", null ],
    [ "UnBlockCom", "class_framework_manager.html#aa5aece9b6a70058a52b6911d6d36c44b", null ],
    [ "ConnectionLost", "class_framework_manager.html#aa54e0dfd1e8dcd8f470ab39de90ad355", null ],
    [ "Widget_impl", "class_framework_manager.html#aea54e136a2f5a2d023439ac4f3ef1593", null ],
    [ "IODevice_impl", "class_framework_manager.html#ae325d2ec7ae2b04bd53703c4e4dc0364", null ]
];