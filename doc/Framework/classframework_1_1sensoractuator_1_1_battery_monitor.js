var classframework_1_1sensoractuator_1_1_battery_monitor =
[
    [ "BatteryMonitor", "classframework_1_1sensoractuator_1_1_battery_monitor.html#a5723146c94b97624f7e837ce14850fdb", null ],
    [ "~BatteryMonitor", "classframework_1_1sensoractuator_1_1_battery_monitor.html#ad27ea94fd7cad1912f57b99db2c2ddfa", null ],
    [ "IsBatteryLow", "classframework_1_1sensoractuator_1_1_battery_monitor.html#a18d25a376ef963a97f834b0f445f473b", null ],
    [ "SetBatteryValue", "classframework_1_1sensoractuator_1_1_battery_monitor.html#aeb37a7fb3e1080ddb4c64ea3e409e0c5", null ],
    [ "GetVoltage", "classframework_1_1sensoractuator_1_1_battery_monitor.html#a2bd5a8d2188160d3e8beec7c0c20f99d", null ]
];