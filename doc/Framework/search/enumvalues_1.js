var searchData=
[
  ['bgr',['BGR',['../classframework_1_1filter_1_1_cvt_color.html#a380b8c490377265e5cd342c9307f6441a2ad5640ebdec72fc79531d1778c6c2dc',1,'framework::filter::CvtColor::BGR()'],['../classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528ea2ad5640ebdec72fc79531d1778c6c2dc',1,'framework::core::cvimage::Type::BGR()']]],
  ['black',['Black',['../classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1caa6d29b92e3e478fa0225edd23d91fcde',1,'framework::gui::DataPlot']]],
  ['blue',['Blue',['../classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1ca8fab133d2d6300bcc7aa43519ba80d31',1,'framework::gui::DataPlot']]],
  ['bottomfrontleft',['BottomFrontLeft',['../classframework_1_1filter_1_1_x4_x8_multiplex.html#a7b6c53801967727a05019fde6b12af95acf21b3d531775198b50d27226ee36c6e',1,'framework::filter::X4X8Multiplex']]],
  ['bottomfrontright',['BottomFrontRight',['../classframework_1_1filter_1_1_x4_x8_multiplex.html#a7b6c53801967727a05019fde6b12af95a23574b537d14fe8f060faee90cee0943',1,'framework::filter::X4X8Multiplex']]],
  ['bottomrearleft',['BottomRearLeft',['../classframework_1_1filter_1_1_x4_x8_multiplex.html#a7b6c53801967727a05019fde6b12af95af2af22b01ec05ba504f17545d66e923b',1,'framework::filter::X4X8Multiplex']]],
  ['bottomrearright',['BottomRearRight',['../classframework_1_1filter_1_1_x4_x8_multiplex.html#a7b6c53801967727a05019fde6b12af95aceeac77420078232ba627961470623c5',1,'framework::filter::X4X8Multiplex']]]
];
