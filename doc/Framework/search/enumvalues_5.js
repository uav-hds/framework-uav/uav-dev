var searchData=
[
  ['gga',['GGA',['../classframework_1_1sensor_1_1_gps.html#a6835e9e2cdd6189bbdfb6d4483478234addb2829db61b82906101f81621b72a15',1,'framework::sensor::Gps']]],
  ['gps',['Gps',['../classframework_1_1sensor_1_1_gps.html#a3bec6c638e73b402193ad6a2141cb070a06960407a35b206422d40932f13c8d91',1,'framework::sensor::Gps']]],
  ['gray',['GRAY',['../classframework_1_1filter_1_1_cvt_color.html#a380b8c490377265e5cd342c9307f6441a48bf014c704c9eaae100a98006a37bf7',1,'framework::filter::CvtColor::GRAY()'],['../classframework_1_1core_1_1cvimage_1_1_type.html#ae8a6b4177d1f139d893636917635528ea48bf014c704c9eaae100a98006a37bf7',1,'framework::core::cvimage::Type::GRAY()']]],
  ['green',['Green',['../classframework_1_1gui_1_1_data_plot.html#ab8052b914d1b7330a7f0b12a7322bc1ca1531d101862a5ea9626c0b4d2f329d6d',1,'framework::gui::DataPlot']]],
  ['gst',['GST',['../classframework_1_1sensor_1_1_gps.html#a6835e9e2cdd6189bbdfb6d4483478234a1a1ffbc77c26d00e01df0a98f7c711df',1,'framework::sensor::Gps']]]
];
