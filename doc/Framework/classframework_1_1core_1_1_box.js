var classframework_1_1core_1_1_box =
[
    [ "Box", "classframework_1_1core_1_1_box.html#ae6dc6dec3960567cc69a3841c3716044", null ],
    [ "~Box", "classframework_1_1core_1_1_box.html#a957f7623616a9fe61717310fd57baf60", null ],
    [ "ValueChanged", "classframework_1_1core_1_1_box.html#a84a23ce9afcdbdc0008fb810aa448bcc", null ],
    [ "SetValueChanged", "classframework_1_1core_1_1_box.html#aab9f7e5af1f5baa2dfc18a8418ae921b", null ],
    [ "GetMutex", "classframework_1_1core_1_1_box.html#a9dbfeb5bf3796bbb4032cf00f116e9cf", null ],
    [ "ReleaseMutex", "classframework_1_1core_1_1_box.html#aaa28d8e9e3df9db0f03ee38589a63fa7", null ]
];